# What is cypress?

    A tool for reliably testing anything that runs in web browser


# Why adopt Cypress?

    Cypress has been growing in popularity quickly, currently the Cypress repository has over 13,000 stars on github, so it has become very rich documentation and forums for developers of all skill levels.
    
    
# Advantages of Cypress

    - Free & Open Source
    - Easy for learn
    - Simple environment setup 
    - Creating faster, easier and more reliable test cases
    - Compatible with friendly JavaScript structure
    - Appropriate tool for end-to-end testing (e2e) 
    - Tool with more practical features for debugging


# Basic Structure

    Cypress - After installing Cypress, the Cypress folder will be created in your project

    Fixtures - This folder can be used to store static data as JSON object

    Integration - This is the default folder to run test case scripts

    Plugins - This folder has an index.js file used to export modules

    Screenshots - This folder is suitable for storing the print screen when some test fails
        
    Support - It can be used to add some other files if you need help in your project like the commands.js file, where generic methods and functions can be created to import into test cases


# Pre Conditions to execute cypress for frontend tests automation

    - Install node js
    - Open a new terminal and insert the command: "npm init -y"
    - After iniciate npm, run command: "npm install"
    - run command: "npm install cypress --save-dev"

# Method to execute test script

    - finally run command: "npx cypress open" if want to show test run on browser
    or use "npx cypress run" for execute only by console.

# To run all tests with chrome

    - run command: "npx cypress run --browser chrome" or "npm run cy:chrome"

# To run all tests with firefox

    - run command: "npx cypress run --browser firefox" or "npm run cy:firefox"

# To run all tests with edge

    - run command: "npx cypress run --browser edge" or "npm run cy:edge"

# To run all tests with browser default (electron)

    - run command: "npx cypress run"