Cypress.Commands.add('getAccessToken', () => {
    return window.localStorage.getItem('access_token')
});

import ElementsGeneric from '../e2e/View/ElementsGeneric.cy.js'

let elements_generic = new ElementsGeneric;

Cypress.Commands.add('checkToast',(title,message)=>{
    if(elements_generic.getToast_Title().contains(title) && elements_generic.getToast_Message().contains(message))
        cy.log('Pass - The toast was displayed')
    else
        throw new Error("Fail - The toast was not displayed properly")
})

Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
  })

Cypress.Commands.add("login", (email, password) => {
    cy.session([email, password], () => {
      cy.visit("https://accounts.google.com/ServiceLogin?hl=pt-BR&passive=true&continue=http://support.google.com/mail/answer/8494%3Fhl%3Dpt-BR%26co%3DGENIE.Platform%253DDesktop&ec=GAZAdQ");
      
      cy.wait(500)
      cy.get('input[type="email"]',{timeout:60000}).type(email).type('{Enter}')
      cy.wait(5000)
      cy.get('input[type="password"]',{timeout:60000}).type(password).type('{Enter}')
      cy.get('input[id="totpPin"]',{timeout:60000}).type('{control}v', { force: true })
      cy.wait(1000)
    });
});