export default class Queries {
    quarter_TotalDemand(firstDay, lastDay){
        const query =  `select sum(pdp_qty) from brazil_demand bd 
                        INNER JOIN  material m
                        ON       m.id = bd.material_id
                        AND         m.status IS TRUE
                        where pdp_date between  '${firstDay}' and '${lastDay}'`
        return query
    }
    quarter_TotalAllocation(){
        const query =   `WITH CURRENT_PERIOD AS (
            SELECT      date_trunc('month', p.first_day_quarter)::date AS first_day_quarter_real,
                        p.last_day_quarter                             AS last_day_quarter,
                        cm.current_month                               AS current_month
            FROM        setup_management s
            INNER JOIN  country c
            ON          c.id = s.country_id
            AND         c.iso_code = 'BR'
            INNER JOIN  ( SELECT MIN(first_day_week)               AS first_day_quarter,
                                MAX(last_day_week)                 AS last_day_quarter,
                                fiscal_year                        AS fiscal_year,
                                quarter                            AS quarter
                          FROM        setup_management
                          INNER JOIN  country c
                          ON          c.id = country_id
                          AND         c.iso_code = 'BR'
                          GROUP BY    fiscal_year,
                                      quarter
                        ) AS p
            ON          p.fiscal_year = s.fiscal_year
            AND         p.quarter = s.quarter
            INNER JOIN  (SELECT lpad(cast ( date_part('month', (select current_timestamp)) as VARCHAR), 2, '0') as current_month) AS cm
            ON          true
            WHERE       current_date BETWEEN s.first_day_week AND s.last_day_week
            ),
            KA_ALLOCATION_PARAMS AS (
            SELECT      km.id                                                                        AS ka_id,
                        m.id                                                                         AS material_id,
                        ka.year_month::date                                                          AS year_month,
                        lpad(cast ( date_part('month', (ka.year_month::date)) as VARCHAR), 2, '0')   AS month,
                        CASE
                          WHEN date_trunc('month', cal.day::date)::date = ka.year_month::date
                            THEN 'week'||cal.one_plan_week
                          WHEN ka.year_month::date > current_date
                            THEN 'next'
                          WHEN ka.year_month::date < current_date
                            THEN 'previous'
                        END                                                                          AS week_in_time
            FROM        KA_ALLOCATION ka
            INNER JOIN  MATERIAL m
            ON          m.id = ka.material_id
            AND         m.status IS TRUE
            AND         m.product_id IS NOT NULL
            INNER JOIN  KA_MANAGEMENT km
            ON          km.id = ka.ka_management_id
            AND         km.status IS TRUE
            INNER JOIN  KA_MANAGEMENT_MATERIALS kmm
            ON          kmm.material_id = m.id
            AND         kmm.ka_management_id = km.id
            AND         kmm.status is true
            INNER JOIN  CURRENT_PERIOD cp
            ON          ka.year_month::date between cp.first_day_quarter_real and cp.last_day_quarter
            INNER JOIN  CALENDAR_BQ cal
            ON          cal.day::date = current_date
            ),
            KA_ALLOCATION_PREPARE as (
            SELECT      kap.month                                             AS month,
                        kap.week_in_time                                      AS week_in_time,
                        COALESCE(SUM(ka.week1_actual),0)                      AS week1_actual,
                        COALESCE(SUM(ka.week1_allocated),0)                   AS week1_allocated,
                        COALESCE(SUM(ka.week2_actual),0)                      AS week2_actual,
                        COALESCE(SUM(ka.week2_allocated),0)                   AS week2_allocated,
                        COALESCE(SUM(ka.week3_actual),0)                      AS week3_actual,
                        COALESCE(SUM(ka.week3_allocated),0)                   AS week3_allocated,
                        COALESCE(SUM(ka.week4_actual),0)                      AS week4_actual,
                        COALESCE(SUM(ka.week4_allocated),0)                   AS week4_allocated,
                        COALESCE(SUM(ka.week5_actual),0)                      AS week5_actual,
                        COALESCE(SUM(ka.week5_allocated),0)                   AS week5_allocated
            FROM        KA_ALLOCATION_PARAMS kap
            INNER JOIN  KA_ALLOCATION ka
            ON          kap.material_id = ka.material_id
            AND         kap.ka_id = ka.ka_management_id
            AND         kap.year_month = ka.year_month::date
            GROUP BY    kap.month,
                        kap.week_in_time
            ),
            KA_ALLOCATION_RESULT as (
            SELECT      CASE
                          WHEN kap.week_in_time = 'previous' THEN COALESCE(kap.week1_actual,0)
                          ELSE
                            CASE
                              WHEN kap.week_in_time = 'week1' THEN COALESCE(kap.week1_allocated,0)
                                ELSE CASE WHEN kap.week_in_time = 'week2' THEN COALESCE(kap.week1_actual,0)
                                  ELSE CASE WHEN kap.week_in_time = 'week3' THEN COALESCE(kap.week1_actual,0)
                                    ELSE CASE WHEN kap.week_in_time = 'week4' THEN COALESCE(kap.week1_actual,0)
                                      ELSE CASE WHEN kap.week_in_time = 'week5' THEN COALESCE(kap.week1_actual,0)
                                        ELSE CASE WHEN kap.week_in_time = 'next' and kap.month <> cp.current_month THEN COALESCE(kap.week1_allocated,0)
                                            END
                                          END
                                       END
                                     END
                                   END
                            END
                        END                                                                                                              AS week1,
                        CASE
                          WHEN kap.week_in_time = 'previous' THEN COALESCE(kap.week2_actual,0)
                          ELSE
                            CASE
                              WHEN kap.week_in_time = 'week1' THEN COALESCE(kap.week2_allocated,0)
                                ELSE CASE WHEN kap.week_in_time = 'week2' THEN COALESCE(kap.week2_allocated,0)
                                    ELSE CASE WHEN kap.week_in_time = 'week3' THEN COALESCE(kap.week2_actual,0)
                                      ELSE CASE WHEN kap.week_in_time = 'week4' THEN COALESCE(kap.week2_actual,0)
                                        ELSE CASE WHEN kap.week_in_time = 'week5' THEN COALESCE(kap.week2_actual,0)
                                          ELSE CASE WHEN kap.week_in_time = 'next' and kap.month <> cp.current_month THEN COALESCE(kap.week2_allocated,0)
                                            END
                                           END
                                         END
                                       END
                                    END
                            END
                        END                                                                                                              AS week2,
                        CASE
                          WHEN kap.week_in_time = 'previous' THEN COALESCE(kap.week3_actual,0)
                          ELSE
                            CASE
                              WHEN kap.week_in_time = 'week1' THEN COALESCE(kap.week3_allocated,0)
                                ELSE CASE WHEN kap.week_in_time = 'week2' THEN COALESCE(kap.week3_allocated,0)
                                  ELSE CASE WHEN kap.week_in_time = 'week3' THEN COALESCE(kap.week3_allocated,0)
                                    ELSE CASE WHEN kap.week_in_time = 'week4' THEN COALESCE(kap.week3_actual,0)
                                      ELSE CASE WHEN kap.week_in_time = 'week5' THEN COALESCE(kap.week3_actual,0)
                                        ELSE CASE WHEN kap.week_in_time = 'next' and kap.month <> cp.current_month THEN COALESCE(kap.week3_allocated,0)
                                            END
                                         END
                                       END
                                     END
                                END
                            END
                        END                                                                                                              AS week3,
                        CASE
                          WHEN kap.week_in_time = 'previous' THEN COALESCE(kap.week4_actual,0)
                          ELSE
                            CASE
                              WHEN kap.week_in_time = 'week1' THEN COALESCE(kap.week4_allocated,0)
                                ELSE CASE WHEN kap.week_in_time = 'week2' THEN COALESCE(kap.week4_allocated,0)
                                    ELSE CASE WHEN kap.week_in_time = 'week3' THEN COALESCE(kap.week4_allocated,0)
                                      ELSE CASE WHEN kap.week_in_time = 'week4' THEN COALESCE(kap.week4_allocated,0)
                                        ELSE CASE WHEN kap.week_in_time = 'week5' THEN COALESCE(kap.week4_actual,0)
                                          ELSE CASE WHEN kap.week_in_time = 'next' and kap.month <> cp.current_month THEN COALESCE(kap.week4_allocated,0)
                                            END
                                          END
                                        END
                                      END
                                    END
                            END
                        END                                                                                                              AS week4,
                        CASE
                          WHEN kap.week_in_time = 'previous' THEN COALESCE(kap.week5_actual,0)
                          ELSE
                            CASE
                              WHEN kap.week_in_time = 'week1' THEN COALESCE(kap.week5_allocated,0)
                                ELSE CASE WHEN kap.week_in_time = 'week2' THEN COALESCE(kap.week5_allocated,0)
                                    ELSE CASE WHEN kap.week_in_time = 'week3' THEN COALESCE(kap.week5_allocated,0)
                                      ELSE CASE WHEN kap.week_in_time = 'week4' THEN COALESCE(kap.week5_allocated,0)
                                        ELSE CASE WHEN kap.week_in_time = 'week5' THEN COALESCE(kap.week5_allocated,0)
                                          ELSE CASE WHEN kap.week_in_time = 'next' and kap.month <> cp.current_month THEN COALESCE(kap.week5_allocated,0)
                                            END
                                          END
                                        END
                                      END
                                    END
                            END
                        END                                                                                                              AS week5
            FROM        KA_ALLOCATION_PREPARE kap
            INNER JOIN  CURRENT_PERIOD cp
            ON          TRUE
            ),
            KA_ALLOCATION_DETAIL as (
            SELECT      SUM(( kar.week1::int + kar.week2::int + kar.week3::int + kar.week4::int + kar.week5::int ))                     AS order_qty
            FROM        KA_ALLOCATION_RESULT kar
            ) select * from KA_ALLOCATION_DETAIL;`
        return query
    }
}