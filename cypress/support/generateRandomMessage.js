function msgTestAuto(){
    var s = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    const msgTestAuto = Array(5).join().split(',').map(function() { return s.charAt(Math.floor(Math.random() * s.length)); }).join('');
    console.log(`Message genereted was ${msgTestAuto}`)
    return msgTestAuto
}
export default msgTestAuto();
