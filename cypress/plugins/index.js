/// <reference types="cypress" />

// const cucumber = require('cypress-cucumber-preprocessor').default
// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

/**
 * @type {Cypress.PluginConfig}
 */
const { Client } = require('pg');

module.exports = (on, config) => {
  on("task", {
    async connectDB(query){
      const client = new Client({
        user: "customer_allocation",
        password: "xcy2v?%2xx56RVFX",
        host: "127.0.0.1",
        database: "postgres",
        ssl: false,
        port: 5432
      })
      await client.connect()
      const res = await client.query(query)
      await client.end()
      return res.rows;
    }
  })
};

