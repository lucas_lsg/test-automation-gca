import SetupManagement_Controller from "./SetupManagement_Controller.cy"
import Queries from "../../support/queries"
let setupManagement_Controller = new SetupManagement_Controller
let query = new Queries
export default class KA_Allocation_Controller{
    
    checkDemandValue_DealVsAlloc(){
       
        cy.getAccessToken().then(token => {
            cy.log(token)
            cy.request({
                method: 'GET',
                url: 'https://dev-dot-gca-back-dot-dataservices-non-prod.appspot.com/kaAllocation/dealVsAllocation',
                headers: {
                    Authorization: token,
                    'Content-Type': 'application/json',
                },
                includeCredentials: true
            }).then(response => {
                expect(response.status).to.eq(200)
                cy.wrap(response.body).as('payload')
            })

            cy.get('@payload').then((res) => {
                var total_demand_payload = res.cards[0].valueDD
                setupManagement_Controller.getCurrentQuarter_RangeDate().then((list_date)=>{
                    cy.task("connectDB", query.quarter_TotalDemand(list_date[0],list_date[1])
                    ).then((total_demand_query)=>{
                        //format query results
                        var total_demand_query = parseInt(Object.values(total_demand_query[0])[0])
                        if(total_demand_payload == 0){
                            expect(total_demand_query).to.be.NaN
                        }else{
                            //Compare values
                            if(total_demand_query != null){
                                expect(total_demand_query).to.eq(total_demand_payload)
                            }
                        }
                    })
                })
            });
        })
    }

    checkAllocationValue_DealVsAlloc(){
        
        cy.getAccessToken().then(token => {
            cy.log(token)
            cy.request({
                method: 'GET',
                url: 'https://dev-dot-gca-back-dot-dataservices-non-prod.appspot.com/kaAllocation/dealVsAllocation',
                headers: {
                    Authorization: token,
                    'Content-Type': 'application/json',
                },
                includeCredentials: true
            }).then(response => {
                expect(response.status).to.eq(200)
                cy.wrap(response.body).as('payload')
            })

            cy.get('@payload').then((res) => {
                var total_alloc_payload = res.cards[0].valueTot
                cy.task("connectDB", query.quarter_TotalAllocation()
                ).then((total_alloc_query)=>{
                    //format query results
                    var total_alloc_query = parseInt(Object.values(total_alloc_query[0])[0])
                    if(total_alloc_payload == 0){
                        expect(total_alloc_query).to.be.NaN
                    }else{
                        //Compare values
                        if(total_alloc_query != null){
                            expect(total_alloc_query).to.eq(total_alloc_payload)
                        }
                    }
                })
            });
        })
    }

    checkDemandValue_DemandVsMls(){
        
        cy.getAccessToken().then(token => {
            cy.log(token)
            cy.request({
                method: 'GET',
                url: 'https://dev-dot-gca-back-dot-dataservices-non-prod.appspot.com/kaAllocation/demandVsMls',
                headers: {
                    Authorization: token,
                    'Content-Type': 'application/json',
                },
                includeCredentials: true
            }).then(response => {
                expect(response.status).to.eq(200);
                cy.wrap(response.body).as('payload')
            })
            cy.get('@payload').then((res) => {
                let total_demand_payload = res.data.source[3].values[0].valueBrDemand
                setupManagement_Controller.getCurrentQuarter_RangeDate().then((list_date)=>{
                    cy.task("connectDB", query.quarter_TotalDemand(list_date[0],list_date[1])
                    ).then((total_demand_query)=>{
                        //format query results
                        var total_demand_query = parseInt(Object.values(total_demand_query[0])[0])
                        if(total_demand_payload == 0){
                            expect(total_demand_query).to.be.NaN
                        }else{
                            //Compare values
                            if(total_demand_query != null){
                                expect(total_demand_query).to.eq(total_demand_payload)
                            }
                        }
                    })
                })
            });
        })
    }

    checkAllocationValue_DemandVsMls(){
        
        cy.getAccessToken().then(token => {
            cy.log(token)
            cy.request({
                method: 'GET',
                url: 'https://dev-dot-gca-back-dot-dataservices-non-prod.appspot.com/kaAllocation/demandVsMls',
                headers: {
                    Authorization: token,
                    'Content-Type': 'application/json',
                },
                includeCredentials: true
            }).then(response => {
                expect(response.status).to.eq(200);
                cy.wrap(response.body).as('payload')
            })
            cy.get('@payload').then((res) => {
                let total_alloc_payload = res.data.source[3].values[1].valueMls
                cy.task("connectDB",query.quarter_TotalAllocation()
                ).then((total_alloc_query)=>{
                    //format query results
                    var total_alloc_query = parseInt(Object.values(total_alloc_query[0])[0])
                    if(total_alloc_payload == 0){
                        expect(total_alloc_query).to.be.NaN
                    }else{
                        //Compare values
                        if(total_alloc_query != null){
                            expect(total_alloc_query).to.eq(total_alloc_payload)
                        }
                    }
                })
            });
        })
    }
}