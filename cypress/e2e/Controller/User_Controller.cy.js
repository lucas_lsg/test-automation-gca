import ElementUserPage from '../View/ElementsUserPage.cy.js'
import ElementsHomePage from '../View/ElementsHomePage.cy.js'
import ElementsGeneric from '../View/ElementsGeneric.cy.js'
let elements_UserPage = new ElementUserPage;
let elements_homePage = new ElementsHomePage;
let elements_Generic = new ElementsGeneric;

export default class User_Controller{
    openUserPage(){
        if(elements_homePage.getTitle_Page()){
            elements_homePage.getCard_Admin().click()
            elements_homePage.getCard_AdminUser().click()
            return true
        }else cy.log('Not able to access home page')
    }
    backtoHomePage(){
        elements_Generic.getNavLinkHome().click({force:true})
    }
    checkElements(){
        this.openUserPage()
        elements_Generic.getNav_Bar()
        elements_UserPage.getTitle_Page()
        elements_UserPage.getInputList_LastName()
        elements_UserPage.getInputList_Name()
        elements_UserPage.getInputList_Geo()
        elements_UserPage.getInputList_Country()
        elements_UserPage.getSelectList_Status()
        elements_UserPage.getInputList_Profile()
        elements_UserPage.getButton_New_User().click()
        elements_UserPage.getInput_LastName_Modal()
        elements_UserPage.getInput_Name_Modal()
        elements_UserPage.getInput_Email_Modal()
        elements_UserPage.getInput_Country_Modal()
        elements_UserPage.getSelect_Status_Modal()
        elements_UserPage.getInput_Profile_Modal()
        elements_UserPage.getSelect_SalesManager_Modal()
        elements_UserPage.getSelect_OrderAdm_Modal()
        elements_UserPage.getInput_Profile_Modal()
        elements_Generic.getButton_Save()
        elements_Generic.getButton_Cancel().click()
        this.backtoHomePage()
    }
    createNewUser(lastname, name, email, country,status,profile, salesmanager, orderadm, toast){
        if(this.openUserPage()){
            if(elements_UserPage.getTitle_Page()){
                if (elements_UserPage.getButton_New_User().click()){
                    elements_UserPage.getInput_LastName_Modal().type(lastname)
                    elements_UserPage.getInput_Name_Modal().type(name)
                    elements_UserPage.getInput_Email_Modal().type(email.toLowerCase())
                    if(country == " "){
                        elements_UserPage.getSelect_Status_Modal().click()
                        cy.wait(600)
                        elements_Generic.getFiltered_Result().contains(status).click()
                        elements_UserPage.getInput_Profile_Modal().type(profile)
                        cy.wait(600)
                        elements_Generic.getFiltered_Result().contains(profile).click()
                        elements_UserPage.getSelect_SalesManager_Modal().click()
                        cy.wait(600)
                        elements_Generic.getFiltered_Result().contains(salesmanager).click()
                        elements_UserPage.getSelect_OrderAdm_Modal().click()
                        cy.wait(600)
                        elements_Generic.getFiltered_Result().contains(orderadm).click()
                    }else if(status == " "){
                        elements_UserPage.getInput_Country_Modal().click()
                        elements_UserPage.getInput_Country_Modal().type(country)
                        cy.wait(600)
                        elements_Generic.getFiltered_Result().contains(country).click()
                        cy.wait(600)
                        elements_UserPage.getInput_Profile_Modal().type(profile)
                        cy.wait(600)
                        elements_Generic.getFiltered_Result().contains(profile).click()
                        elements_UserPage.getSelect_SalesManager_Modal().click()
                        cy.wait(600)
                        elements_Generic.getFiltered_Result().contains(salesmanager).click()
                        elements_UserPage.getSelect_OrderAdm_Modal().click()
                        cy.wait(600)
                        elements_Generic.getFiltered_Result().contains(orderadm).click()
                    }else if(profile == " "){
                        elements_UserPage.getInput_Country_Modal().click()
                        elements_UserPage.getInput_Country_Modal().type(country)
                        cy.wait(600)
                        elements_Generic.getFiltered_Result().contains(country).click()
                        elements_UserPage.getSelect_Status_Modal().click()
                        cy.wait(600)
                        elements_Generic.getFiltered_Result().contains(status).click()
                        cy.wait(600)
                        elements_UserPage.getSelect_SalesManager_Modal().click()
                        cy.wait(600)
                        elements_Generic.getFiltered_Result().contains(salesmanager).click()
                        elements_UserPage.getSelect_OrderAdm_Modal().click()
                        cy.wait(600)
                        elements_Generic.getFiltered_Result().contains(orderadm).click()

                    }else if(salesmanager == " "){
                        elements_UserPage.getInput_Country_Modal().click()
                        elements_UserPage.getInput_Country_Modal().type(country)
                        cy.wait(600)
                        elements_Generic.getFiltered_Result().contains(country).click()
                        elements_UserPage.getSelect_Status_Modal().click()
                        cy.wait(600)
                        elements_Generic.getFiltered_Result().contains(status).click()
                        elements_UserPage.getInput_Profile_Modal().type(profile)
                        cy.wait(600)
                        elements_Generic.getFiltered_Result().contains(profile).click()
                        elements_UserPage.getSelect_OrderAdm_Modal().click()
                        cy.wait(600)
                        elements_Generic.getFiltered_Result().contains(orderadm).click()
                    }else if(orderadm == " "){
                        elements_UserPage.getInput_Country_Modal().click()
                        elements_UserPage.getInput_Country_Modal().type(country)
                        cy.wait(600)
                        elements_Generic.getFiltered_Result().contains(country).click()
                        elements_UserPage.getSelect_Status_Modal().click()
                        cy.wait(600)
                        elements_Generic.getFiltered_Result().contains(status).click()
                        elements_UserPage.getInput_Profile_Modal().type(profile)
                        cy.wait(600)
                        elements_Generic.getFiltered_Result().contains(profile).click()
                        elements_UserPage.getSelect_SalesManager_Modal().click()
                        cy.wait(600)
                        elements_Generic.getFiltered_Result().contains(salesmanager).click()
                    }else{
                        elements_UserPage.getInput_Country_Modal().click()
                        elements_UserPage.getInput_Country_Modal().type(country)
                        cy.wait(600)
                        elements_Generic.getFiltered_Result().contains(country).click()
                        elements_UserPage.getSelect_Status_Modal().click()
                        cy.wait(600)
                        elements_Generic.getFiltered_Result().contains(status).click()
                        elements_UserPage.getInput_Profile_Modal().type(profile)
                        cy.wait(600)
                        elements_Generic.getFiltered_Result().contains(profile).click()
                        elements_UserPage.getSelect_SalesManager_Modal().click()
                        cy.wait(600)
                        elements_Generic.getFiltered_Result().contains(salesmanager).click()
                        elements_UserPage.getSelect_OrderAdm_Modal().click()
                        cy.wait(600)
                        elements_Generic.getFiltered_Result().contains(orderadm).click()
                    }
                    elements_Generic.getButton_Save().click()
                    this.checktoasts(toast)
                }
            }else cy.log('Not able to access User page')
        }
        this.backtoHomePage()
    }
    updateNewUser(lastname, name, email, oldcountry, newcountry, oldstatus, newstatus, oldprofile, newprofile, salesmanager, orderadm,toast){
        if(this.openUserPage()){
            if(elements_UserPage.getTitle_Page()){
                elements_Generic.getButtonClearFilter().click()
                elements_Generic.getButtonClearFilter().click()
                elements_UserPage.getInputList_LastName().type(lastname)
                cy.wait(600)
                elements_Generic.getFiltered_Result().contains(lastname).click()
                elements_UserPage.getInputList_Name().type(name)
                cy.wait(600)
                elements_Generic.getFiltered_Result().contains(name).click()
                elements_UserPage.getInputList_Country().type(oldcountry).click()
                cy.wait(600)
                elements_Generic.getFiltered_Result().contains(oldcountry)
                elements_UserPage.getSelectList_Status().click()
                cy.wait(600)
                elements_Generic.getFiltered_Result().contains(oldstatus).click()
                elements_UserPage.getInputList_Profile().type(oldprofile)
                cy.wait(600)
                elements_Generic.getFiltered_Result().contains(oldprofile).click()
                cy.wait(3000)
                elements_Generic.getIcon_Edit().click()
                elements_UserPage.getInput_LastName_Modal().type('edit')
                elements_UserPage.getInput_Name_Modal().type('edit')
                elements_UserPage.getInput_Email_Modal().clear()
                elements_UserPage.getInput_Email_Modal().type(email.toLowerCase())
                elements_UserPage.getInput_Country_Modal().clear()
                elements_UserPage.getInput_Country_Modal().click()
                elements_UserPage.getInput_Country_Modal().type(newcountry)
                cy.wait(600)
                elements_Generic.getFiltered_Result().contains(newcountry).click()
                elements_UserPage.getSelect_Status_Modal().click()
                cy.wait(600)
                elements_Generic.getFiltered_Result().contains(newstatus).click()
                elements_UserPage.getInput_Profile_Modal().click()
                cy.wait(600)
                elements_Generic.getFiltered_Result().contains(newprofile).click()
                elements_UserPage.getSelect_SalesManager_Modal().click()
                cy.wait(600)
                elements_Generic.getFiltered_Result().contains(salesmanager).click()
                elements_UserPage.getSelect_OrderAdm_Modal().click()
                cy.wait(600)
                elements_Generic.getFiltered_Result().contains(orderadm).click()
                elements_Generic.getButton_Save().click()
                if(elements_Generic.getTitle_Confirmation()){
                    elements_Generic.getButton_Yes().click()
                    this.checktoasts(toast)
                }else cy.log("Confirmation modal was not displayed")
            }
        }
        this.backtoHomePage()
    }
    deleteNewUser(lastname, name, country, status, profile, toast){
        if(this.openUserPage()){
            if(elements_UserPage.getTitle_Page()){
                elements_UserPage.getInputList_LastName().type(lastname)
                cy.wait(600)
                elements_Generic.getFiltered_Result().contains(lastname).click()
                elements_UserPage.getInputList_Name().type(name)
                cy.wait(600)
                elements_Generic.getFiltered_Result().contains(name).click()
                elements_UserPage.getInputList_Country().click()
                cy.wait(600)
                elements_Generic.getFiltered_Result().contains(country).click()
                elements_UserPage.getSelectList_Status().click()
                cy.wait(600)
                elements_Generic.getFiltered_Result().contains(status).click()
                elements_UserPage.getInputList_Profile().type(profile)
                cy.wait(600)
                elements_Generic.getFiltered_Result().contains(profile).click()
                cy.wait(3000)
                elements_Generic.getIcon_Delete().click()
                if(elements_Generic.getTitle_Confirmation()){
                    elements_Generic.getButton_Yes().click()
                    this.checktoasts(toast)
                }else cy.log("Confirmation modal was not displayed")
            }
        }
        this.backtoHomePage()
    }

    checkDuplicatedUser(lastname, name, email, country,status,profile, salesmanager, orderadm, toast){
        for(var i = 0; i < 2; i++){
            if(this.openUserPage()){
                if(elements_UserPage.getTitle_Page()){
                    if (elements_UserPage.getButton_New_User().click()){
                        elements_UserPage.getInput_LastName_Modal().type(lastname)
                        elements_UserPage.getInput_Name_Modal().type(name)
                        elements_UserPage.getInput_Email_Modal().type(email.toLowerCase())
                        elements_UserPage.getInput_Country_Modal().click()
                        elements_UserPage.getInput_Country_Modal().type(country)
                        cy.wait(600)
                        elements_Generic.getFiltered_Result().contains(country).click()
                        elements_UserPage.getSelect_Status_Modal().click()
                        cy.wait(600)
                        elements_Generic.getFiltered_Result().contains(status).click()
                        elements_UserPage.getInput_Profile_Modal().type(profile)
                        cy.wait(600)
                        elements_Generic.getFiltered_Result().contains(profile).click()
                        elements_UserPage.getSelect_SalesManager_Modal().click()
                        cy.wait(600)
                        elements_Generic.getFiltered_Result().contains(salesmanager).click()
                        elements_UserPage.getSelect_OrderAdm_Modal().click()
                        cy.wait(600)
                        elements_Generic.getFiltered_Result().contains(orderadm).click()
                    }
                    elements_Generic.getButton_Save().click()
                    elements_Generic.getButton_Cancel().click()
                    this.backtoHomePage()
                }
            }else cy.log('Not able to access User page')
        }
        this.checktoasts(toast)
    }

    checkNotFoundUser(name,status){
        if(this.openUserPage()){
            if(elements_UserPage.getTitle_Page()){
                elements_UserPage.getInputList_Name().click()
                elements_UserPage.getInputList_Name().type(name)
                elements_UserPage.getSelectList_Status().click()
                cy.wait(600)
                elements_Generic.getFiltered_Result().contains(status).click()
                cy.wait(600)
                elements_UserPage.checkNoResultsFound().contains("No results were found")
            }
        }
    }

    checktoasts(expectedToast){
        cy.wait(2000)
        elements_Generic.getToast().then((toast)=>{
            switch (expectedToast) {
                case "newUser_success":
                    cy.checkToast("New User","Registered successfully")
                    break;
                case "newUser_duplicate_failed":
                    cy.checkToast("USER EMAIL already exists","Please inform another USER EMAIL!")
                    break;
                case "newUser_invalidEmail":
                    cy.checkToast("Invalid e-mail address","Please check if the domain is @motorola.com or @lenovo.com!")
                    break;
                case "updateUser_success":
                    cy.checkToast("Updated User","Successfully!")
                    break;
                case "deleteUser_success":
                    cy.checkToast("User","Successfully removed!")
                    break;
                case "deleteUser_associatedOrderwithSales":
                    cy.checkToast("SALES MANAGER associated with this USER","Please remove it before delete to complete this operation!")
                    break;
                case "deleteUser_associatedSaleswithOrder":
                    cy.checkToast("ORDER ADMIN associated with this USER","Please remove it before delete to complete this operation!")
                    break;
                case "lastName_mandatoryField":
                    cy.checkToast("Last Name","Mandatory field")
                    break;
                case "name_mandatoryField":
                    cy.checkToast("Name","Mandatory field")
                    break;
                case "email_mandatoryField":
                    cy.checkToast("Email","Mandatory field")
                    break;
                case "country_mandatoryField":
                    cy.checkToast("Country","Mandatory field")
                    break;
                case "status_mandatoryField":
                    cy.checkToast("Status","Mandatory field")
                    break;
                case "profile_mandatoryField":
                    cy.checkToast("Profile","Mandatory field")
                    break;
                case "salesManager_mandatoryField":
                    cy.checkToast("This user is a Sales Manager?","Mandatory field")
                    break;
                case "orderAdmin_mandatoryField":
                    cy.checkToast("This user is a Order Admin?","Mandatory field")
                    break;
                default:
                    console.log("Unknown Toast");
                    break;
                }
            })
    }
}