import ElementOrderAdminPage from '../View/ElementsOrderAdminPage.cy.js'
import ElementsHomePage from '../View/ElementsHomePage.cy.js'
import ElementsGeneric from '../View/ElementsGeneric.cy.js'
let elements_OrderAdminPage = new ElementOrderAdminPage;
let elements_homePage = new ElementsHomePage;
let elements_Generic = new ElementsGeneric;

export default class SetupOrderAdmin_Controller{
    openOrderAdminPage(){
        if(elements_homePage.getTitle_Page()){
            elements_homePage.getCard_Setup().click()
            elements_homePage.getCard_SetupOrderADM().click()
            return true
        }else cy.log('Not able to access home page')
    }
    backtoHomePage(){
        elements_Generic.getNavLinkHome().click()
    }
    checkElements(){
        this.openOrderAdminPage()
        elements_Generic.getNav_Bar()
        elements_OrderAdminPage.getTitle_Page()
        elements_OrderAdminPage.getSelect_Geo()
        elements_OrderAdminPage.getSelect_Country()
        elements_OrderAdminPage.getSelect_OrderAdmin()
        this.backtoHomePage()
    }
    updateAssociationOrderAdmin(orderadmin, salesmanagerList, toast){
        if(this.openOrderAdminPage()){
            if(elements_OrderAdminPage.getTitle_Page){
                elements_OrderAdminPage.getSelect_OrderAdmin().type(orderadmin)
                cy.wait(600)
                elements_Generic.getFiltered_Result().contains(orderadmin).click()
                cy.wait(3000)
                elements_Generic.getIcon_Edit().click()
                if (elements_OrderAdminPage.getTitle_Modal().click()){
                    elements_OrderAdminPage.getSelect_SalesManager_Modal().click()
                    for(var i = 0 ; i < salesmanagerList.length ; i ++){
                        cy.wait(600)
                        elements_Generic.getFiltered_Result().contains(salesmanagerList[i].toString()).click()
                        cy.get('body').trigger('keydown', { keyCode: 27})
                        cy.wait(100);
                        cy.get('body').trigger('keyup', { keyCode: 27})
                    }
                    elements_Generic.getButton_Save().click()
                    if(elements_Generic.getTitle_Confirmation()){
                        elements_Generic.getButton_Yes().click()
                        this.checktoasts(toast)
                    }else cy.log("Confirmation modal was not displayed")
                }
            }else cy.log('Not able to access SetupOrderAdmin page')
        }
        this.backtoHomePage()
    }
    removeAssociationOrderAdmin(orderadmin, toast){
        if(this.openOrderAdminPage()){
            if(elements_OrderAdminPage.getTitle_Page){
                elements_OrderAdminPage.getSelect_OrderAdmin().type(orderadmin)
                cy.wait(600)
                elements_Generic.getFiltered_Result().contains(orderadmin).click()
                cy.wait(3000)
                elements_Generic.getIcon_Delete().click()
                if(elements_Generic.getTitle_Confirmation()){
                    elements_Generic.getButton_Yes().click()
                    this.checktoasts(toast)
                }else cy.log("Confirmation modal was not displayed")
            }
        }
        this.backtoHomePage()
    }
    checktoasts(expectedToast){
        cy.wait(2000)
        elements_Generic.getToast().then((toast)=>{
            switch (expectedToast) {
                case "updateSetupOrderAdmin_success":
                    cy.checkToast("Updated Association","Successfully!")
                    break;
                case "removeOrderAdminAssociation_success":
                    cy.checkToast("Association","Successfully removed!")
                    break;
                case "removeOrderAdminAssociation_failed":
                    cy.checkToast("Delete not allowed", "These sales managers are associated with a sales structure of some customer. Please, remove this association to complete this action!")
                    break;
                default:
                    console.log("Unknown Toast");
                    break;
                }
            })
    }
}