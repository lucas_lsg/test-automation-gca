
import ElementFiscalCalendarPage from '../View/ElementsFiscalCalendarPage.cy.js'
import ElementsHomePage from '../View/ElementsHomePage.cy.js'
import ElementsGeneric from '../View/ElementsGeneric.cy.js'
let elements_FiscalCalendarPage = new ElementFiscalCalendarPage;
let elements_homePage = new ElementsHomePage;
let elements_Generic = new ElementsGeneric;

export default class SetupFiscalCalendar_Controller{
    openFiscalCalendarPage(){
        if(elements_homePage.getTitle_Page()){
            elements_homePage.getCard_Setup().click()
            elements_homePage.getCard_SetupFiscalCalendar().click()
            return true
        }else cy.log('Not able to access home page')
    }
    backtoHomePage(){
        elements_Generic.getNavLinkHome().click({force:true})
    }
    checkElements(){
        this.openFiscalCalendarPage()
        elements_Generic.getNav_Bar()
        elements_FiscalCalendarPage.getTitle_Page()
        elements_FiscalCalendarPage.getInputList_Quarter()
        elements_FiscalCalendarPage.getInputList_Week()
        elements_FiscalCalendarPage.getInputList_Date()
        this.backtoHomePage()
    }

    checkFilters(filters_list,fiscal_year,quarter,week,day){
        if(this.openFiscalCalendarPage()){
            if(elements_FiscalCalendarPage.getTitle_Page()){
                for(var i=0; i<filters_list.length;i++){
                    if(filters_list[i]=='FISCAL_YEAR'){
                        // Check default value in fiscal year filter
                        elements_FiscalCalendarPage.getFiscalYear_Table().then((filter_result)=>{
                            if(filter_result[0].innerText ==  fiscal_year +"/"+ (parseInt(fiscal_year)+1)){
                                expect(filter_result[0].innerText).equal(fiscal_year +"/"+ (parseInt(fiscal_year)+1))
                                cy.log('PASS - Fiscal year filter is working correctly')
                            }else{
                                cy.log('FAIL - Fiscal year filter is not working correctly')
                            }
                        })
                    }

                    else if(filters_list[i]=='QUARTER'){
                        // Check default value in quarter filter
                        elements_FiscalCalendarPage.getQuarter_Table().then((filter_result)=>{
                            if(filter_result[0].innerText == quarter){
                                expect(filter_result[0].innerText).equal(quarter)
                                cy.log('PASS - Quarter filter is working correctly')
                            }else{
                                cy.log('FAIL - Quarter filter is not working correctly')
                            }
                        })
                    }

                    else if(filters_list[i]=='WEEK'){
                        elements_FiscalCalendarPage.getInputList_Week().click()
                        elements_Generic.getFiltered_Result().contains(week).click()
                        cy.wait(3000)
                        elements_FiscalCalendarPage.getWeek_Table().then((filter_result)=>{
                            if(filter_result[0].innerText == 'WK'+week){
                                expect(filter_result[0].innerText).equal('WK'+week)
                                cy.log('PASS - Week filter is working correctly')
                            }else{
                                cy.log('FAIL - Week filter is not working correctly')
                            }
                        })
                    }

                    else if(filters_list[i]=='DAY'){
                        elements_FiscalCalendarPage.getCalendarIcon().click()
                        elements_FiscalCalendarPage.getOneDay().contains(day).click()
                        cy.wait(3000)
                        elements_FiscalCalendarPage.getDay_Table().then((filter_result)=>{
                            if((filter_result[0].innerText.split('/'))[1].split('')[1] == day){
                                expect((filter_result[0].innerText.split('/'))[1].split('')[1]).equal(day)
                                cy.log('PASS - Day filter is working correctly')
                            }else{
                                cy.log('FAIL - Day filter is not working correctly')
                            }
                        })
                    }
                }
            }
        }
    }
}