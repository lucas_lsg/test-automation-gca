import ElementSetupManagementPage from '../View/ElementsSetupManagementPage.cy.js'
import ElementsHomePage from '../View/ElementsHomePage.cy.js'
import ElementsGeneric from '../View/ElementsGeneric.cy.js'
let elements_SetupManagementPage = new ElementSetupManagementPage;
let elements_homePage = new ElementsHomePage;
let elements_Generic = new ElementsGeneric;

export default class SetupManagement_Controller{
    openSetupManagementPage(){
        if(elements_homePage.getTitle_Page()){
            elements_homePage.getCard_Setup().click()
            elements_homePage.getCard_SetupSManag().click()
            return true
        }else cy.log('Not able to access home page')
    }
    backtoHomePage(){
        elements_Generic.getNavLinkHome().click()
    }
    checkElements(){
        this.openSetupManagementPage()
        elements_Generic.getNav_Bar()
        elements_SetupManagementPage.getInputList_Country()
        elements_SetupManagementPage.getSelectList_FiscalYear()
        elements_SetupManagementPage.getSelectList_Quarter()
        elements_SetupManagementPage.getButton_Edit_Current_Setup().click()
        elements_SetupManagementPage.getTitle_Modal()
        elements_SetupManagementPage.getInput_Country()
        elements_SetupManagementPage.getInput_ToBook()
        elements_Generic.getButton_Save()
        elements_Generic.getButton_Cancel().click()
        this.backtoHomePage()
    }

    checkEditCurrentSetup(weekPlus,feature,toast){
        if (this.openSetupManagementPage()){
            elements_SetupManagementPage.getButton_Edit_Current_Setup().click()
            if(feature == "toBook"){
                elements_SetupManagementPage.getInput_ToBook().click()
            }else if(feature == "toAllocate"){
                elements_SetupManagementPage.getInput_ToAllocate().click()
            }
            elements_Generic.getFiltered_Result().contains(weekPlus).click()
            elements_Generic.getButton_Save().click()
            if(elements_Generic.getTitle_Confirmation()){
                elements_Generic.getButton_Yes().click()
                this.checktoasts(toast)
            }else cy.log("Confirmation modal was not displayed")
        }
        // Backend validation using endpoint response
        cy.getAccessToken().then(token => {
            cy.request({
                method: 'GET',
                url: 'https://dev-dot-gca-back-dot-dataservices-non-prod.appspot.com/setupManagement',
                headers: {
                    Authorization: token,
                    'Content-Type': 'application/json',
                },
                includeCredentials: true
            }).then(response => {
                if(feature == "toBook"){
                    var back_content = response.body.rows[0].toBook[0]
                }else if(feature == "toAllocate"){
                    var back_content = response.body.rows[0].toAllocate[0]
                }
                expect(response.status).to.eq(200);
                //const toBook_content = response.body.rows[0].toBook[0]
                const falseValues = Object.values(back_content).filter(back_content => back_content === false);
                cy.log(falseValues)
                if(weekPlus == 'Week +0'){
                    if(falseValues.length > 1){
                        expect(falseValues[0]).to.equal(false)
                        expect(falseValues[1]).to.equal(false)
                    }else if(falseValues.length == 1){
                        expect(falseValues[0]).to.equal(false)
                    }else {
                        expect(falseValues).to.be.empty
                    }
                }
                else if(weekPlus == 'Week +1'){
                    if(falseValues.length > 1){
                        expect(falseValues[0]).to.equal(false)
                        expect(falseValues[1]).to.equal(false)
                    }else if(falseValues.length == 1){
                        expect(falseValues[0]).to.equal(false)
                    }else {
                        expect(falseValues).to.be.empty
                    }
                }
                else if(weekPlus == 'Week +2'){
                    if(falseValues.length > 1){
                        expect(falseValues[0]).to.equal(false)
                        expect(falseValues[1]).to.equal(false)
                    }else if(falseValues.length == 1){
                        expect(falseValues[0]).to.equal(false)
                    }else {
                        expect(falseValues).to.be.empty
                    }
                }
                else{
                    cy.log('Week plus selected not exist')
                }
            })
        })
        // Frontend validation using another endpoint with rule to show check icon on page
        cy.getAccessToken().then(token => {
            cy.request({
                method: 'GET',
                url: 'https://dev-dot-gca-back-dot-dataservices-non-prod.appspot.com/setupManagement/currentSetup',
                headers: {
                    Authorization: token,
                    'Content-Type': 'application/json',
                },
                includeCredentials: true
            }).then(response => {
                if(feature == "toBook"){
                    var front_content = response.body.toBook.split(',')
                    var row = 2
                }else if(feature == "toAllocate"){
                    var front_content = response.body.toAllocate.split(',')
                    var row = 3
                }
                expect(response.status).to.eq(200);
                //const toBook_content = response.body.toBook.split(',')
                const trimmedArray = front_content.map(element => element.replace(/\s/g, ""))
                cy.log(trimmedArray)
                var weeks_format_list = []
                for(var i = 0; i<trimmedArray.length; i++){
                    const weeks_format = trimmedArray[i].replace('Wk', 'week')
                    weeks_format_list.push(weeks_format.replace('.','-'))
                }
                cy.log(weeks_format_list)
                for(var j =0; j<weeks_format_list.length; j++){
                    elements_SetupManagementPage.getCheckIconSetup(row,weeks_format_list[j]).should('be.visible')
                }
            })
        })
        this.backtoHomePage()
    }

    checkEditCurrentEnteredSetup(month,toast){
        if (this.openSetupManagementPage()){
            elements_SetupManagementPage.getButton_Edit_Current_Setup().click()
            elements_SetupManagementPage.getInput_EnteredSetup().click()
            elements_Generic.getFiltered_Result().contains(month).click({force:true})
            elements_Generic.getButton_Save().click({force:true})
            if(elements_Generic.getTitle_Confirmation()){
                elements_Generic.getButton_Yes().click()
                this.checktoasts(toast)
            }else cy.log("Confirmation modal was not displayed")
        }
        cy.getAccessToken().then(token =>{
            cy.request({
                method: 'GET',
                url: 'https://dev-dot-gca-back-dot-dataservices-non-prod.appspot.com/setupManagement/enteredSetup',
                headers: {
                    Authorization: token,
                    'Content-Type': 'application/json',
                },
                includeCredentials: true
            }).then(response => {
                expect(response.status).to.eq(200);
                cy.log(response.body)
                const headers_obj = response.body.headers
                var monthChecked_list = []
                for (var i = 0; i<headers_obj.length; i++){
                    if (headers_obj[i].entered == true){
                        monthChecked_list.push(headers_obj[i].key)
                    }
                }
                for(var j = 0; j<monthChecked_list.length; j++){
                    if(monthChecked_list[j] == month.toLowerCase()){
                        elements_SetupManagementPage.getCheckIconEntered(monthChecked_list[j]).should('be.visible')
                    }
                }
                //finished test with month selected not enable
                elements_SetupManagementPage.getButton_Edit_Current_Setup().click()
                elements_SetupManagementPage.getInput_EnteredSetup().click()
                elements_Generic.getFiltered_Result().contains(month).click({force:true})
                elements_Generic.getButton_Save().click({force:true})
                if(elements_Generic.getTitle_Confirmation()){
                    elements_Generic.getButton_Yes().click()
                    this.checktoasts(toast)
                }else cy.log("Confirmation modal was not displayed")
            })
        })
    }
    checktoasts(expectedToast){
        cy.wait(2000)
        elements_Generic.getToast().then((toast)=>{
            switch (expectedToast) {
                case "UpdateSetupManagementSuccessMessage":
                    cy.checkToast("Updated Setup", "Successfully!")

                default:
                    console.log("Unknown Toast");
                    break;
            }
        })
    }

    getCurrentQuarter_RangeDate(){
        return new Cypress.Promise((data)=>{
            cy.getAccessToken().then(token => {
                cy.request({
                    method: 'GET',
                    url: 'https://dev-dot-gca-back-dot-dataservices-non-prod.appspot.com/setupManagement/currentSetup',
                    headers: {
                        Authorization: token,
                        'Content-Type': 'application/json',
                    },
                    includeCredentials: true
                }).then((response) => {
                    cy.log(response)
                    expect(response.status).to.eq(200);
                    cy.wrap(response.body).as('payload')
                })
                cy.get('@payload').then((res)=>{
                    
                    var list_date = [
                        res.firstDayQuarter,
                        res.lastDayQuarter
                    ]
                    data(list_date)
                })
            })
        })
    }
}