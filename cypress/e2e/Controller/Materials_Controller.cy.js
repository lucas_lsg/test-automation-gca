import ElementsMaterialsPage from '../View/ElementsMaterialsPage.cy.js'
import ElementsHomePage from '../View/ElementsHomePage.cy.js'
import ElementsGeneric from '../View/ElementsGeneric.cy.js'
let elements_MaterialsPage = new ElementsMaterialsPage;
let elements_homePage = new ElementsHomePage;
let elements_Generic = new ElementsGeneric;

export default class Materials_Controller{
    openMaterialsPage(){
        if(elements_homePage.getTitle_Page()){
            elements_homePage.getCard_Setup().click()
            elements_homePage.getCard_SetupMaterials().click()
            return true
        }else cy.log('Not able to access home page')
    }
    backtoHomePage(){
        elements_Generic.getNavLinkHome().click({force:true})
    }
    checkElements(){
        this.openMaterialsPage()
        elements_Generic.getNav_Bar()
        elements_MaterialsPage.getTitle_Page()
        elements_MaterialsPage.getInput_ExportIcon()
        elements_MaterialsPage.getInput_Geo()
        elements_MaterialsPage.getInput_Country()
        elements_MaterialsPage.getInput_InternalName()
        elements_MaterialsPage.getInput_SalesModel()
        elements_MaterialsPage.getInput_Origin()
        elements_MaterialsPage.getLink_AllFilters().click()
        elements_MaterialsPage.getInput_Status()
        elements_MaterialsPage.getInput_Ean()
        elements_MaterialsPage.getInput_ColorDesc()
        elements_MaterialsPage.getInput_MarketName()
        elements_MaterialsPage.getInput_AllocationBz()
        elements_MaterialsPage.getInput_ProductGroup()
        elements_MaterialsPage.getLink_LessFilters().click()
        elements_Generic.getGrid_Icon().click()
        this.backtoHomePage()
    }

    checkFilters(filters_list,internal_name,status,color_desc,market_name,product_group,origin,results_number){
        if(this.openMaterialsPage()){
            if(elements_MaterialsPage.getTitle_Page()){
                for(var i=0; i<filters_list.length;i++){
                    if(filters_list[i]=='INTERNAL_NAME'){
                        elements_MaterialsPage.getInput_InternalName().type(internal_name)
                        elements_Generic.getFiltered_Result().contains(internal_name).click()
                        cy.wait(3000)
                        for(var j=1; j<=results_number;j++){
                            elements_MaterialsPage.getInternalName_results(j).then((filter_result)=>{
                                if(filter_result[0].innerText == internal_name){
                                    expect(filter_result[0].innerText).equal(internal_name)
                                    cy.log('PASS - Internal name filter is working correctly')
                                }else{
                                    cy.log('FAIL - Internal name filter is not working correctly')
                                }
                            })
                        }
                        elements_MaterialsPage.getInput_InternalName().clear()
                    }

                    else if(filters_list[i]=='STATUS'){
                        elements_MaterialsPage.getInput_Status().click()
                        elements_Generic.getFiltered_Result().contains(status).click()
                        cy.wait(3000)
                        for(var j=1; j<=results_number;j++){
                            elements_MaterialsPage.getStatus_results(j).then((filter_result)=>{
                                if(filter_result[0].innerText == status){
                                    expect(filter_result[0].innerText).equal(status)
                                    cy.log('PASS - Status filter is working correctly')
                                }else{
                                    cy.log('FAIL - Status filter is not working correctly')
                                }
                            })
                        }
                    }

                    else if(filters_list[i]=='COLOR_DESC'){
                        elements_MaterialsPage.getLink_AllFilters().click()
                        elements_MaterialsPage.getInput_ColorDesc().type(color_desc)
                        elements_Generic.getFiltered_Result().contains(color_desc).click()
                        cy.wait(3000)
                        for(var j=1; j<=results_number;j++){
                            elements_MaterialsPage.getColorDesc_results(j).then((filter_result)=>{
                                if(filter_result[0].innerText == color_desc){
                                    expect(filter_result[0].innerText).equal(color_desc)
                                    cy.log('PASS - Color desc filter is working correctly')
                                }else{
                                    cy.log('FAIL - Color desc filter is not working correctly')
                                }
                            })
                        }
                        elements_MaterialsPage.getInput_ColorDesc().clear()
                    }

                    else if(filters_list[i]=='MARKET_NAME'){
                        elements_MaterialsPage.getInput_MarketName().type(market_name)
                        elements_Generic.getFiltered_Result().contains(market_name).click()
                        cy.wait(3000)
                        for(var j=1; j<=results_number;j++){
                            elements_MaterialsPage.getMarketName_results(j).then((filter_result)=>{
                                if(filter_result[0].innerText == market_name){
                                    expect(filter_result[0].innerText).equal(market_name)
                                    cy.log('PASS - Market name filter is working correctly')
                                }else{
                                    cy.log('FAIL - Market name filter is not working correctly')
                                }
                            })
                        }
                        elements_MaterialsPage.getInput_MarketName().clear()
                    }

                    else if(filters_list[i]=='PRODUCT_GROUP'){
                        elements_MaterialsPage.getInput_ProductGroup().type(product_group)
                        elements_Generic.getFiltered_Result().contains(product_group).click()
                        cy.wait(3000)
                        for(var j=1; j<=results_number;j++){
                            elements_MaterialsPage.getProductGroup_results(j).then((filter_result)=>{
                                if(filter_result[0].innerText == product_group){
                                    expect(filter_result[0].innerText).equal(product_group)
                                    cy.log('PASS - Product group filter is working correctly')
                                }else{
                                    cy.log('FAIL - Product group filter is not working correctly')
                                }
                            })
                        }
                        elements_MaterialsPage.getInput_ProductGroup().clear()
                    }

                    else if(filters_list[i]=='ORIGIN'){
                        elements_MaterialsPage.getInput_Origin().type(origin)
                        elements_Generic.getFiltered_Result().contains(origin).click()
                        cy.wait(3000)
                        for(var j=1; j<=results_number;j++){
                            elements_MaterialsPage.getOrigin_results(j).then((filter_result)=>{
                                if(filter_result[0].innerText == origin){
                                    expect(filter_result[0].innerText).equal(origin)
                                    cy.log('PASS - Origin filter is working correctly')
                                }else{
                                    cy.log('FAIL - Origin filter is not working correctly')
                                }
                            })
                        }
                    }
                }
            }
        }
    }

    checkNoResultsFound(product_name){
        if(this.openMaterialsPage()){
            if(elements_MaterialsPage.getTitle_Page()){
                elements_MaterialsPage.getInput_InternalName().type(`${product_name}{enter}`)
                cy.wait(3000)
                elements_Generic.checkNoResultsFound().then((message)=>{
                    expect(message[0].innerText).equal('No results were found')
                })
            }
        }
    }

    checkExport(file_name){
        if(this.openMaterialsPage()){
            if(elements_MaterialsPage.getTitle_Page()){
                // will pass if the file exist
                elements_MaterialsPage.getInput_ExportIcon().click()
                cy.readFile(file_name).should('exist')
            }
        }
    }

    updateMaterial(ean,allocationBz,localMarketName, localColorDesc, toast){
        if(this.openMaterialsPage()){
            if(elements_MaterialsPage.getTitle_Page()){
                elements_MaterialsPage.getLink_AllFilters().click()
                elements_MaterialsPage.getInput_Ean().type(ean)
                elements_Generic.getFiltered_Result().contains(ean).click()
                elements_MaterialsPage.getInput_AllocationBz().type(allocationBz)
                cy.wait(3000)
                elements_Generic.getIcon_Edit().click()
                if(elements_MaterialsPage.getTitle_EditModal()){
                    elements_MaterialsPage.getInput_LocalMarketName().click()
                    elements_MaterialsPage.getInput_LocalMarketName().clear()
                    elements_MaterialsPage.getInput_LocalMarketName().type(localMarketName)
                    cy.wait(500)
                    elements_MaterialsPage.getInput_LocalColorDesc().click()
                    elements_MaterialsPage.getInput_LocalColorDesc().clear()
                    elements_MaterialsPage.getInput_LocalColorDesc().type(localColorDesc)
                }
                cy.wait(600)
                elements_Generic.getButton_Save().click()
                if(elements_Generic.getTitle_Confirmation()){
                    elements_Generic.getButton_Yes().click()
                    this.checktoasts(toast)
                }else cy.log("Confirmation _Modal was not displayed")
            }
        }
        this.backtoHomePage()
    }
   
    checktoasts(expectedToast){
        cy.wait(2000)
        elements_Generic.getToast().then((toast)=>{
            switch (expectedToast) {
               
                case "updateMaterial_success":
                    cy.checkToast("Updated Material", "Successfully!")
                    break;
                case "channel_blank_failed":
                    cy.checkToast("Channel","Mandatory field")
                    break;
                case "description_blank_failed":
                    cy.checkToast("Description","Mandatory field")
                    break;
                case "country_blank_failed":
                    cy.checkToast("Country","Mandatory field")
                    break;
                case "newChannel_duplicate_failed":
                    cy.checkToast("CHANNEL already exists","Please inform another CHANNEL!")
                    break;
                case "updateChannel_blank_failed":
                    cy.checkToast("Channel","Mandatory field")
                    break;
                case "updateChannel_duplicate_failed":
                    cy.checkToast("Channel already exists","Please inform another Channel!")
                    break;
                case "Channel_associated_failed":
                    cy.checkToast("CUSTOMER_SHORTS associated with this CHANNEL","Please remove it before delete to complete this operation!")
                break;
                default:
                    console.log("Unknown Toast");
                    break;
            }
        })
    }
    checkNotFoundChannel(channel){
        if(this.openChannelPage()){
            if(elements_MaterialsPage.getTitle_Page()){
                elements_Generic.getButtonClearFilter().click()
                elements_MaterialsPage.getInputList_Channel().click()
                elements_MaterialsPage.getInputList_Channel().type(channel).type('{enter}')
                cy.wait(600)
                elements_MaterialsPage.checkNoResultsFound().contains("No results were found")
            }
        }
    }
}