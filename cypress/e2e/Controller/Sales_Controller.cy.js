
import ElementSetupSalesPage from '../View/ElementsSetupSalesPage.cy.js'
import ElementsHomePage from '../View/ElementsHomePage.cy.js'
import ElementsGeneric from '../View/ElementsGeneric.cy.js'
let elements_SetupSalesPage = new ElementSetupSalesPage;
let elements_homePage = new ElementsHomePage;
let elements_Generic = new ElementsGeneric;

export default class SetupSales_Controller{
    openSetupSalesPage(){ 
        if(elements_homePage.getTitle_Page()){
            elements_homePage.getCard_Setup().click()
            elements_homePage.getCard_SetupSalesManag().click()
            return true
        }else cy.log('Not able to access home page')     
    }
    backtoHomePage(){
        elements_Generic.getNavLinkHome().click()
    }
    checkElements(){
        this.openSetupSalesPage()
        elements_Generic.getNav_Bar()
        elements_SetupSalesPage.getTitle_Page()
        elements_SetupSalesPage.getInputList_Geo()
        elements_SetupSalesPage.getInputList_Country()
        this.backtoHomePage()
    }
}