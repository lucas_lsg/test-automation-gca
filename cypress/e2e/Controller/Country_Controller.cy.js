import ElementCountryPage from '../View/ElementsCountryPage.cy.js'
import ElementsHomePage from '../View/ElementsHomePage.cy.js'
import ElementsGeneric from '../View/ElementsGeneric.cy.js'
let elements_CountryPage = new ElementCountryPage;
let elements_homePage = new ElementsHomePage;
let elements_Generic = new ElementsGeneric;

export default class Country_Controller{
    openCountryPage(){

        if(elements_homePage.getTitle_Page()){
            elements_homePage.getCard_Setup().click()
            elements_homePage.getCard_SetupCountry().click()
            return true
        }else cy.log('Not able to access home page')

    }
    backtoHomePage(){
        elements_Generic.getNavLinkHome().click({force:true})
    }
    checkElements(){
        this.openCountryPage()
        elements_Generic.getNav_Bar()
        elements_CountryPage.getTitle_Page()
        elements_CountryPage.getInputList_Geo()
        elements_CountryPage.getInputList_Country()
        elements_CountryPage.getInputList_ISO_Code()
        elements_CountryPage.getButton_New_Country().click()
        elements_CountryPage.getTitle_Modal()
        elements_CountryPage.getInput_Geo_Modal()
        elements_CountryPage.getInput_IsoCode_Modal()
        elements_Generic.getButton_Save()
        elements_Generic.getButton_Cancel().click()
        this.backtoHomePage()
    }
    createNewCountry(country,geo,iso_code, toast){
        if(this.openCountryPage()){
            if(elements_CountryPage.getTitle_Page()){
                if (elements_CountryPage.getButton_New_Country().click()){
                    if(geo == " "){
                        elements_CountryPage.getInput_Country_Modal().type(country)
                        elements_CountryPage.getInput_IsoCode_Modal().type(iso_code)
                        elements_Generic.getButton_Save().click()
                    }else{
                        elements_CountryPage.getInput_Geo_Modal().type(geo)
                        elements_CountryPage.getInput_Country_Modal().type(country)
                        elements_CountryPage.getInput_IsoCode_Modal().type(iso_code)
                        elements_Generic.getButton_Save().click()
                    }
                    
                    this.checktoasts(toast)
                }
            }else cy.log('Not able to access country page')
        }
        this.backtoHomePage()
    }
    updateNewCountry(country, toast){
        if(this.openCountryPage()){
            if(elements_CountryPage.getTitle_Page()){
                elements_Generic.getButtonClearFilter().click()
                elements_Generic.getButtonClearFilter().click()
                elements_CountryPage.getInputList_Country().click()
                elements_CountryPage.getInputList_Country().type(country)
                cy.wait(600)
                elements_Generic.getFiltered_Result().contains(country).click()
                cy.wait(3000)
                elements_Generic.getIcon_Edit().click()
                elements_CountryPage.getInput_Country_Modal().type(' EDITED')
                elements_Generic.getButton_Save().click()
                if(elements_Generic.getTitle_Confirmation()){
                    elements_Generic.getButton_Yes().click()
                    this.checktoasts(toast)
                }else cy.log("Confirmation modal was not displayed")
            }
        }
        this.backtoHomePage()
    }
    deleteNewCountry(country, toast){
        if(this.openCountryPage()){
            if(elements_CountryPage.getTitle_Page()){
                elements_Generic.getButtonClearFilter().click()
                elements_Generic.getButtonClearFilter().click()
                elements_CountryPage.getInputList_Country().click()
                elements_CountryPage.getInputList_Country().type(country)
                cy.wait(600)
                elements_Generic.getFiltered_Result().contains(country).click()
                cy.wait(3000)
                elements_Generic.getIcon_Delete().click()
                if(elements_Generic.getTitle_Confirmation()){
                    elements_Generic.getButton_Yes().click()
                    this.checktoasts(toast)
                }else cy.log("Confirmation modal was not displayed")
            }
        }
        this.backtoHomePage()
    }
    checktoasts(expectedToast){
        cy.wait(2000)
        elements_Generic.getToast().then((toast)=>{
            switch (expectedToast) {
                case "newcountry_success":
                    cy.checkToast("New Country",  "Registered successfully")
                    break;
                case "updatecountry_success":
                    cy.checkToast("Updated Country",  "Successfully!")
                    break;
                case "deletecountry_success":
                    cy.checkToast("Country",  "Successfully removed!")
                    break;
                case "country_blank_failed":
                    cy.checkToast("Country",  "Mandatory field")
                    break;
                case "geo_blank_failed":
                    cy.checkToast("GEO",  "Mandatory field")
                    break;
                case "isoCode_blank_failed":
                    cy.checkToast("Iso Code",  "Mandatory field")
                    break;
                case "country_duplicate_failed":
                    cy.checkToast("COUNTRY already exists", "Please inform another COUNTRY!")
                    break;
                case "isoCode_duplicate_failed":
                    cy.checkToast("ISO CODE already exists", "Please inform another ISO CODE!")
                    break;
                case "updatecountry_blank_failed":
                    cy.checkToast("Country",  "Mandatory field")
                    break;
                case "updatecountry_duplicate_failed":
                    cy.checkToast("COUNTRY already exists", "Please inform another COUNTRY!")
                    break;
                case "deletecountry_associated_channel_failed":
                    cy.checkToast("CHANNEL associated with this COUNTRY", "Please remove it before delete to complete this operation!")
                    break;
                default:
                    console.log("Unknown Toast");
                    break;
            }
        })
    }

    checkNotFoundCountry(country){
        if(this.openCountryPage()){
            if(elements_CountryPage.getTitle_Page()){
                elements_Generic.getButtonClearFilter().click()
                elements_CountryPage.getInputList_Country().click()
                elements_CountryPage.getInputList_Country().type(country).type('{enter}')
                cy.wait(600)
                elements_CountryPage.checkNoResultsFound().contains("No results were found")
            }
        }
    }
}