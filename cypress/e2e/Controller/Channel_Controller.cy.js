import ElementChannelPage from '../View/ElementsChannelPage.cy.js'
import ElementsHomePage from '../View/ElementsHomePage.cy.js'
import ElementsGeneric from '../View/ElementsGeneric.cy.js'
let elements_ChannelPage = new ElementChannelPage;
let elements_homePage = new ElementsHomePage;
let elements_Generic = new ElementsGeneric;

export default class Channel_Controller{
    openChannelPage(){
        if(elements_homePage.getTitle_Page()){
            elements_homePage.getCard_Setup().click()
            elements_homePage.getCard_SetupChannel().click()
            return true
        }else cy.log('Not able to access home page')
    }
    backtoHomePage(){
        elements_Generic.getNavLinkHome().click({force:true})
    }
    checkElements(){
        this.openChannelPage()
        elements_Generic.getNav_Bar()
        elements_ChannelPage.getTitle_Page()
        elements_ChannelPage.getInputList_Geo()
        elements_ChannelPage.getInputList_Country()
        elements_ChannelPage.getInputList_Channel()
        elements_ChannelPage.getButton_New_Channel().click()
        elements_ChannelPage.getTitle_Modal()
        elements_ChannelPage.getInput_Country_Modal()
        elements_ChannelPage.getInput_Channel_Modal()
        elements_ChannelPage.getInput_Description_Modal()
        elements_Generic.getButton_Save()
        elements_Generic.getButton_Cancel().click()
        this.backtoHomePage()
    }
    createNewChannel(country,channel,description, toast){
        if(this.openChannelPage()){
            if(elements_ChannelPage.getTitle_Page()){
                if (elements_ChannelPage.getButton_New_Channel().click()){
                    if(country == " "){
                        elements_ChannelPage.getInput_Channel_Modal().type(channel)
                        elements_ChannelPage.getInput_Description_Modal().type(description)
                    }else{
                        elements_ChannelPage.getInput_Country_Modal().click()
                        elements_ChannelPage.getInput_Country_Modal().type(country)
                        cy.wait(600)
                        elements_Generic.getFiltered_Result().contains(country).click()
                        elements_ChannelPage.getInput_Channel_Modal().type(channel)
                        elements_ChannelPage.getInput_Description_Modal().type(description)
                    }
                    elements_Generic.getButton_Save().click()
                    this.checktoasts(toast)
                }
            }else cy.log('Not able to access Channel page')
        }
        this.backtoHomePage()
    }
    updateNewChannel(oldcountry, country, channel, toast){
        if(this.openChannelPage()){
            if(elements_ChannelPage.getTitle_Page()){
                elements_Generic.getButtonClearFilter().click()
                elements_Generic.getButtonClearFilter().click()
                elements_ChannelPage.getInputList_Country().click()
                elements_ChannelPage.getInputList_Country().type(oldcountry)
                cy.wait(600)
                elements_Generic.getFiltered_Result().contains(oldcountry).click()
                elements_ChannelPage.getInputList_Channel().click()
                elements_ChannelPage.getInputList_Channel().type(channel)
                cy.wait(600)
                elements_Generic.getFiltered_Result().contains(channel).click()
                cy.wait(3000)
                elements_Generic.getIcon_Edit().click()
                elements_ChannelPage.getInput_Country_Modal().clear()
                elements_ChannelPage.getInput_Country_Modal().click()
                elements_ChannelPage.getInput_Country_Modal().type(country)
                cy.wait(600)
                elements_Generic.getFiltered_Result().contains(country).click()
                elements_ChannelPage.getInput_Channel_Modal().type(' EDITED')
                elements_ChannelPage.getInput_Description_Modal().type(' EDITED')
                elements_Generic.getButton_Save().click()
                if(elements_Generic.getTitle_Confirmation()){
                    elements_Generic.getButton_Yes().click()
                    this.checktoasts(toast)
                }else cy.log("Confirmation _Modal was not displayed")
            }
        }
        this.backtoHomePage()
    }
    deleteNewChannel(country, channel,toast){
        if(this.openChannelPage()){
            if(elements_ChannelPage.getTitle_Page()){
                elements_Generic.getButtonClearFilter().click()
                elements_Generic.getButtonClearFilter().click()
                elements_ChannelPage.getInputList_Country().click()
                elements_ChannelPage.getInputList_Country().type(country)
                cy.wait(600)
                elements_Generic.getFiltered_Result().contains(country).click()
                elements_ChannelPage.getInputList_Channel().click()
                elements_ChannelPage.getInputList_Channel().type(channel)
                cy.wait(600)
                elements_Generic.getFiltered_Result().contains(channel).click()
                cy.wait(3000)
                elements_Generic.getIcon_Delete().click()
                if(elements_Generic.getTitle_Confirmation()){
                    elements_Generic.getButton_Yes().click()
                    this.checktoasts(toast)
                }else cy.log("Confirmation _Modal was not displayed")
            }
        }
        this.backtoHomePage()
    }
    checktoasts(expectedToast){
        cy.wait(2000)
        elements_Generic.getToast().then((toast)=>{
            switch (expectedToast) {
                case "newChannel_success":
                    cy.checkToast("New Channel","Registered successfully")
                    break;
                case "updateChannel_success":
                    cy.checkToast("Updated Channel", "Successfully!")
                    break;
                case "deleteChannel_success":
                    cy.checkToast("Channel","Successfully removed!")
                    break;
                case "channel_blank_failed":
                    cy.checkToast("Channel","Mandatory field")
                    break;
                case "description_blank_failed":
                    cy.checkToast("Description","Mandatory field")
                    break;
                case "country_blank_failed":
                    cy.checkToast("Country","Mandatory field")
                    break;
                case "newChannel_duplicate_failed":
                    cy.checkToast("CHANNEL already exists","Please inform another CHANNEL!")
                    break;
                case "updateChannel_blank_failed":
                    cy.checkToast("Channel","Mandatory field")
                    break;
                case "updateChannel_duplicate_failed":
                    cy.checkToast("Channel already exists","Please inform another Channel!")
                    break;
                case "Channel_associated_failed":
                    cy.checkToast("CUSTOMER_SHORTS associated with this CHANNEL","Please remove it before delete to complete this operation!")
                break;
                default:
                    console.log("Unknown Toast");
                    break;
            }
        })
    }
    checkNotFoundChannel(channel){
        if(this.openChannelPage()){
            if(elements_ChannelPage.getTitle_Page()){
                elements_Generic.getButtonClearFilter().click()
                elements_ChannelPage.getInputList_Channel().click()
                elements_ChannelPage.getInputList_Channel().type(channel).type('{enter}')
                cy.wait(600)
                elements_ChannelPage.checkNoResultsFound().contains("No results were found")
            }
        }
    }
}