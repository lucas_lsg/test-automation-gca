import ElementsGeneric from '../View/ElementsGeneric.cy.js'
let elements_Generic = new ElementsGeneric;

export default class SignGoogle_Controller{
    signGooglePage(){
        cy.visit('/home')
        if(elements_Generic.getTitleSignGoogle()){
            elements_Generic.getButtonSignGoogle().click()
        }
    }
}