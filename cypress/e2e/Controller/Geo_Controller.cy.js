import ElementGeoPage from '../View/ElementsGeoPage.cy.js'
import ElementsHomePage from '../View/ElementsHomePage.cy.js'
import ElementsGeneric from '../View/ElementsGeneric.cy.js'
let elements_GeoPage = new ElementGeoPage;
let elements_homePage = new ElementsHomePage;
let elements_Generic = new ElementsGeneric;

export default class Geo_Controller{
    openGeoPage(){
        if(elements_homePage.getTitle_Page()){
            elements_homePage.getCard_Setup().click()
            elements_homePage.getCard_SetupGeo().click()
            return true
        }else cy.log('Not able to access home page')
    }
    backtoHomePage(){
        elements_Generic.getNavLinkHome().click({force:true})
    }
    checkElements(){
        this.openGeoPage()
        elements_Generic.getNav_Bar()
        elements_GeoPage.getTitle_Page()
        elements_GeoPage.getInputList_Geo()
        elements_GeoPage.getButton_New_Geo().click()
        elements_GeoPage.getTitle_Modal()
        elements_GeoPage.getInput_Geo_Modal()
        elements_Generic.getButton_Save()
        elements_Generic.getButton_Cancel().click()
        this.backtoHomePage()
    }
    createNewGeo(geo, toast){
        this.openGeoPage()
        if(elements_GeoPage.getTitle_Page()){
            if (elements_GeoPage.getButton_New_Geo().click()){
                elements_GeoPage.getInput_Geo_Modal().click()
                elements_GeoPage.getInput_Geo_Modal().type(geo)
                elements_Generic.getButton_Save().click()
                this.checktoasts(toast)
            }
        }else cy.log('Not able to access geo page')
        this.backtoHomePage()
    }
    updateNewGeo(geo, toast){
        this.openGeoPage()
        if(elements_GeoPage.getTitle_Page()){
            elements_Generic.getButtonClearFilter().click()
            elements_GeoPage.getInputList_Geo().click()
            elements_GeoPage.getInputList_Geo().type(geo)
            cy.wait(600)
            elements_Generic.getFiltered_Result().contains(geo).click()
            cy.wait(3000)
            elements_Generic.getIcon_Edit().click()
            elements_GeoPage.getInput_Geo_Modal().type(' EDITED')
            elements_Generic.getButton_Save().click()
            if(elements_Generic.getTitle_Confirmation()){
                elements_Generic.getButton_Yes().click()
                this.checktoasts(toast)
            }else cy.log("Confirmation modal was not displayed")
        }
        this.backtoHomePage()
    }
    deleteNewGeo(geo,toast){
        this.openGeoPage()
        if(elements_GeoPage.getTitle_Page()){
            elements_Generic.getButtonClearFilter().click()
            elements_GeoPage.getInputList_Geo().click()
            elements_GeoPage.getInputList_Geo().type(geo)
            cy.wait(600)
            elements_Generic.getFiltered_Result().contains(geo).click()
            cy.wait(3000)
            elements_Generic.getIcon_Delete().click()
            if(elements_Generic.getTitle_Confirmation()){
                elements_Generic.getButton_Yes().click()
                this.checktoasts(toast)
            }else cy.log("Confirmation modal was not displayed")
        }
        this.backtoHomePage()
    }
    checktoasts(expectedToast){
        cy.wait(2000)
        elements_Generic.getToast().then((toast)=>{
            switch (expectedToast) {
                case "newgeo_success":
                    cy.checkToast("New Geo","Registered successfully")
                    break;
                case "updategeo_success":
                    cy.checkToast("Updated Geo","Successfully!")
                    break;
                case "deletegeo_success":
                    cy.checkToast("Geo","Successfully removed!")
                    break;
                case "newgeo_blank_failed":
                    cy.checkToast("GEO","Mandatory field")
                    break;
                case "newgeo_duplicate_failed":
                    cy.checkToast("GEO already exists","Please inform another GEO!")
                    break;
                case "updategeo_blank_failed":
                    cy.checkToast("GEO","Mandatory field")
                    break;
                case "updategeo_duplicate_failed":
                    cy.checkToast("GEO already exists","Please inform another GEO!")
                    break;
                case "deletegeo_country_associated_failed":
                    cy.checkToast("COUNTRY associated with this GEOS","Please remove it before delete to complete this operation!")
                    break;
                default:
                    console.log("Unknown Toast");
                    break;
            }
        })
    }

    checkNotFoundProfile(geo){
        if(this.openGeoPage()){
            if(elements_GeoPage.getTitle_Page()){
                elements_Generic.getButtonClearFilter().click()
                elements_GeoPage.getInputList_Geo().click()
                elements_GeoPage.getInputList_Geo().type(geo).type('{enter}')
                cy.wait(600)
                elements_GeoPage.checkNoResultsFound().contains("No results were found")
            }
        }
    }
}