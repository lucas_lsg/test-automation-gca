import ElementCustomerPage from '../View/ElementsCustomerPage.cy.js'
import ElementsHomePage from '../View/ElementsHomePage.cy.js'
import ElementsGeneric from '../View/ElementsGeneric.cy.js'
let elementsCustomerPage = new ElementCustomerPage;
let elements_homePage = new ElementsHomePage;
let elements_Generic = new ElementsGeneric;

export default class SetupCustomer_Controller{
    openSetupCustomerPage(){
        if(elements_homePage.getTitle_Page()){
            elements_homePage.getCard_Setup().click()
            elements_homePage.getCard_SetupCustomerSettings().click()
            return true
        }else cy.log('Not able to access home page')
    }
    backtoHomePage(){
        elements_Generic.getNavLinkHome().click({force:true})
    }
    checkElements(){
        this.openSetupCustomerPage()
        elements_Generic.getNav_Bar()
        elementsCustomerPage.getTitle_Page()
        elementsCustomerPage.getInputList_Geo()
        elementsCustomerPage.getInputList_Country()
        elementsCustomerPage.getSelectList_Status()
        elementsCustomerPage.getInputList_CustomerShort()
        elementsCustomerPage.getButton_NewCustomer().click()
        elementsCustomerPage.getInput_CustomerShort_Modal()
        elementsCustomerPage.getInput_CustomerPOLineNumber_Modal()
        elementsCustomerPage.getSelect_Channel_Modal()
        elementsCustomerPage.getSelect_Country_Modal()
        elementsCustomerPage.getSelect_OrderAdmin_Modal()
        elementsCustomerPage.getSelect_SalesManager_Modal()
        elementsCustomerPage.getSelect_Status_Modal()
        elementsCustomerPage.getSelect_TaxesApplication_Modal()
        elementsCustomerPage.getSelect_CustomerNumbers_Modal()
        elements_Generic.getButton_Cancel().click()
        elementsCustomerPage.getLink_CustomerShort()
        elementsCustomerPage.getLink_CustomerList().click()
        elementsCustomerPage.getInputList_CustomerNumber()
        elementsCustomerPage.getInputList_CustomerShort()
        elementsCustomerPage.getInputList_CustomerName()
        elementsCustomerPage.getBtn_All().click()
        elementsCustomerPage.getBtn_Associated().click()
        elementsCustomerPage.getBtn_NotAssociated().click()
        this.backtoHomePage()
    }
    createNewCustomerShort(customerShort, customerPoLineNumber, channel, country, status, taxesApplicationList, toast){
        if(this.openSetupCustomerPage()){
            if(elementsCustomerPage.getTitle_Page()){
                if (elementsCustomerPage.getButton_NewCustomer().click()){
                    if(customerShort == ''){
                        elementsCustomerPage.getInput_CustomerPOLineNumber_Modal().type(customerPoLineNumber)
                        elementsCustomerPage.getSelect_Channel_Modal().click({force:true})
                        elements_Generic.getMultiSelect_Search().type(channel)
                        elements_Generic.getFiltered_Result().contains(channel).click({force:true})
                        cy.wait(1000)
                        elementsCustomerPage.getSelect_Country_Modal().click({force:true})
                        elements_Generic.getMultiSelect_Search().type(country)
                        cy.wait(1000)
                        elementsCustomerPage.getSelect_Status_Modal().click({force:true})
                        elements_Generic.getFiltered_Result().contains(status).click()
                        for(var i = 0 ; i < taxesApplicationList.length ; i ++){
                            elementsCustomerPage.getSelect_TaxesApplication_Modal().click()
                            cy.wait(600)
                            elements_Generic.getFiltered_Result().contains(taxesApplicationList[i].toString()).click()
                            cy.get('body').trigger('keydown', { keyCode: 27})
                            cy.wait(100);
                            cy.get('body').trigger('keyup', { keyCode: 27})
                        }
                        if(elements_Generic.getButton_Save().click()){
                            this.checktoasts(toast)
                        }
                        
                    }else if(channel == ''){
                        elementsCustomerPage.getInput_CustomerShort_Modal().type(customerShort)
                        elementsCustomerPage.getInput_CustomerPOLineNumber_Modal().type(customerPoLineNumber)
                        cy.wait(600)
                        elementsCustomerPage.getSelect_Country_Modal().click()
                        elements_Generic.getMultiSelect_Search().type(country)
                        cy.wait(600)
                        elementsCustomerPage.getSelect_Country_Modal().click()
                        elements_Generic.getFiltered_Result().contains(country).click({force:true})
                        elementsCustomerPage.getSelect_Status_Modal().click()
                        elements_Generic.getFiltered_Result().contains(status).click()
                        for(var i = 0 ; i < taxesApplicationList.length ; i ++){
                            elementsCustomerPage.getSelect_TaxesApplication_Modal().click()
                            cy.wait(600)
                            elements_Generic.getFiltered_Result().contains(taxesApplicationList[i].toString()).click()
                            cy.get('body').trigger('keydown', { keyCode: 27})
                            cy.wait(100);
                            cy.get('body').trigger('keyup', { keyCode: 27})
                        }
                        if(elements_Generic.getButton_Save().click()){
                            this.checktoasts(toast)
                        }
                    }else if(taxesApplicationList == ''){
                        elementsCustomerPage.getInput_CustomerShort_Modal().type(customerShort)
                        elementsCustomerPage.getInput_CustomerPOLineNumber_Modal().type(customerPoLineNumber)
                        elementsCustomerPage.getSelect_Channel_Modal().click({force:true})
                        elements_Generic.getMultiSelect_Search().type(channel)
                        cy.wait(1000)
                        elementsCustomerPage.getSelect_Country_Modal().click({force:true})
                        elements_Generic.getMultiSelect_Search().type(country)
                        cy.wait(1000)
                        elementsCustomerPage.getSelect_Status_Modal().click({force:true})
                        elements_Generic.getFiltered_Result().contains(status).click()
                        if(elements_Generic.getButton_Save().click()){
                            this.checktoasts(toast)
                        }
                    }else if(status == ''){
                        elementsCustomerPage.getInput_CustomerShort_Modal().type(customerShort)
                        elementsCustomerPage.getInput_CustomerPOLineNumber_Modal().type(customerPoLineNumber)
                        elementsCustomerPage.getSelect_Channel_Modal().click({force:true})
                        elements_Generic.getMultiSelect_Search().type(channel)
                        elements_Generic.getFiltered_Result().contains(channel).click({force:true})
                        cy.wait(1000)
                        elementsCustomerPage.getSelect_Country_Modal().click({force:true})
                        elements_Generic.getMultiSelect_Search().type(country)
                        cy.wait(1000)
                        elementsCustomerPage.getSelect_Country_Modal().click({force:true})
                        elements_Generic.getFiltered_Result().contains(country).click({force:true})
                        for(var i = 0 ; i < taxesApplicationList.length ; i ++){
                            elementsCustomerPage.getSelect_TaxesApplication_Modal().click()
                            cy.wait(600)
                            elements_Generic.getFiltered_Result().contains(taxesApplicationList[i].toString()).click()
                            cy.get('body').trigger('keydown', { keyCode: 27})
                            cy.wait(100);
                            cy.get('body').trigger('keyup', { keyCode: 27})
                        }
                        if(elements_Generic.getButton_Save().click()){
                            this.checktoasts(toast)
                        }
                    }else {
                        elementsCustomerPage.getInput_CustomerShort_Modal().type(customerShort)
                        elementsCustomerPage.getInput_CustomerPOLineNumber_Modal().type(customerPoLineNumber)
                        elementsCustomerPage.getSelect_Channel_Modal().click({force:true})
                        elements_Generic.getMultiSelect_Search().type(channel)
                        cy.wait(1000)
                        elementsCustomerPage.getSelect_Country_Modal().click({force:true})
                        elements_Generic.getMultiSelect_Search().type(country)
                        elements_Generic.getFiltered_Result().contains(country).click()
                        cy.wait(1000)
                        
                        elementsCustomerPage.getSelect_Status_Modal().click({force:true})
                        elements_Generic.getFiltered_Result().contains(status).click()
                        for(var i = 0 ; i < taxesApplicationList.length ; i ++){
                            elementsCustomerPage.getSelect_TaxesApplication_Modal().click()
                            cy.wait(600)
                            elements_Generic.getFiltered_Result().contains(taxesApplicationList[i].toString()).click()
                            cy.get('body').trigger('keydown', { keyCode: 27})
                            cy.wait(100);
                            cy.get('body').trigger('keyup', { keyCode: 27})
                        }
                        elements_Generic.getButton_Save().click()
                        if(elements_Generic.getTitle_Confirmation()){
                            elements_Generic.getButton_Yes().click()
                            this.checktoasts(toast)
                        }else cy.log("Confirmation modal was not displayed")
                    }
                }
            }else cy.log('Not able to access Customer page')
        }
        this.backtoHomePage()
    }

    updateCustomerShort(customerShort, toast){
        if(this.openSetupCustomerPage()){
            if(elementsCustomerPage.getTitle_Page()){
                if (elementsCustomerPage.getLink_CustomerShort()){
                    elementsCustomerPage.getInputList_CustomerShort().type(customerShort)
                    elements_Generic.getFiltered_Result().click()
                    cy.wait(600)
                    elementsCustomerPage.getEdit_Icon().click()
                    if(elementsCustomerPage.getTitle_EditModal()){
                        elementsCustomerPage.getInput_CustomerShort_Modal().clear()
                        elementsCustomerPage.getInput_CustomerShort_Modal().type(customerShort+" EDITED")
                        cy.wait(600)
                    }
                    elements_Generic.getButton_Save().click()
                    if(elements_Generic.getTitle_Confirmation()){
                        elements_Generic.getButton_Yes().click()
                        this.checktoasts(toast)
                    }else cy.log("Confirmation modal was not displayed")
                }
            }else cy.log('Not able to access Customer page')
        }
        this.backtoHomePage()
    }

    checkCustomerListFilters(filters_list,geo,country,customer_number,customer_short,customer_name,results_number){
        if(this.openSetupCustomerPage()){
            if(elementsCustomerPage.getTitle_Page()){
                elementsCustomerPage.getLink_CustomerList().click()
                cy.wait(300)
                elementsCustomerPage.getBtn_All().click()
                cy.wait(300)
                for (var i = 0; i < filters_list.length; i++){
                    for(var j=1; j<=results_number;j++){
                        if(filters_list[i] == 'GEO'){
                            elementsCustomerPage.getInputList_Geo().click()
                            elements_Generic.getFiltered_Result().contains(geo).click()
                            cy.wait(600)
                            elementsCustomerPage.getGeo_Table(j).then((filtered_value)=>{
                                expect(filtered_value[0].innerText).equal(geo)
                                cy.log('PASS - GEO filter displayed default value correctly')
                            })
                        }
                        else if(filters_list[i] == 'COUNTRY'){
                            elementsCustomerPage.getInputList_Country().click()
                            elements_Generic.getFiltered_Result().contains(country).click()
                            cy.wait(600)
                            elementsCustomerPage.getCountry_Table(j).then((filtered_value)=>{
                                expect(filtered_value[0].innerText).equal(country)
                                cy.log('PASS - COUNTRY filter displayed default value correctly')
                            })
                        }
                        else if(filters_list[i] == 'CUSTOMER_NUMBER'){
                            elementsCustomerPage.getInputList_CustomerNumber().type(customer_number)
                            elements_Generic.getFiltered_Result().contains(customer_number).click()
                            cy.wait(600)
                            elementsCustomerPage.getCustomerNumber_Table(j).then((filtered_value)=>{
                                expect(filtered_value[0].innerText).equal(customer_number)
                                cy.log('PASS - CUSTOMER_NUMBER filter is working correctly')
                            })
                        }
                        else if(filters_list[i] == 'CUSTOMER_SHORT'){
                            elementsCustomerPage.getInputList_CustomerShort().type(customer_short)
                            elements_Generic.getFiltered_Result().contains(customer_short).click()
                            cy.wait(600)
                            elementsCustomerPage.getCollapse_icon().first().click()
                            cy.wait(600)
                            elementsCustomerPage.getCustomerShort_Table(j).then((filtered_value)=>{
                                expect(filtered_value[0].innerText).equal(customer_short)
                                cy.log('PASS - CUSTOMER_SHORT filter is working correctly')
                            })
                        }
                        else if(filters_list[i] == 'CUSTOMER_NAME'){
                            elementsCustomerPage.getInputList_CustomerName().type(customer_name)
                            elements_Generic.getFiltered_Result().contains(customer_name).click()
                            cy.wait(600)
                            elementsCustomerPage.getCustomerName_Table(j).then((filtered_value)=>{
                                expect(filtered_value[0].innerText).equal(customer_name)
                                cy.log('PASS - CUSTOMER_NAME filter is working correctly')
                            })
                        }else cy.log("Not received a filter list")
                    }
                }
            }
        }
    }

    checktoasts(expectedToast){
        cy.wait(2000)
        elements_Generic.getToast().then((toast)=>{
            switch (expectedToast) {
                case "createCustomer_success":
                    cy.checkToast("New Association", "Registered successfully")
                    break;
                case "updateCustomer_success":
                    cy.checkToast("Updated Association", "Successfully!")
                    break;
                case "customerShortMandatoryfield":
                    cy.checkToast("Customer Short", "Mandatory field")
                    break;
                case "channelMandatoryfield":
                    cy.checkToast("Channel", "Mandatory field")
                    break;
                case "taxesMandatoryfield":
                    cy.checkToast("Taxes", "Mandatory field")
                    break;
                case "statusMandatoryfield":
                    cy.checkToast("Status", "Mandatory field")
                    break;
                case "insertInvalidData":
                    cy.checkToast("Internal error", "Invalid data on the Request!")
                    break;
                default:
                    console.log("Unknown Toast");
                    break;
            }
        })
    }
}