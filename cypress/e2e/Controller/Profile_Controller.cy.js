import ElementProfilePage from '../View/ElementsProfilePage.cy.js'
import ElementsHomePage from '../View/ElementsHomePage.cy.js'
import ElementsGeneric from '../View/ElementsGeneric.cy.js'
let elements_ProfilePage = new ElementProfilePage;
let elements_homePage = new ElementsHomePage;
let elements_Generic = new ElementsGeneric;

export default class Profile_Controller{
    openProfilePage(){
        if(elements_homePage.getTitle_Page()){
            elements_homePage.getCard_Admin().click()
            elements_homePage.getCard_AdminProfile().click()
            return true
        }else cy.log('Not able to access home page')
    }
    backtoHomePage(){
        elements_Generic.getNavLinkHome().click({force:true})
    }
    checkElements(){
        this.openProfilePage()
        elements_Generic.getNav_Bar()
        elements_ProfilePage.getTitle_Page()
        elements_ProfilePage.getInputList_Profile()
        elements_ProfilePage.getSelectList_Status()
        elements_ProfilePage.getInputList_Geo()
        elements_ProfilePage.getInputList_Country()
        elements_ProfilePage.getButton_New_Profile().click()
        elements_ProfilePage.getTitle_Modal()
        elements_ProfilePage.getInput_Profile_Modal()
        elements_ProfilePage.getSelect_Status_Modal()
        elements_ProfilePage.getSelectCountry_Modal()
        elements_ProfilePage.getTitle_PermModal_Modal()
        elements_ProfilePage.getTabAdministration_Modal()
        elements_ProfilePage.getItemProfile_Modal()
        elements_ProfilePage.getItemUser_Modal()
        elements_ProfilePage.getItemProfileCreate_Modal()
        elements_ProfilePage.getItemUserCreate_Modal()
        elements_ProfilePage.getItemProfileDelete_Modal()
        elements_ProfilePage.getItemUserDelete_Modal()
        elements_ProfilePage.getItemProfileUpdate_Modal()
        elements_ProfilePage.getItemUserUpdate_Modal()
        elements_ProfilePage.getItemProfileView_Modal()
        elements_ProfilePage.getItemUserView_Modal()
        elements_ProfilePage.getTabCustomerAlloc_Modal().click()
        elements_ProfilePage.getItemAllocationRetail_Modal()
        elements_ProfilePage.getItemBooked_Modal()
        elements_ProfilePage.getItemExecutiveAllocation_Modal()
        elements_ProfilePage.getItemMissing_Modal()
        elements_ProfilePage.getItemOverbooked_Modal()
        elements_ProfilePage.getItemToBook_Modal()
        elements_ProfilePage.getItemAllocationRetailCreate_Modal()
        elements_ProfilePage.getItemBookedCreate_Modal()
        elements_ProfilePage.getItemExecutiveAllocationCreate_Modal()
        elements_ProfilePage.getItemMissingCreate_Modal()
        elements_ProfilePage.getItemOverbookedCreate_Modal()
        elements_ProfilePage.getItemToBookCreate_Modal()
        elements_ProfilePage.getItemAllocationRetailDelete_Modal()
        elements_ProfilePage.getItemBookedDelete_Modal()
        elements_ProfilePage.getItemExecutiveAllocationDelete_Modal()
        elements_ProfilePage.getItemMissingDelete_Modal()
        elements_ProfilePage.getItemOverbookedDelete_Modal()
        elements_ProfilePage.getItemToBookDelete_Modal()
        elements_ProfilePage.getItemAllocationRetailUpdate_Modal()
        elements_ProfilePage.getItemBookedUpdate_Modal()
        elements_ProfilePage.getItemExecutiveAllocationUpdate_Modal()
        elements_ProfilePage.getItemMissingUpdate_Modal()
        elements_ProfilePage.getItemOverbookedUpdate_Modal()
        elements_ProfilePage.getItemToBookUpdate_Modal()
        elements_ProfilePage.getItemAllocationRetailView_Modal()
        elements_ProfilePage.getItemBookedView_Modal()
        elements_ProfilePage.getItemExecutiveAllocationView_Modal()
        elements_ProfilePage.getItemMissingView_Modal()
        elements_ProfilePage.getItemOverbookedView_Modal()
        elements_ProfilePage.getItemToBookView_Modal()
        elements_ProfilePage.getTabKaAlloc_Modal().click()
        elements_ProfilePage.getItemBrazilDemand_Modal()
        elements_ProfilePage.getItemCarriesInfo_Modal()
        elements_ProfilePage.getItemKaAllocation_Modal()
        elements_ProfilePage.getItemKaManagement_Modal()
        elements_ProfilePage.getItemLom_Modal()
        elements_ProfilePage.getItemBrazilDemandCreate_Modal()
        elements_ProfilePage.getItemCarriesInfoCreate_Modal()
        elements_ProfilePage.getItemKaManagementCreate_Modal()
        elements_ProfilePage.getItemLomCreate_Modal()
        elements_ProfilePage.getItemBrazilDemandDelete_Modal()
        elements_ProfilePage.getItemCarriesInfoDelete_Modal()
        elements_ProfilePage.getItemLomDelete_Modal()
        elements_ProfilePage.getItemBrazilDemandUpdate_Modal()
        elements_ProfilePage.getItemCarriesInfoUpdate_Modal()
        elements_ProfilePage.getItemKaAllocationUpdate_Modal()
        elements_ProfilePage.getItemKaManagementUpdate_Modal()
        elements_ProfilePage.getItemLomUpdate_Modal()
        elements_ProfilePage.getItemBrazilDemandView_Modal()
        elements_ProfilePage.getItemCarriesInfoView_Modal()
        elements_ProfilePage.getItemKaAllocationView_Modal()
        elements_ProfilePage.getItemKaManagementView_Modal()
        elements_ProfilePage.getItemLomView_Modal()
        elements_ProfilePage.getTabOnePlan_Modal().click()
        elements_ProfilePage.getItemOnePlanUpload_Modal()
        elements_ProfilePage.getItemFlexOnePlanVariation_Modal()
        elements_ProfilePage.getItemOnePlanCreate_Modal()
        elements_ProfilePage.getItemOnePlanVariationAnalysisCreate_Modal()
        elements_ProfilePage.getItemOnePlanDelete_Modal()
        elements_ProfilePage.getItemOnePlanVariationAnalysisDelete_Modal()
        elements_ProfilePage.getItemOnePlanUpdate_Modal()
        elements_ProfilePage.getItemOnePlanVariationAnalysisUpdate_Modal()
        elements_ProfilePage.getItemOnePlanView_Modal()
        elements_ProfilePage.getItemOnePlanVariationAnalysisView_Modal()
        elements_ProfilePage.getTabSetup_Modal().click()
        elements_ProfilePage.getItemChannel_Modal()
        elements_ProfilePage.getItemCountry_Modal()
        elements_ProfilePage.getItemCustomerManagement_Modal()
        elements_ProfilePage.getItemCustomerSettings_Modal().eq(0)
        elements_ProfilePage.getItemFiscalCalendar_Modal()
        elements_ProfilePage.getItemGeo_Modal()
        elements_ProfilePage.getItemOrderAdminManagement_Modal()
        elements_ProfilePage.getItemSalesManagement_Modal()
        elements_ProfilePage.getItemSetupManagement_Modal()
        elements_ProfilePage.getItemChannelCreate_Modal()
        elements_ProfilePage.getItemCountryCreate_Modal()
        elements_ProfilePage.getItemCustomerSettingsCreate_Modal().eq(0)
        elements_ProfilePage.getItemGeoCreate_Modal()
        elements_ProfilePage.getItemSetupManagementCreate_Modal()
        elements_ProfilePage.getItemChannelDelete_Modal()
        elements_ProfilePage.getItemCountryDelete_Modal()
        elements_ProfilePage.getItemGeoDelete_Modal()
        elements_ProfilePage.getItemOrderAdminManagementDelete_Modal()
        elements_ProfilePage.getItemSalesManagementDelete_Modal()
        elements_ProfilePage.getItemChannelUpdate_Modal()
        elements_ProfilePage.getItemCountryUpdate_Modal()
        elements_ProfilePage.getItemCustomerManagementUpdate_Modal()
        elements_ProfilePage.getItemCustomerSettingsUpdate_Modal().eq(0)
        elements_ProfilePage.getItemGeoUpdate_Modal()
        elements_ProfilePage.getItemOrderAdminManagementUpdate_Modal()
        elements_ProfilePage.getItemSalesManagementUpdate_Modal()
        elements_ProfilePage.getItemSetupManagementUpdate_Modal()
        elements_ProfilePage.getItemChannelView_Modal()
        elements_ProfilePage.getItemCountryView_Modal()
        elements_ProfilePage.getItemCustomerManagementView_Modal()
        elements_ProfilePage.getItemCustomerSettingsView_Modal().eq(0)
        elements_ProfilePage.getItemFiscalCalendarView_Modal()
        elements_ProfilePage.getItemGeoView_Modal()
        elements_ProfilePage.getItemOrderAdminManagementView_Modal()
        elements_ProfilePage.getItemSalesManagementView_Modal()
        elements_ProfilePage.getItemSetupManagementView_Modal()
        elements_Generic.getButton_Save()
        elements_Generic.getButton_Cancel().click()
        this.backtoHomePage()
    }
    createNewProfileAdministrationPermissions(profile, status, countrylist,toast){
        if(this.openProfilePage()){
            if(elements_ProfilePage.getTitle_Page()){
                if (elements_ProfilePage.getButton_New_Profile().click()){
                    elements_ProfilePage.getInput_Profile_Modal().click()
                    elements_ProfilePage.getInput_Profile_Modal().type(profile)
                    elements_ProfilePage.getSelect_Status_Modal().click()
                    cy.wait(600)
                    elements_Generic.getFiltered_Result().contains(status).click()
                    for(var i = 0 ; i < countrylist.length ; i ++){
                        elements_ProfilePage.getSelectCountry_Modal().click()
                        cy.wait(600)
                        elements_Generic.getFiltered_Result().contains(countrylist[i].toString()).click()
                        cy.get('body').trigger('keydown', { keyCode: 27})
                        cy.wait(100);
                        cy.get('body').trigger('keyup', { keyCode: 27})
                    }
                    elements_ProfilePage.getTabAdministration_Modal().click()
                    elements_ProfilePage.getItemProfileCreate_Modal().click()
                    elements_ProfilePage.getItemUserCreate_Modal().click()
                    elements_ProfilePage.getItemProfileDelete_Modal().click()
                    elements_ProfilePage.getItemUserDelete_Modal().click()
                    elements_ProfilePage.getItemProfileUpdate_Modal().click()
                    elements_ProfilePage.getItemUserUpdate_Modal().click()
                    elements_Generic.getButton_Save().click()
                    this.checktoasts(toast)
                }
            }else cy.log('Not able to access Profile page')
        }
        this.backtoHomePage()
    }
    createNewProfileAllPermissions(profile, status, countrylist, toast){
        if(this.openProfilePage()){
            if(elements_ProfilePage.getTitle_Page()){
                if (elements_ProfilePage.getButton_New_Profile().click()){
                    elements_ProfilePage.getInput_Profile_Modal().click()
                    elements_ProfilePage.getInput_Profile_Modal().type(profile)
                    elements_ProfilePage.getSelect_Status_Modal().click()
                    cy.wait(600)
                    elements_Generic.getFiltered_Result().contains(status).click()
                    for(var i = 0 ; i < countrylist.length ; i ++){
                        elements_ProfilePage.getSelectCountry_Modal().click()
                        cy.wait(600)
                        elements_Generic.getFiltered_Result().contains(countrylist[i].toString()).click()
                        cy.get('body').trigger('keydown', { keyCode: 27})
                        cy.wait(100);
                        cy.get('body').trigger('keyup', { keyCode: 27})
                    }
                    elements_ProfilePage.getTabAdministration_Modal().click()
                    elements_ProfilePage.getItemProfileCreate_Modal().click()
                    elements_ProfilePage.getItemUserCreate_Modal().click()
                    elements_ProfilePage.getItemProfileDelete_Modal().click()
                    elements_ProfilePage.getItemUserDelete_Modal().click()
                    elements_ProfilePage.getItemProfileUpdate_Modal().click()
                    elements_ProfilePage.getItemUserUpdate_Modal().click()
                    elements_ProfilePage.getTabCustomerAlloc_Modal().click().click()
                    elements_ProfilePage.getItemAllocationRetailCreate_Modal().click()
                    elements_ProfilePage.getItemBookedCreate_Modal().click()
                    elements_ProfilePage.getItemExecutiveAllocationCreate_Modal().click()
                    elements_ProfilePage.getItemMissingCreate_Modal().click()
                    elements_ProfilePage.getItemOverbookedCreate_Modal().click()
                    elements_ProfilePage.getItemToBookCreate_Modal().click()
                    elements_ProfilePage.getItemAllocationRetailDelete_Modal().click()
                    elements_ProfilePage.getItemBookedDelete_Modal().click()
                    elements_ProfilePage.getItemExecutiveAllocationDelete_Modal().click()
                    elements_ProfilePage.getItemMissingDelete_Modal().click()
                    elements_ProfilePage.getItemOverbookedDelete_Modal().click()
                    elements_ProfilePage.getItemToBookDelete_Modal().click()
                    elements_ProfilePage.getItemAllocationRetailUpdate_Modal().click()
                    elements_ProfilePage.getItemBookedUpdate_Modal().click()
                    elements_ProfilePage.getItemExecutiveAllocationUpdate_Modal().click()
                    elements_ProfilePage.getItemMissingUpdate_Modal().click()
                    elements_ProfilePage.getItemOverbookedUpdate_Modal().click()
                    elements_ProfilePage.getItemToBookUpdate_Modal().click()
                    elements_ProfilePage.getTabKaAlloc_Modal().click().click()
                    elements_ProfilePage.getItemBrazilDemandCreate_Modal().click()
                    elements_ProfilePage.getItemCarriesInfoCreate_Modal().click()
                    elements_ProfilePage.getItemKaManagementCreate_Modal().click()
                    elements_ProfilePage.getItemLomCreate_Modal().click()
                    elements_ProfilePage.getItemBrazilDemandDelete_Modal().click()
                    elements_ProfilePage.getItemCarriesInfoDelete_Modal().click()
                    elements_ProfilePage.getItemLomDelete_Modal().click()
                    elements_ProfilePage.getItemBrazilDemandUpdate_Modal().click()
                    elements_ProfilePage.getItemCarriesInfoUpdate_Modal().click()
                    elements_ProfilePage.getItemKaAllocationUpdate_Modal().click()
                    elements_ProfilePage.getItemKaManagementUpdate_Modal().click()
                    elements_ProfilePage.getItemLomUpdate_Modal().click()
                    elements_ProfilePage.getTabOnePlan_Modal().click()
                    elements_ProfilePage.getItemOnePlanCreate_Modal().click()
                    elements_ProfilePage.getItemOnePlanVariationAnalysisCreate_Modal().click()
                    elements_ProfilePage.getItemOnePlanDelete_Modal().click()
                    elements_ProfilePage.getItemOnePlanVariationAnalysisDelete_Modal().click()
                    elements_ProfilePage.getItemOnePlanUpdate_Modal().click()
                    elements_ProfilePage.getItemOnePlanVariationAnalysisUpdate_Modal().click()
                    elements_ProfilePage.getTabSetup_Modal().click()
                    elements_ProfilePage.getItemChannelCreate_Modal().click()
                    elements_ProfilePage.getItemCountryCreate_Modal().click()
                    elements_ProfilePage.getItemCustomerSettingsCreate_Modal().eq(0).click()
                    elements_ProfilePage.getItemGeoCreate_Modal().click()
                    elements_ProfilePage.getItemSalesManagementCreate_Modal().click()
                    elements_ProfilePage.getItemSetupManagementCreate_Modal().click()
                    elements_ProfilePage.getItemChannelDelete_Modal().click()
                    elements_ProfilePage.getItemCountryDelete_Modal().click()
                    elements_ProfilePage.getItemGeoDelete_Modal().click()
                    elements_ProfilePage.getItemOrderAdminManagementDelete_Modal().click()
                    elements_ProfilePage.getItemSalesManagementDelete_Modal().click()
                    elements_ProfilePage.getItemChannelUpdate_Modal().click()
                    elements_ProfilePage.getItemCountryUpdate_Modal().click()
                    elements_ProfilePage.getItemCustomerManagementUpdate_Modal().click()
                    elements_ProfilePage.getItemCustomerSettingsUpdate_Modal().eq(0).click()
                    elements_ProfilePage.getItemFiscalCalendarView_Modal().click()
                    elements_ProfilePage.getItemGeoUpdate_Modal().click()
                    elements_ProfilePage.getItemOrderAdminManagementUpdate_Modal().click()
                    elements_ProfilePage.getItemSalesManagementUpdate_Modal().click()
                    elements_Generic.getButton_Save().click()
                    this.checktoasts(toast)
                }
            }else cy.log('Not able to access Profile page')
        }
        this.backtoHomePage()
    }
    updateNewProfile(profile, oldstatus, newstatus, country, countrylist, toast){
        if(this.openProfilePage()){
            if(elements_ProfilePage.getTitle_Page()){
                elements_ProfilePage.getInputList_Profile().click({force:true})
                elements_ProfilePage.getInputList_Profile().type(profile)
                cy.wait(600)
                elements_Generic.getFiltered_Result().contains(profile).click()
                elements_ProfilePage.getSelectList_Status().click()
                cy.wait(600)
                elements_Generic.getFiltered_Result().contains(oldstatus).click()
                elements_ProfilePage.getInputList_Country().click()
                cy.wait(600)
                elements_Generic.getFiltered_Result().contains(country)
                cy.wait(3000)
                elements_Generic.getIcon_Edit().click()
                elements_ProfilePage.getInput_Profile_Modal().click()
                elements_ProfilePage.getInput_Profile_Modal().type(profile)
                elements_ProfilePage.getSelect_Status_Modal().click()
                cy.wait(600)
                elements_Generic.getFiltered_Result().contains(newstatus).click()
                for(var i = 0 ; i < countrylist.length ; i ++){
                    elements_ProfilePage.getSelectCountry_Modal().click()
                    cy.wait(600)
                    elements_Generic.getFiltered_Result().contains(countrylist[i].toString()).click()
                    cy.get('body').trigger('keydown', { keyCode: 27})
                    cy.wait(100);
                    cy.get('body').trigger('keyup', { keyCode: 27})
                }
                elements_ProfilePage.getTabAdministration_Modal().click()
                elements_ProfilePage.getItemProfileDelete_Modal().click()
                elements_ProfilePage.getItemUserDelete_Modal().click()
                elements_ProfilePage.getItemProfileUpdate_Modal().click()
                elements_ProfilePage.getItemUserUpdate_Modal().click()
                elements_ProfilePage.getTabCustomerAlloc_Modal().click().click()
                elements_ProfilePage.getItemAllocationRetailDelete_Modal().click()
                elements_ProfilePage.getItemBookedDelete_Modal().click()
                elements_ProfilePage.getItemExecutiveAllocationDelete_Modal().click()
                elements_ProfilePage.getItemMissingDelete_Modal().click()
                elements_ProfilePage.getItemOverbookedDelete_Modal().click()
                elements_ProfilePage.getItemToBookDelete_Modal().click()
                elements_ProfilePage.getItemAllocationRetailUpdate_Modal().click()
                elements_ProfilePage.getItemBookedUpdate_Modal().click()
                elements_ProfilePage.getItemExecutiveAllocationUpdate_Modal().click()
                elements_ProfilePage.getItemMissingUpdate_Modal().click()
                elements_ProfilePage.getItemOverbookedUpdate_Modal().click()
                elements_ProfilePage.getItemToBookUpdate_Modal().click()
                elements_ProfilePage.getTabKaAlloc_Modal().click().click()
                elements_ProfilePage.getItemBrazilDemandDelete_Modal().click()
                elements_ProfilePage.getItemCarriesInfoDelete_Modal().click()
                elements_ProfilePage.getItemLomDelete_Modal().click()
                elements_ProfilePage.getItemBrazilDemandUpdate_Modal().click()
                elements_ProfilePage.getItemCarriesInfoUpdate_Modal().click()
                elements_ProfilePage.getItemKaAllocationUpdate_Modal().click()
                elements_ProfilePage.getItemKaManagementUpdate_Modal().click()
                elements_ProfilePage.getItemLomUpdate_Modal().click()
                elements_ProfilePage.getTabOnePlan_Modal().click()
                elements_ProfilePage.getItemOnePlanDelete_Modal().click()
                elements_ProfilePage.getItemOnePlanVariationAnalysisDelete_Modal().click()
                elements_ProfilePage.getItemOnePlanUpdate_Modal().click()
                elements_ProfilePage.getItemOnePlanVariationAnalysisUpdate_Modal().click()
                elements_ProfilePage.getTabSetup_Modal().click()
                elements_ProfilePage.getItemChannelDelete_Modal().click()
                elements_ProfilePage.getItemCountryDelete_Modal().click()
                elements_ProfilePage.getItemGeoDelete_Modal().click()
                elements_ProfilePage.getItemOrderAdminManagementDelete_Modal().click()
                elements_ProfilePage.getItemSalesManagementDelete_Modal().click()
                elements_ProfilePage.getItemChannelUpdate_Modal().click()
                elements_ProfilePage.getItemCountryUpdate_Modal().click()
                elements_ProfilePage.getItemCustomerManagementUpdate_Modal().click()
                elements_ProfilePage.getItemCustomerSettingsUpdate_Modal().eq(0).click()
                elements_ProfilePage.getItemGeoUpdate_Modal().click()
                elements_ProfilePage.getItemOrderAdminManagementUpdate_Modal().click()
                elements_ProfilePage.getItemSalesManagementUpdate_Modal().click()
                elements_ProfilePage.getItemSetupManagementUpdate_Modal().click()
                elements_Generic.getButton_Save().click()
                if(elements_Generic.getTitle_Confirmation()){
                    elements_Generic.getButton_Yes().click()
                    this.checktoasts(toast)
                }else cy.log("Confirmation modal was not displayed")
            }
        }
        this.backtoHomePage()
    }
    deleteNewProfile(profile, status, country, toast){
        if(this.openProfilePage()){
            if(elements_ProfilePage.getTitle_Page()){
                elements_ProfilePage.getInputList_Profile().type(profile)
                cy.wait(600)
                elements_Generic.getFiltered_Result().contains(profile).click()
                elements_ProfilePage.getSelectList_Status().click()
                cy.wait(600)
                elements_Generic.getFiltered_Result().contains(status).click()
                elements_ProfilePage.getInputList_Country().click()
                cy.wait(600)
                elements_Generic.getFiltered_Result().contains(country)
                cy.wait(3000)
                elements_Generic.getIcon_Delete().click()
                if(elements_Generic.getTitle_Confirmation()){
                    elements_Generic.getButton_Yes().click()
                    this.checktoasts(toast)
                }else cy.log("Confirmation modal was not displayed")
            }
        }
        this.backtoHomePage()
    }
    checktoasts(expectedToast){
        cy.wait(2000)
        elements_Generic.getToast().then((toast)=>{
            switch (expectedToast) {
                case "newProfile_success":
                    cy.checkToast("New Profile","Registered successfully")
                    break;
                case "updateProfile_success":
                    cy.checkToast("Updated Profile","Successfully!")
                    break;
                case "deleteProfile_success":
                    cy.checkToast("Profile","Successfully removed!")
                    break;
                case "deleteProfile_associated_failed":
                    cy.checkToast("USER associated with this PROFILE","Please remove it before delete to complete this operation!")
                    break;
                case "newProfile_duplicate_failed":
                    cy.checkToast("PROFILE already exists","Please inform another PROFILE!")
                    break;
                case "profile_mandatoryField":
                    cy.checkToast("Profile","Mandatory field")
                    break;
                case "status_mandatoryField":
                    cy.checkToast("Status","Mandatory field")
                    break;
                case "country_mandatoryField":
                    cy.checkToast("Country","Mandatory field")
                    break;
                case "profile_noAction":
                    cy.checkToast("Permissions","No action (Create, Delete, Edit, View) checked. Please informed regarding the profile/function")
                    break;
                default:
                    console.log("Unknown Toast");
                    break;
            }
        })
    }

    createNewProfileWithoutAction(profile, status, countrylist,toast){
        if(this.openProfilePage()){
            if(elements_ProfilePage.getTitle_Page()){
                if (elements_ProfilePage.getButton_New_Profile().click()){
                    if (profile == ""){
                        
                        elements_ProfilePage.getSelect_Status_Modal().click()
                        cy.wait(600)
                        elements_Generic.getFiltered_Result().contains(status).click()
                        for(var i = 0 ; i < countrylist.length ; i ++){
                            elements_ProfilePage.getSelectCountry_Modal().click()
                            cy.wait(600)
                            elements_Generic.getFiltered_Result().contains(countrylist[i].toString()).click()
                            cy.get('body').trigger('keydown', { keyCode: 27})
                            cy.wait(100);
                            cy.get('body').trigger('keyup', { keyCode: 27})
                        }
                    }else if(status == ""){

                        elements_ProfilePage.getInput_Profile_Modal().click()
                        elements_ProfilePage.getInput_Profile_Modal().type(profile)
                       
                        for(var i = 0 ; i < countrylist.length ; i ++){
                            elements_ProfilePage.getSelectCountry_Modal().click()
                            cy.wait(600)
                            elements_Generic.getFiltered_Result().contains(countrylist[i].toString()).click()
                            cy.get('body').trigger('keydown', { keyCode: 27})
                            cy.wait(100);
                            cy.get('body').trigger('keyup', { keyCode: 27})
                        }
                    }else if(countrylist == ""){
                        elements_ProfilePage.getInput_Profile_Modal().click()
                        elements_ProfilePage.getInput_Profile_Modal().type(profile)
                        elements_ProfilePage.getSelect_Status_Modal().click()
                        cy.wait(600)
                        elements_Generic.getFiltered_Result().contains(status).click()
                    }else{
                        elements_ProfilePage.getInput_Profile_Modal().click()
                        elements_ProfilePage.getInput_Profile_Modal().type(profile)
                        elements_ProfilePage.getSelect_Status_Modal().click()
                        cy.wait(600)
                        elements_Generic.getFiltered_Result().contains(status).click()
                        for(var i = 0 ; i < countrylist.length ; i ++){
                            elements_ProfilePage.getSelectCountry_Modal().click()
                            cy.wait(600)
                            elements_Generic.getFiltered_Result().contains(countrylist[i].toString()).click()
                            cy.get('body').trigger('keydown', { keyCode: 27})
                            cy.wait(100);
                            cy.get('body').trigger('keyup', { keyCode: 27})
                        }
                    }
                    
                    elements_Generic.getButton_Save().click()
                    this.checktoasts(toast)
                }
            }else cy.log('Not able to access Profile page')
        }
        elements_Generic.getButton_Cancel().click()
        this.backtoHomePage()
    }

    createDuplicatedProfile(profile, status, country,toast){
        if(this.openProfilePage()){
            for(var i = 0 ; i < 2 ; i ++){
                if(elements_ProfilePage.getTitle_Page()){
                    if (elements_ProfilePage.getButton_New_Profile().click()){
                        
                        elements_ProfilePage.getInput_Profile_Modal().click()
                        elements_ProfilePage.getInput_Profile_Modal().type(profile)
                        elements_ProfilePage.getSelect_Status_Modal().click()
                        cy.wait(600)
                        elements_Generic.getFiltered_Result().contains(status).click()
                        elements_ProfilePage.SelectAllPermissions_Modal().first().click()
                        elements_ProfilePage.getSelectCountry_Modal().click()
                        cy.wait(600)
                        elements_Generic.getFiltered_Result().contains(country).click()
                        cy.wait(100);
                        elements_Generic.getButton_Save().click({force:true})
                    }
                }
            }
            this.checktoasts(toast)
        }
    }

    checkNotFoundProfile(profile,status){
        if(this.openProfilePage()){
            if(elements_ProfilePage.getTitle_Page()){
                elements_ProfilePage.getInputList_Profile().click()
                elements_ProfilePage.getInputList_Profile().type(profile)
                elements_ProfilePage.getSelectList_Status().click()
                cy.wait(600)
                elements_Generic.getFiltered_Result().contains(status).click()
                cy.wait(600)
                elements_ProfilePage.checkNoResultsFound().contains("No results were found")
            }
        }
    }
}