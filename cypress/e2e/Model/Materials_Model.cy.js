export default class Materials_Model{
    testData = {
        materials: {
            allocation_bz: 'B2B',
            internal_name: 'BORAG22',
            product_group: 'BORAG22',
            color_desc: 'QUARTZ BLACK',
            market_name: 'moto e22',
            ean:'7892597353026',
            local_color_desc: 'Edit test',
            local_market_name: 'Edit test',
            invalid_item: '@#45_!?&%',
            name_notFound: 'Not Found',
            empty: "",
            origin:{
                jag: 'JAG',
                man: 'MAN'
            },
            status:{
                on: 'Active',
                off: 'Inactive'
            },
            results_number:{
                value1:'1',
                value2:'2',
                value3:'3',
                value4:'4',
                value5:'5',
                value6:'6',
                value7:'7',
                value8:'8',
                value9:'9',
                value10:'10',
                
            },
            filters:{
                list:[
                    'INTERNAL_NAME',
                    'STATUS',
                    'COLOR_DESC',
                    'MARKET_NAME',
                    'PRODUCT_GROUP',
                    'ORIGIN',
                ]
            }
        }
    }
}