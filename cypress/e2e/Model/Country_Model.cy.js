import msgTestAuto from '../../support/generateRandomMessage.js'

export default class Country_Model{
    testData = {
        country: {
            name: `${msgTestAuto} COUNTRY`,
            new_name: `${msgTestAuto} NEW`,
            invalid_item: '#$@_&2459',
            name_duplicated: `${msgTestAuto} Duplicated`,
            name_associated: `${msgTestAuto} Associated`,
            description:`${msgTestAuto} Description`,
            iso_code: `${msgTestAuto}`,
            new_iso_code: `${msgTestAuto} NEW`,
            name_notFound: 'Not Found',
            empty: " ",
            country: {
                AR:'Argentina',
                BR: 'Brazil'
            },
            
        }
    }
}