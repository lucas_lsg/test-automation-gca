import msgTestAuto from '../../support/generateRandomMessage.js'

export default class Profile_Model{
    testData = {
        profile: {
            name: `${msgTestAuto}`,
            name_duplicated: `${msgTestAuto} Duplicated`,
            name_associated: `${msgTestAuto} Associated`,
            empty: "",
            status:{
                on: 'Active',
                off: 'Inactive'
            },
            country: {
                list1:['Chile','Argentina','Brazil'],
                list2:['Chile','Argentina'],
                BR: 'Brazil'
            },
            name_notFound: 'Not Found',
        }
    }
}