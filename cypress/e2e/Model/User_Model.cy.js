import msgTestAuto from '../../support/generateRandomMessage.js'

export default class User_Model{
    testData = {
        user: {
            last_name: `Last ${msgTestAuto}`,
            name: `${msgTestAuto}`,
            email:`${msgTestAuto}@motorola.com`,
            new_email: `${msgTestAuto}@lenovo.com`,
            invalid_email:`${msgTestAuto}@gmail.com`,
            invalid_lastName:`2!@54`,
            invalid_name:`#!%&45`,
            name_duplicated: `${msgTestAuto} Duplicated`,
            empty: " ",
            status:{
                on: 'Active',
                off: 'Inactive'
            },
            country: {
                AR: 'Argentina',
                BR: 'Brazil'
            },
            sales_manager:{
                on: 'Yes',
                off: 'No'
            },
            order_adm:{
                on: 'Yes',
                off: 'No'
            },
            profile:'BR Allocation',
            new_profile:'BR System Manager'
        }
    }
}