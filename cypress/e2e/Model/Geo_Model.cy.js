import msgTestAuto from '../../support/generateRandomMessage.js'

export default class Geo_Model{
    testData = {
        geo: {
            name: `${msgTestAuto} GEO`,
            invalid_name:`#!%&45`,
            name_duplicated: `${msgTestAuto} Duplicated`,
            name_notFound: 'Not Found',
            empty: " ",
        }
    }
}