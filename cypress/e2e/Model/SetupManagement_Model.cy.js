export default class SetupManagement_Model{
    testData = {
        week: {
            plus_0: 'Week +0',
            plus_1: 'Week +1',
            plus_2: 'Week +2'
        },
        feature:{
            toBook: 'toBook',
            toAllocate: 'toAllocate'
        },
        month:{
            january:'Jan',
            february:'Feb',
            march:'Mar',
            april:'Apr',
            may:'May',
            june:'Jun',
            july:'Jul',
            august:'Aug',
            september:'Sep',
            october:'Oct',
            november:'Nov',
            december:'Dec'
        }
    }
}