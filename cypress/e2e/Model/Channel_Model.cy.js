import msgTestAuto from '../../support/generateRandomMessage.js'

export default class Channel_Model{
    testData = {
        channel: {
            name: `${msgTestAuto} CHANNEL`,
            name_duplicated: `${msgTestAuto} Duplicated`,
            name_associated: `${msgTestAuto} Associated`,
            description:`${msgTestAuto} Description`,
            invalid_item: '@#45_!?&%',
            name_notFound: 'Not Found',
            empty: " ",
            country: {
                AR:'Argentina',
                BR: 'Brazil'
            },
            
        }
    }
}