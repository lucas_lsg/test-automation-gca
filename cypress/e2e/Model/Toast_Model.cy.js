export default class Toast_Model{
     testData = {
          toast_profile: {
               success: {
                    create: "newProfile_success",
                    edit: "updateProfile_success",
                    delete: "deleteProfile_success",
               },
           
               fail:{
                    create_duplicated: "newProfile_duplicate_failed", 
                    delete_associated: "deleteProfile_associated_failed",
               },

               warning:{
                    name_empty: "profile_mandatoryField",
                    status_empty: "status_mandatoryField",
                    country_empty: "country_mandatoryField",
                    create_noAction: "profile_noAction"
               }
          },
        
          toast_user: {
               success: {
                    create: "newUser_success",
                    edit: "updateUser_success",
                    delete: "deleteUser_success",
               },

               fail:{
                    create_duplicated: "newUser_duplicate_failed",
                    delete_order_associated: "deleteUser_associatedOrderwithSales",
                    delete_sales_associated: "deleteUser_associatedSaleswithOrder"
               },

               warning:{
                    last_name_empty: "lastName_mandatoryField",
                    name_empty: "name_mandatoryField",
                    email_empty: "email_mandatoryField",
                    country_empty: "country_mandatoryField",
                    status_empty: "status_mandatoryField",
                    salesManager_empty:"salesManager_mandatoryField",
                    orderAdmin_empty:"orderAdmin_mandatoryField",
                    profile_empty:"profile_mandatoryField",
                    invalid_email: "newUser_invalidEmail"
               }
          },

          toast_geo: {
               success: {
                    create: "newgeo_success",
                    edit: "updategeo_success",
                    delete: "deletegeo_success",
               },

               fail:{
                    create_duplicated: "newgeo_duplicate_failed",
                    delete_country_associated: "deletegeo_country_associated_failed"
               },

               warning:{
                    geo_empty: "newgeo_blank_failed"
               }
          },

          toast_channel: {
               success: {
                    create: "newChannel_success",
                    edit: "updateChannel_success",
                    delete: "deleteChannel_success",
               },

               fail:{
                    create_duplicated: "newChannel_duplicate_failed",
                    delete_associated: "Channel_associated_failed"
               },

               warning:{
                    channel_empty: "channel_blank_failed",
                    country_empty: "country_blank_failed",
                    description_empty: "description_blank_failed"
               }
          },

          toast_country: {
               success: {
                    create: "newcountry_success",
                    edit: "updatecountry_success",
                    delete: "deletecountry_success",
               },

               fail:{
                    country_duplicated: "country_duplicate_failed",
                    isoCode_duplicated: "isoCode_duplicate_failed",
                    delete_channel_associated: "deletecountry_associated_channel_failed"
               },

               warning:{
                    country_empty: "country_blank_failed",
                    geo_empty: "geo_blank_failed",
                    isoCode_empty: "IsoCode_blank_failed"
               }
          },
          toast_materials: {
               success: {
                    edit: "updateMaterial_success",
               },
          },

          toast_customer: {
               success: {
                    create: "createCustomer_success",
                    edit: "updateCustomer_success"
               },
               warning:{
                    customerShort_empty: "customerShortMandatoryfield",
                    channel_empty: "channelMandatoryfield",
                    taxes_empty: "taxesMandatoryfield",
                    status_empty: "statusMandatoryfield"
               },
               fail:{
                    invalid_data: "insertInvalidData"
               },
          },
          toast_setup_managmt:{
               success:{
                    update:"UpdateSetupManagementSuccessMessage",
               }
          }
    }
}