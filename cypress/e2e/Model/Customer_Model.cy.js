import msgTestAuto from '../../support/generateRandomMessage.js'

export default class Customer_Model{
    testData = {
        customer: {
            customer_short: `${msgTestAuto} SHORT`,
            new_customer_short: `NEW CUSTOMER ${msgTestAuto}`,
            customer_PO_line: `${msgTestAuto} PO Line`,
            name_associated: `${msgTestAuto} Associated`,
            description:`${msgTestAuto} Description`,
            invalid_item: '@#45_!?&%',
            name_notFound: 'Not Found',
            empty: "",
            space: " ",
            country: {
                AR:'Argentina',
                BR: 'Brazil'
            },
            channel: {
                first:'B2W',
                second: 'CASAS BAHIA'
            },
            orderAdmin:{
                first:'Cardoso,Heloisa',
                second: 'Inacio,Simone'
            },
            salesManager:{
                first:'Bronzoni,Adalberto',
                second:'Costa,Filadelfo'
            },
            status:{
                on: 'Active',
                off: 'Inactive'
            },
            taxes_application:{
                list1:['FORA_12','FORA_7','SP'],
                list2:['FORA_12','FORA_7']
            },
            customerList:{
                filters: [
                    'GEO',
                    'COUNTRY',
                    'CUSTOMER_NUMBER',
                    'CUSTOMER_SHORT',
                    'CUSTOMER_NAME'
                ],
                geo:'LATAM',
                country:'Brazil',
                customer_number:'1216940493',
                customer_short:'CORPORATE',
                customer_name:'ALGAR TELECOM S/A',
                results_number: {
                    total1: '1',
                    total2:'2',
                }
            }
            
            
        }
    }
}