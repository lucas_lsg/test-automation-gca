export default class FiscalCalendar_Model{
    testData = {
        fiscalCalendar: {
            fiscal_year: '2023/2024',
            quarter: 'FQ1',
            week: 'WK18',
            day: '5',
            filters:{
                list:[
                    'WEEK',
                    'DAY',
                    'FISCAL_YEAR',
                    'QUARTER',
                ]
            }
        }
    }
}