/// <reference types="cypress" />
import Geo_Controller from '../../Controller/Geo_Controller.cy.js'
import Country_Controller from '../../Controller/Country_Controller.cy.js';
import SignGoogle_Controller from '../../Controller/SignGoogle_Controller.cy.js'
import Toast_Model from '../../Model/Toast_Model.cy.js';
import Country_Model from '../../Model/Country_Model.cy.js';
import Geo_Model from '../../Model/Geo_Model.cy'

let signGoogle_Controller = new SignGoogle_Controller;
let geo_Controller = new Geo_Controller
let country_Controller = new Country_Controller
let geo_Model = new Geo_Model
let country_Model = new Country_Model
let toast_Model = new Toast_Model

const geo = geo_Model.testData.geo
const country = country_Model.testData.country
const toast_geo = toast_Model.testData.toast_geo
const toast_country = toast_Model.testData.toast_country

describe(`
    Given I'm Home page
    When I'm going to the Geo page
    Then filters should be working correctly
    And I should be able to crate new Geo
    And I should be able to update Geo
    And I should be able to delete Geo
    And your confirmations toast should appear correctly`,()=>{
    beforeEach('Open home page',()=>{
        signGoogle_Controller.signGooglePage()
    })
    it('Check all UI elements from Geo page',()=>{
        geo_Controller.checkElements()
    })
    it(`Try create new Geo`,()=>{
        geo_Controller.createNewGeo
        (
            geo.name, 
            toast_geo.success.create
        )
    })
    it(`Try create duplicated Geo`,()=>{
        geo_Controller.createNewGeo
        (
            geo.name, 
            toast_geo.fail.create_duplicated
        )
    })
    it(`Try delete Geo associated with a Country`,()=>{
        country_Controller.createNewCountry
        (
            country.name,
            geo.name,
            country.iso_code,
            toast_country.success.create
        )
        geo_Controller.deleteNewGeo
        (
            geo.name, 
            toast_geo.fail.delete_country_associated
        )
    })
    it(`Try create Geo without name`,()=>{
        geo_Controller.createNewGeo
        (
            geo.empty, 
            toast_geo.warning.geo_empty
        )
    })
    it(`Try create Geo with invalid name`,()=>{
        geo_Controller.createNewGeo
        (
            geo.invalid_name, 
            toast_geo.warning.geo_empty
        )
    })
    it(`Try edit last Geo`,()=>{
        geo_Controller.updateNewGeo
        (
            geo.name, 
            toast_geo.success.edit
        )
    })
    it('Try filter by not found Geo',()=>{
        geo_Controller.checkNotFoundProfile(geo.invalid_name)
    })
    it(`Try delete last Geo`,()=>{
        country_Controller.deleteNewCountry
        (
            country.name,
            toast_country.success.delete
        )
        geo_Controller.deleteNewGeo
        (
            geo.name, 
            toast_geo.success.delete
        )
    })
})