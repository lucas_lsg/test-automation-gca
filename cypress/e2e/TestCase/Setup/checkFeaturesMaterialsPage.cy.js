/// <reference types="cypress" />
import Materials_Controller from '../../Controller/Materials_Controller.cy.js';
import SignGoogle_Controller from '../../Controller/SignGoogle_Controller.cy.js';
import Toast_Model from '../../Model/Toast_Model.cy.js';
import Materials_Model from '../../Model/Materials_Model.cy.js';

let materials_Controller = new Materials_Controller;
let signGoogle_Controller = new SignGoogle_Controller;
let materials_Model = new Materials_Model
let toast_Model = new Toast_Model

const materials = materials_Model.testData.materials
const toast_materials = toast_Model.testData.toast_materials

describe(`
  Given I'm Materials page
  When I'm navigation to the Materials page
  Then I should see title from Materials page
  And each filter should be working correctly
  And compare with value returned in the table`,()=>{
    beforeEach('Open home page',()=>{
        signGoogle_Controller.signGooglePage()
    })
    it('Check elements and Navigation',()=>{
        materials_Controller.checkElements()
    })
    it('Try edit a Material',()=>{
        materials_Controller.updateMaterial
        (
            materials.ean,
            materials.allocation_bz,
            materials.local_market_name,
            materials.local_color_desc,
            toast_materials.success.edit
        )
    })
    it('Check if filters are working correctly',()=>{
        materials_Controller.checkFilters
        (
            materials.filters.list,
            materials.internal_name,
            materials.status.on,
            materials.color_desc,
            materials.market_name,
            materials.product_group,
            materials.origin.jag,
            materials.results_number.value6
        )
    })
    it('Check behavior when no results were found', ()=>{
        materials_Controller.checkNoResultsFound(materials.name_notFound)
    })
    it('Check export feature', ()=>{
        materials_Controller.checkExport('./cypress/downloads/material_exported.xlsx')
    })
})