/// <reference types="cypress" />
import OrderAdmin_Controller from '../../Controller/OrderAdmin_Controller.cy.js'
import User_Controller from '../../Controller/User_Controller.cy.js'
import msgTestAuto from '../../../support/generateRandomMessage.js'
import SignGoogle_Controller from '../../Controller/SignGoogle_Controller.cy.js'
let signGoogle_Controller = new SignGoogle_Controller;
let orderAdmin_Controller = new OrderAdmin_Controller
let user_Controller = new User_Controller

describe(`
    Given I'm Home page
    When I'm going to the OrderAdmin page
    Then filters should be working correctly
    And I should be able to crate new OrderAdmin
    And I should be able to update OrderAdmin
    And I should be able to delete OrderAdmin
    And your confirmations toast should appear correctly`,()=>{
    before('Open home page',()=>{
        signGoogle_Controller.signGooglePage()
        orderAdmin_Controller.checkElements()
    })
    it(`Try Update OrderAdmin association`,()=>{
        user_Controller.createNewUser('lastN'+`${msgTestAuto}`,
        `${msgTestAuto} OrderAdmin`,`${msgTestAuto}`+'@lenovo.com',
        'Brazil', 'Active', 'BR System Manager', 'No', 'Yes',"newUser_success")
        user_Controller.createNewUser('lastN'+`${msgTestAuto}`,
        `${msgTestAuto} SalesManager`,`${msgTestAuto}`+'@motorola.com',
        'Brazil', 'Active', 'BR Allocation', 'Yes', 'No',"newUser_success")
        orderAdmin_Controller.updateAssociationOrderAdmin(`${msgTestAuto} OrderAdmin`,['lastN'+`${msgTestAuto}`+', '+`${msgTestAuto} SalesManager`], "updateSetupOrderAdmin_success")
        user_Controller.deleteNewUser('lastN'+`${msgTestAuto}`,
        `${msgTestAuto} OrderAdmin`,'Brazil', 'Active', 'BR System Manager','deleteUser_associatedOrderwithSales')
        user_Controller.deleteNewUser('lastN'+`${msgTestAuto}`,
        `${msgTestAuto} SalesManager`,'Brazil', 'Active', 'BR Allocation','deleteUser_associatedSaleswithOrder')
        orderAdmin_Controller.removeAssociationOrderAdmin(`${msgTestAuto} OrderAdmin`, "removeOrderAdminAssociation_success")
        user_Controller.deleteNewUser('lastN'+`${msgTestAuto}`,
        `${msgTestAuto} OrderAdmin`,'Brazil', 'Active', 'BR System Manager','deleteUser_success')
        user_Controller.deleteNewUser('lastN'+`${msgTestAuto}`,
        `${msgTestAuto} SalesManager`,'Brazil', 'Active', 'BR Allocation','deleteUser_success')
   })
})