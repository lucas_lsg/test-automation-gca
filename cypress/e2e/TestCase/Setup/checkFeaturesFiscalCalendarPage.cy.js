/// <reference types="cypress" />
import FiscalCalendar_Controller from '../../Controller/FiscalCalendar_Controller.cy.js';
import SignGoogle_Controller from '../../Controller/SignGoogle_Controller.cy.js';
import FiscalCalendar_Model from '../../Model/FiscalCalendar_Model.cy.js';
let fiscalCalendar_Controller = new FiscalCalendar_Controller;
let signGoogle_Controller = new SignGoogle_Controller;
let fiscalCalendar_Model = new FiscalCalendar_Model
const fiscalCalendar = fiscalCalendar_Model.testData.fiscalCalendar
describe(`
  Given I'm FiscalCalendar page
  When I'm navigation to the FiscalCalendar page
  Then I should see title from FiscalCalendar page
  And each filter should be working correctly
  And compare with value returned in the table`,()=>{
    beforeEach('Open fiscalCalendar page',()=>{
        signGoogle_Controller.signGooglePage()
    })
    it('Check elements and Navigation',()=>{
        fiscalCalendar_Controller.checkElements()
    })

    it('Check if filters working correctly using payload received from endpoint',()=>{
        cy.wait(10000)
        cy.getAccessToken().then(token => {
            cy.log(token)
            cy.request({
                method: 'GET',
                url: 'https://dev-dot-gca-back-dot-dataservices-non-prod.appspot.com/setupManagement/currentSetup',
                headers: {
                    Authorization: token,
                    'Content-Type': 'application/json',
                },
                includeCredentials: true
            }).then(response => {
                expect(response.status).to.eq(200);
                const current_WK = response.body.currentWeek.split('k')[1]
                const fq = response.body.quarter
                const fy = response.body.fiscalYear
                var lastDay_WK = parseInt(response.body.lastDayWeek.split('-')[2])
                if(lastDay_WK < 10){
                    lastDay_WK = response.body.lastDayWeek.split('')[9]
                }
                fiscalCalendar_Controller.checkFilters(fiscalCalendar.filters.list,fy,fq,current_WK,lastDay_WK)
            })
        })
    })
})