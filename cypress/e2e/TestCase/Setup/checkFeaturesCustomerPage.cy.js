/// <reference types="cypress" />
import Customer_Controller from '../../Controller/Customer_Controller.cy.js';
import SignGoogle_Controller from '../../Controller/SignGoogle_Controller.cy.js';
import Customer_Model from '../../Model/Customer_Model.cy.js';
import Toast_Model from '../../Model/Toast_Model.cy.js';

let customer_Controller = new Customer_Controller;
let signGoogle_Controller = new SignGoogle_Controller;
let toast_Model = new Toast_Model;
let customer_Model = new Customer_Model;

const customer = customer_Model.testData.customer
const toast_customer = toast_Model.testData.toast_customer

describe(`
  Given I'm Customer page
  When I'm navigation to the Customer page
  Then I should see title from Customer page
  And each filter should be working correctly
  And compare with value returned in the table`,()=>{
    beforeEach('Open customer page',()=>{
        signGoogle_Controller.signGooglePage()
    })
    it('Check elements and Navigation',()=>{
        customer_Controller.checkElements()
    })
    it('Try create new Customer Short',()=>{
        customer_Controller.createNewCustomerShort
        (
            customer.new_customer_short,
            customer.customer_PO_line,
            customer.channel.first,
            customer.country.BR,
            customer.status.off,
            customer.taxes_application.list1,
            toast_customer.success.create
        )
    })
    it('Try edit last Customer Short',()=>{
        customer_Controller.updateCustomerShort
        (
            customer.new_customer_short,
            toast_customer.success.edit
        )
    })
    it('Try create a new register with special character as Customer Short name',()=>{
        customer_Controller.createNewCustomerShort
        (
            customer.space,
            customer.customer_PO_line,
            customer.channel.first,
            customer.country.BR,
            customer.status.off,
            customer.taxes_application.list1,
            toast_customer.fail.invalid_data
        )
    })

    it('Try create a new register without Customer Short name',()=>{
        customer_Controller.createNewCustomerShort
        (
            customer.empty,
            customer.customer_PO_line,
            customer.channel.first,
            customer.country.BR,
            customer.status.off,
            customer.taxes_application.list1,
            toast_customer.warning.customerShort_empty
        )
    })

    it('Try create a new register without Channel name',()=>{
        customer_Controller.createNewCustomerShort
        (
            customer.customer_short,
            customer.customer_PO_line,
            customer.empty,
            customer.country.BR,
            customer.status.off,
            customer.taxes_application.list1,
            toast_customer.warning.channel_empty
        )
    })

    it('Try create a new register without Taxes name',()=>{
        customer_Controller.createNewCustomerShort
        (
            customer.customer_short,
            customer.customer_PO_line,
            customer.channel.first,
            customer.country.BR,
            customer.status.off,
            customer.empty,
            toast_customer.warning.taxes_empty
        )
    })

    it('Try create a new register without Status',()=>{
        customer_Controller.createNewCustomerShort
        (
            customer.customer_short,
            customer.customer_PO_line,
            customer.channel.first,
            customer.country.BR,
            customer.empty,
            customer.taxes_application.list1,
            toast_customer.warning.status_empty
        )
    })

    it('Check filter from Customer list page',()=>{
        customer_Controller.checkCustomerListFilters
        (
            customer.customerList.filters,
            customer.customerList.geo,
            customer.customerList.country,
            customer.customerList.customer_number,
            customer.customerList.customer_short,
            customer.customerList.customer_name,
            customer.customerList.results_number.total1)
    })
})