/// <reference types="cypress" />
import Sales_Controller from '../../Controller/Sales_Controller.cy.js';
import SignGoogle_Controller from '../../Controller/SignGoogle_Controller.cy.js';
let sales_Controller = new Sales_Controller;
let signGoogle_Controller = new SignGoogle_Controller;

describe(`
  Given I'm Sales page
  When I'm navigation to the Sales page
  Then I should see title from Sales page
  And each filter should be working correctly
  And compare with value returned in the table`,()=>{
    before('Open sales page',()=>{
        signGoogle_Controller.signGooglePage()
    })
    it('Check elements and Navigation',()=>{
        sales_Controller.checkElements()
    })
})