/// <reference types="cypress" />
import SetupManagement_Controller from '../../Controller/SetupManagement_Controller.cy.js'
import SignGoogle_Controller from '../../Controller/SignGoogle_Controller.cy.js'
import SetupManagement_Model from '../../Model/SetupManagement_Model.cy.js'
import Toast_Model from '../../Model/Toast_Model.cy.js';

let signGoogle_Controller = new SignGoogle_Controller;
let setupManagement_Controller = new SetupManagement_Controller
let setupManagmt_Model = new SetupManagement_Model
let toast = new Toast_Model
const setupManagmt = setupManagmt_Model.testData
const toast_setup_managmt = toast.testData.toast_setup_managmt

describe(`
    Given I'm Home page
    When I'm going to the SetupManagement page
    Then filters should be working correctly
    And I should be able to create new SetupManagement
    And I should be able to update SetupManagement
    And I should be able to delete SetupManagement
    And your confirmations toast should appear correctly`,()=>{
    beforeEach('Open home page',()=>{
        signGoogle_Controller.signGooglePage()
    })
    it('Check main elements from UI', ()=>{
        setupManagement_Controller.checkElements()
    })
    it(`Check To Book and To Allocate features for Week +0`,()=>{
        setupManagement_Controller.checkEditCurrentSetup
        (
            setupManagmt.week.plus_0,
            setupManagmt.feature.toBook,
            toast_setup_managmt.success
        )

        setupManagement_Controller.checkEditCurrentSetup
        (
            setupManagmt.week.plus_0,
            setupManagmt.feature.toAllocate,
            toast_setup_managmt.success
        )
    })
    it(`Check To Book and To Allocate features for Week +1`,()=>{
        setupManagement_Controller.checkEditCurrentSetup
        (
            setupManagmt.week.plus_1,
            setupManagmt.feature.toBook,
            toast_setup_managmt.success
        )

        setupManagement_Controller.checkEditCurrentSetup
        (
            setupManagmt.week.plus_1,
            setupManagmt.feature.toAllocate,
            toast_setup_managmt.success
        )
    })
    it(`Check Entered Setup feature`,()=>{
        setupManagement_Controller.checkEditCurrentEnteredSetup
        (
            setupManagmt.month.september,
            toast_setup_managmt.success
        )
    })
})