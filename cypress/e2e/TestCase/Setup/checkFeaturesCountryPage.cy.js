/// <reference types="cypress" />
import Country_Controller from '../../Controller/Country_Controller.cy.js'
import Geo_Controller from '../../Controller/Geo_Controller.cy.js'
import Channel_Controller from '../../Controller/Channel_Controller.cy.js';
import SignGoogle_Controller from '../../Controller/SignGoogle_Controller.cy.js'
import Country_Model from '../../Model/Country_Model.cy.js';
import Channel_Model from '../../Model/Channel_Model.cy.js';
import Geo_Model from '../../Model/Geo_Model.cy.js';
import Toast_Model from '../../Model/Toast_Model.cy.js';

let signGoogle_Controller = new SignGoogle_Controller;
let country_Controller = new Country_Controller
let geo_Controller = new Geo_Controller
let channel_Controller = new Channel_Controller
let country_Model = new Country_Model
let geo_Model = new Geo_Model
let channel_Model = new Channel_Model
let toast_Model = new Toast_Model

const country = country_Model.testData.country
const geo = geo_Model.testData.geo
const channel = channel_Model.testData.channel
const toast_country = toast_Model.testData.toast_country
const toast_geo = toast_Model.testData.toast_geo
const toast_channel = toast_Model.testData.toast_channel


describe(`
    Given I'm Home page
    When I'm going to the Country page
    Then filters should be working correctly
    And I should be able to crate new Country
    And I should be able to update Country
    And I should be able to delete Country
    And your confirmations toast should appear correctly`,()=>{
    beforeEach('Open home page',()=>{
        signGoogle_Controller.signGooglePage()
    })
    it('Check all UI elements from Country page',()=>{
        country_Controller.checkElements()
    })
    it(`Try create new Country `,()=>{
        geo_Controller.createNewGeo
        (
            geo.name, 
            toast_geo.success.create
        )
        
        country_Controller.createNewCountry
        (
            country.name,
            geo.name,
            country.iso_code,
            toast_country.success.create
        )
    })
    it(`Try create duplicated Country `,()=>{
        country_Controller.createNewCountry
        (
            country.name,
            geo.name,
            country.new_iso_code,
            toast_country.fail.country_duplicated
        )
    })
    it(`Try create duplicated Iso Code `,()=>{
        country_Controller.createNewCountry
        (
            country.new_name,
            geo.name,
            country.iso_code,
            toast_country.fail.isoCode_duplicated
        )
    })
    it(`Try create new Country without name`,()=>{
        country_Controller.createNewCountry
        (
            country.empty,
            geo.name,
            country.iso_code,
            toast_country.warning.country_empty
        )
    })
    it(`Try create new Country without geo`,()=>{
        country_Controller.createNewCountry
        (
            country.new_name,
            geo.empty,
            country.new_iso_code,
            toast_country.warning.geo_empty
        )
    })
    it(`Try create new Country without Iso Code`,()=>{
        country_Controller.createNewCountry
        (
            country.new_name,
            geo.name,
            country.empty,
            toast_country.warning.isoCode_empty
        )
    })
    it(`Try create new Country with invalid name`,()=>{
        country_Controller.createNewCountry
        (
            country.invalid_item,
            geo.name,
            country.new_iso_code,
            toast_country.warning.country_empty
        )
    })
    it(`Try create new Country with invalid Iso Code`,()=>{
        country_Controller.createNewCountry
        (
            country.new_name,
            geo.name,
            country.invalid_item,
            toast_country.warning.isoCode_empty
        )
    })
    it(`Try edit last Country `,()=>{
        country_Controller.updateNewCountry
        (
            country.name,
            toast_country.success.edit
        )
    })
    it(`Try delete Country associated with channel`,()=>{
        channel_Controller.createNewChannel
        (
            country.name,
            channel.name,
            channel.description,
            toast_channel.success.create
        )
        
        country_Controller.deleteNewCountry
        (
            country.name,
            toast_country.fail.delete_channel_associated
        )
    })
    it('Try filter by not found Country',()=>{
        country_Controller.checkNotFoundCountry(country.invalid_item)
    })
    it(`Try delete last Country `,()=>{
        channel_Controller.deleteNewChannel
        (
            country.name,
            channel.name,
            toast_channel.success.delete
        )

        country_Controller.deleteNewCountry
        (
            country.name,
            toast_country.success.delete
        )
        
        geo_Controller.deleteNewGeo
        (
            geo.name, 
            toast_geo.success.delete
        )
    })
})