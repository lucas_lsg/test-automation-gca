/// <reference types="cypress" />
import Channel_Controller from '../../Controller/Channel_Controller.cy.js';
import Country_Controller from '../../Controller/Country_Controller.cy.js'
import Geo_Controller from '../../Controller/Geo_Controller.cy.js'
import Customer_Controller from '../../Controller/Customer_Controller.cy.js';
import SignGoogle_Controller from '../../Controller/SignGoogle_Controller.cy.js'
import Channel_Model from '../../Model/Channel_Model.cy.js';
import Country_Model from '../../Model/Country_Model.cy.js';
import Customer_Model from '../../Model/Customer_Model.cy.js';
import Geo_Model from '../../Model/Geo_Model.cy.js';
import Toast_Model from '../../Model/Toast_Model.cy.js';

let signGoogle_Controller = new SignGoogle_Controller;
let channel_Controller = new Channel_Controller;
let country_Controller = new Country_Controller;
let geo_Controller = new Geo_Controller;
let customer_Controller = new Customer_Controller;
let channel_Model = new Channel_Model
let country_Model = new Country_Model
let geo_Model = new Geo_Model
let customer_Model = new Customer_Model;
let toast_Model = new Toast_Model

const channel = channel_Model.testData.channel
const country = country_Model.testData.country
const customer = customer_Model.testData.customer
const geo = geo_Model.testData.geo
const toast_channel = toast_Model.testData.toast_channel
const toast_country = toast_Model.testData.toast_country
const toast_geo = toast_Model.testData.toast_geo
const toast_customer = toast_Model.testData.toast_customer

describe(`
    Given I'm Home page
    When I'm going to the Channel page
    Then filters should be working correctly
    And I should be able to crate new Channel
    And I should be able to update Channel
    And I should be able to delete Channel
    And your confirmations toast should appear correctly`,()=>{
    beforeEach('Open home page',()=>{
        signGoogle_Controller.signGooglePage()
    })
    it('Check all UI elements from Channel page',()=>{
        channel_Controller.checkElements()
    })
    it(`Try create new Channel associated with new country`,()=>{
        geo_Controller.createNewGeo
        (
            geo.name, 
            toast_geo.success.create
        )
        country_Controller.createNewCountry
        (
            country.name,
            geo.name,
            country.iso_code,
            toast_country.success.create
        )
        channel_Controller.createNewChannel
        (
            country.name,
            channel.name_associated,
            channel.description,
            toast_channel.success.create
        )
    })
    it(`Try create Channel without name`,()=>{
        channel_Controller.createNewChannel
        (
            country.name,
            channel.empty,
            channel.description,
            toast_channel.warning.channel_empty
        )
    })
    it(`Try create Channel without description`,()=>{
        channel_Controller.createNewChannel
        (
            country.name,
            channel.name,
            channel.empty,
            toast_channel.warning.description_empty
        )
    })
    it(`Try create Channel without country`,()=>{
        channel_Controller.createNewChannel
        (
            country.empty,
            channel.name,
            channel.description,
            toast_channel.warning.country_empty
        )
    })
    it(`Try create duplicated Channel`,()=>{
        channel_Controller.createNewChannel
        (
            country.name,
            channel.name_associated,
            channel.description,
            toast_channel.fail.create_duplicated
        )
    })
    it(`Try create Channel with invalid name`,()=>{
        channel_Controller.createNewChannel
        (
            channel.country.BR,
            channel.invalid_item,
            channel.description,
            toast_channel.warning.channel_empty
        )
    })
    it(`Try create Channel with invalid description`,()=>{
        channel_Controller.createNewChannel
        (
            channel.country.BR,
            channel.name_associated,
            channel.invalid_item,
            toast_channel.warning.description_empty
        )
    })
    it(`Try delete Channel associated with a Customer`,()=>{

        customer_Controller.createNewCustomerShort
        (
            customer.customer_short,
            customer.customer_PO_line,
            channel.name_associated,
            country.name,
            customer.status.off,
            customer.taxes_application.list1,
            toast_customer.success.create
        )
        channel_Controller.deleteNewChannel
        (
            country.name,
            channel.name_associated,
            toast_channel.fail.delete_associated
        )
    })
    it(`Try edit last Channel`,()=>{
        channel_Controller.updateNewChannel
        (
            country.name,
            channel.country.AR,
            channel.name_associated,
            toast_channel.success.edit
        )
    })
    it('Try filter by not found Channel',()=>{
        channel_Controller.checkNotFoundChannel(channel.invalid_item)
    })
    it(`Try delete last Channel`,()=>{
        channel_Controller.createNewChannel
        (
            country.name,
            channel.name,
            channel.description,
            toast_channel.success.create
        )
        channel_Controller.deleteNewChannel
        (
            country.name,
            channel.name,
            toast_channel.success.delete
        )
    })
})