/// <reference types="cypress" />
import SignGoogle_Controller from '../../Controller/SignGoogle_Controller.cy.js';
import ElementsSetupCRSDPage from '../../View/ElementsCRSDPage.cy.js';

let signGoogle_Controller = new SignGoogle_Controller;
let elementsCRSDPage = new ElementsSetupCRSDPage

describe(`Check UI elements from CRSD Page`,()=>{
    beforeEach('Open home page',()=>{
        signGoogle_Controller.signGooglePage()
        elementsCRSDPage.getSetup_Card().contains('setup').click()
        elementsCRSDPage.getCrsd_Card().contains('crsd').click()
        cy.wait(200)
    })
    it('Check breadcrumb',()=>{
       elementsCRSDPage.getBreadcrumb_HOME().contains('Home')
       elementsCRSDPage.getBreadcrumb_SETUP().contains('setup')
       elementsCRSDPage.getBreadcrumb_CRSD().contains('crsd')
    })
    it('Check title page',()=>{
       elementsCRSDPage.getTitle_Page().contains('CRSD')
    })
    it('Check Fiscal Year filter',()=>{
       elementsCRSDPage.getInputList_Year()
    })
    it('Check Calendar CRSD',()=>{
        elementsCRSDPage.getCalendar_CRSD()
    })
    it('Check Back button', ()=>{
        elementsCRSDPage.getBack_Btn().contains('BACK')
    })
})