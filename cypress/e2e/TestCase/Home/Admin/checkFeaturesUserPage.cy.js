/// <reference types="cypress" />
import User_Controller from '../../../Controller/User_Controller.cy.js'
import SignGoogle_Controller from '../../../Controller/SignGoogle_Controller.cy.js'
import User_Model from '../../../Model/User_Model.cy.js'
import Toast_Model from '../../../Model/Toast_Model.cy.js'

let user_Controller = new User_Controller
let signGoogle_Controller = new SignGoogle_Controller;
let user_Model = new User_Model
let toast_Model = new Toast_Model

const user = user_Model.testData.user
const toast_user = toast_Model.testData.toast_user

describe(`
    Given I'm Home page
    When I'm going to the User page
    Then filters should be working correctly
    And I should be able to create new User
    And I should be able to update User
    And I should be able to delete User
    And your confirmations toast should appear correctly`,()=>{
    beforeEach('Open home page',()=>{
        signGoogle_Controller.signGooglePage()
    })
    it('Check all UI elements from User page',()=>{
        user_Controller.checkElements()
    })
    it('Try create new user correctly',()=>{
        user_Controller.createNewUser
        (
            user.last_name,
            user.name,
            user.email,
            user.country.AR,
            user.status.off,
            user.profile,
            user.sales_manager.on,
            user.order_adm.off,
            toast_user.success.create
        )
    })
    it('Try create new user without last name',()=>{
        user_Controller.createNewUser
        (
            user.empty,
            user.name,
            user.email,
            user.country.AR,
            user.status.off,
            user.profile,
            user.sales_manager.on,
            user.order_adm.off,
            toast_user.warning.last_name_empty
        )
    })
    it('Try create new user without name',()=>{
        user_Controller.createNewUser
        (
            user.last_name,
            user.empty,
            user.email,
            user.country.AR,
            user.status.off,
            user.profile,
            user.sales_manager.on,
            user.order_adm.off,
            toast_user.warning.name_empty
        )
    })
    it('Try create new user without email',()=>{
        user_Controller.createNewUser
        (
            user.last_name,
            user.name,
            user.empty,
            user.country.AR,
            user.status.off,
            user.profile,
            user.sales_manager.on,
            user.order_adm.off,
            toast_user.warning.email_empty
        )
    })
    it('Try create new user without country',()=>{
        user_Controller.createNewUser
        (
            user.last_name,
            user.name,
            user.email,
            user.empty,
            user.status.off,
            user.profile,
            user.sales_manager.on,
            user.order_adm.off,
            toast_user.warning.country_empty
        )
    })
    it('Try create new user without status',()=>{
        user_Controller.createNewUser
        (
            user.last_name,
            user.name,
            user.email,
            user.country.AR,
            user.empty,
            user.profile,
            user.sales_manager.on,
            user.order_adm.off,
            toast_user.warning.status_empty
        )
    })
    it('Try create new user without profile',()=>{
        user_Controller.createNewUser
        (
            user.last_name,
            user.name,
            user.email,
            user.country.AR,
            user.status.off,
            user.empty,
            user.sales_manager.on,
            user.order_adm.off,
            toast_user.warning.profile_empty
        )
    })
    it('Try create new user without sales manager',()=>{
        user_Controller.createNewUser
        (
            user.last_name,
            user.name,
            user.email,
            user.country.AR,
            user.status.off,
            user.profile,
            user.empty,
            user.order_adm.off,
            toast_user.warning.salesManager_empty
        )
    })
    it('Try create new user without order admin',()=>{
        user_Controller.createNewUser
        (
            user.last_name,
            user.name,
            user.email,
            user.country.AR,
            user.status.off,
            user.profile,
            user.sales_manager.off,
            user.empty,
            toast_user.warning.orderAdmin_empty
        )
    })
    it('Try create new user with invalid email',()=>{
        user_Controller.createNewUser
        (
            user.last_name,
            user.name,
            user.invalid_email,
            user.country.AR,
            user.status.off,
            user.profile,
            user.sales_manager.on,
            user.order_adm.off,
            toast_user.warning.invalid_email
        )
    })
    it('Try create user duplicated',()=>{
        user_Controller.checkDuplicatedUser
        (
            user.last_name,
            user.name,
            user.email,
            user.country.AR,
            user.status.off,
            user.profile,
            user.sales_manager.off,
            user.order_adm.on,
            toast_user.fail.create_duplicated
        )
    })
    it('Try create new user with invalid name',()=>{
        user_Controller.createNewUser
        (
            user.last_name,
            user.invalid_name,
            user.email,
            user.country.AR,
            user.status.off,
            user.profile,
            user.sales_manager.on,
            user.order_adm.off,
            toast_user.warning.name_empty
        )
    })
    it('Try create new user with invalid last name',()=>{
        user_Controller.createNewUser
        (
            user.invalid_lastName,
            user.name,
            user.email,
            user.country.AR,
            user.status.off,
            user.profile,
            user.sales_manager.on,
            user.order_adm.off,
            toast_user.warning.last_name_empty
        )
    })
    it('Try edit user correctly',()=>{
        user_Controller.updateNewUser
        (
            user.last_name,
            user.name,
            user.new_email,
            user.country.AR,
            user.country.BR,
            user.status.off,
            user.status.on,
            user.profile,
            user.new_profile,
            user.sales_manager.off,
            user.order_adm.on,
            toast_user.success.edit
        )
    })
    it('Try delete user correctly',()=>{
        user_Controller.deleteNewUser
        (
            user.last_name,
            user.name,
            user.country.BR,
            user.status.on,
            user.new_profile,
            toast_user.success.delete
        )
    })
    it('Try filter by not found user',()=>{
        user_Controller.checkNotFoundUser
        (
            user.name,
            user.status.on
        )
    })
})