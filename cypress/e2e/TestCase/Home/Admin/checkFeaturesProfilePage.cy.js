/// <reference types="cypress" />
import Profile_Controller from '../../../Controller/Profile_Controller.cy.js'
import SignGoogle_Controller from '../../../Controller/SignGoogle_Controller.cy.js'
import Profile_Model from '../../../Model/Profile_Model.cy.js'
import User_Model from '../../../Model/User_Model.cy.js';
import Toast_Model from '../../../Model/Toast_Model.cy.js';
import User_Controller from '../../../Controller/User_Controller.cy.js';

let signGoogle_Controller = new SignGoogle_Controller;
let profile_Controller = new Profile_Controller
let profile_Model = new Profile_Model
let user_Model = new User_Model
let toast_Model = new Toast_Model
let user_Controller = new User_Controller

const profile = profile_Model.testData.profile
const user = user_Model.testData.user
const toast_profile = toast_Model.testData.toast_profile
const toast_user = toast_Model.testData.toast_user

describe(`
    Given I'm Home page
    When I'm going to the Profile page
    Then filters should be working correctly
    And I should be able to create new Profile
    And I should be able to update Profile
    And I should be able to delete Profile
    And your confirmations toast should appear correctly`,()=>{
   
    beforeEach('Open home page',()=>{
        signGoogle_Controller.signGooglePage()
    })
    it('First check all UI elements from Profile page', ()=>{
        profile_Controller.checkElements()
    })
    it(`Try create new Profile correctly`,()=>{
        profile_Controller.createNewProfileAllPermissions
        (
            profile.name,
            profile.status.off,
            profile.country.list1, 
            toast_profile.success.create
        )
    })
    it(`Try edit Profile correctly`,()=>{
        profile_Controller.updateNewProfile
        (
            profile.name,
            profile.status.off,
            profile.status.on,
            profile.country.BR,
            profile.country.list2, 
            toast_profile.success.edit
        )
    })
    it(`Try create new Profile without action associated`,()=>{
        profile_Controller.createNewProfileWithoutAction
        (
            profile.name,
            profile.status.on,
            profile.country.list1, 
            toast_profile.warning.create_noAction
        )
    })
    it(`Try create new Profile without name`,()=>{
        profile_Controller.createNewProfileWithoutAction
        (
            profile.empty,
            profile.status.on,
            profile.country.list1, 
            toast_profile.warning.name_empty
        )
    })
    it(`Try create new Profile without status`,()=>{
        profile_Controller.createNewProfileWithoutAction
        (
            profile.name,
            profile.empty,
            profile.country.list1, 
            toast_profile.warning.status_empty
        )
    })
    it(`Try create new Profile without country`,()=>{
        profile_Controller.createNewProfileWithoutAction
        (
            profile.name,
            profile.status.on,
            profile.empty, 
            toast_profile.warning.country_empty
        )
    })
    it(`Try create duplicated Profile`,()=>{
        profile_Controller.createDuplicatedProfile
        (
            profile.name_duplicated,
            profile.status.on,
            profile.country.BR,
            toast_profile.fail.create_duplicated
        )
    })
    it(`Try delete Profile associated with an user`,()=>{
        profile_Controller.createNewProfileAdministrationPermissions
        (
            profile.name_associated,
            profile.status.on,
            profile.country.list1,
            toast_profile.success.create
        )
        user_Controller.createNewUser
        (
            user.last_name, 
            user.name, 
            user.email,
            user.country.BR,
            user.status.on,
            profile.name_associated,
            user.sales_manager.on,
            user.order_adm.off,
            toast_user.success.create
        )
        profile_Controller.deleteNewProfile
        (
            profile.name_associated,
            profile.status.on,
            profile.country.BR, 
            toast_profile.fail.delete_associated
        )
    })
    it(`Try filter by not found profile`,()=>{
        profile_Controller.checkNotFoundProfile
        (
            profile.name_notFound,
            profile.status.on
        )
    })
    it(`Try delete test Profiles correctly`,()=>{

        profile_Controller.deleteNewProfile
        (
            profile.name_duplicated,
            profile.status.on,
            profile.country.BR, 
            toast_profile.success.delete
        )
        user_Controller.deleteNewUser
        (
            user.last_name, 
            user.name,
            user.country.BR,
            user.status.on,
            profile.name_associated,
            toast_user.success.delete
        )
        profile_Controller.deleteNewProfile
        (
            profile.name_associated,
            profile.status.on,
            profile.country.BR, 
            toast_profile.success.delete
        )
        
        cy.wait(3000)

        profile_Controller.deleteNewProfile
        (
            profile.name,
            profile.status.on,
            profile.country.BR, 
            toast_profile.success.delete
        )
        cy.wait(3000)
    })
})