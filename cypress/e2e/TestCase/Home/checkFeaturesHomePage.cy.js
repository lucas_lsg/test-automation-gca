/// <reference types="cypress" />
import Home_Controller from '../../Controller/Home_Controller.cy.js';
import SignGoogle_Controller from '../../Controller/SignGoogle_Controller.cy.js';
let home_Controller = new Home_Controller;
let signGoogle_Controller = new SignGoogle_Controller;

describe(`
  Given I'm Home page
  When I'm navigation to the Home page
  Then I should see title from Home page
  And each filter should be working correctly
  And compare with value returned in the table`,()=>{
    beforeEach('Open home page',()=>{
        signGoogle_Controller.signGooglePage()
    })
    it('Check elements and Navigation',()=>{
        home_Controller.checkElementsAndNavigation()
    })
})