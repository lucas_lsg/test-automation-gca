/// <reference types="cypress" />
import SignGoogle_Controller from '../../Controller/SignGoogle_Controller.cy.js';
import KA_Allocation_Controller from '../../Controller/KA_Allocation_Controller.cy.js';
let signGoogle_Controller = new SignGoogle_Controller;
let ka_Allocation_Controller = new KA_Allocation_Controller
describe(`Compare data from query with payload`,()=>{
    beforeEach('Get access token',()=>{
        signGoogle_Controller.signGooglePage()
    })
    it('Check total Demand and Allocation values from Deal Vs Allocation page ',()=>{
        cy.wait(10000)
        ka_Allocation_Controller.checkDemandValue_DealVsAlloc()
        ka_Allocation_Controller.checkAllocationValue_DealVsAlloc()
    })
    it('Check total Demand and Allocation values from Demand Vs MLS page',()=>{
        cy.wait(10000)
        ka_Allocation_Controller.checkDemandValue_DemandVsMls()
        ka_Allocation_Controller.checkAllocationValue_DemandVsMls()
    })
})