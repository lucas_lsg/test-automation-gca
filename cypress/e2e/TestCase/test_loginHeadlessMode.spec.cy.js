/// <reference types="cypress" />
import SignGoogle_Controller from '../Controller/SignGoogle_Controller.cy.js'
let signGoogle_Controller = new SignGoogle_Controller;
describe(`Test login with session method to keep access token in cache memory`,()=>{
   
    before('Try create session in headless', () => {
        cy.login(
            Cypress.env("EMAIL"),
            Cypress.env("PASSWORD")
          )
    });
    it('Check if access app in headless mode',()=>{
        cy.visit('/home')
        signGoogle_Controller.signGooglePage()
        cy.get('span[class="navbar-title"]').should("contain", "GLOBAL CUSTOMER ALLOCATION");
        //cy.get('h1').should("contain","Home")
    })
})

  