export default class ElementsSetupCustomerPage {
    getTitle_Page(){
        return cy.get('span[class="title"]',{timeout:60000}).contains('CUSTOMER SETTINGS')
    }
    getButton_NewCustomer(){
        return cy.get('span[class="mat-button-wrapper"]',{timeout:60000}).contains('NEW CUSTOMER SHORT')
    }
    getInputList_Geo(){
        return cy.get('input[ng-reflect-name="geoId"]',{timeout:60000}).should('be.visible')
    }
    getInputList_Country(){
        return cy.get('input[ng-reflect-name="countryId"]',{timeout:60000}).should('be.visible')
    }
    getSelectList_Status(){
        return cy.get('mat-select[ng-reflect-name="status"]',{timeout:60000}).should('be.visible')
    }
    getInputList_CustomerShort(){
        return cy.get('input[ng-reflect-name="customerShort"]',{timeout:60000}).should('be.visible')
    }
    getTitle_Modal(){
        return cy.get('header[class="mat-dialog-title title-modal"]',{timeout:60000}).contains('NEW CUSTOMER SHORT')
    }
    getTitle_EditModal(){
        return cy.get('header[class="mat-dialog-title title-modal"]',{timeout:60000}).contains('EDIT CUSTOMER SHORT')
    }
    getInput_CustomerShort_Modal(){
        return cy.get('input[formcontrolname="customerShort"]',{timeout:60000}).should('be.visible')
    }
    getInput_CustomerPOLineNumber_Modal(){
        return cy.get('input[formcontrolname="customerPoLineNumber"]',{timeout:60000}).should('be.visible')
    }
    getSelect_Channel_Modal(){
        return cy.get('mat-select[ng-reflect-placeholder="Channel"]',{timeout:60000}).should('be.visible')
    }
    getSelect_Country_Modal(){
        return cy.get('mat-select[ng-reflect-placeholder="Country"]',{timeout:60000}).should('exist')
    }
    getSelect_OrderAdmin_Modal(){
        return cy.get('mat-select[ng-reflect-placeholder="Order Admin"]',{timeout:60000}).should('be.visible')
    }
    getSelect_SalesManager_Modal(){
        return cy.get('mat-select[ng-reflect-placeholder="Sales Manager"]',{timeout:60000}).should('be.visible')
    }
    getSelect_Status_Modal(){
        return cy.get('mat-select[formcontrolname="status"]',{timeout:60000}).should('be.visible')
    }
    getSelect_TaxesApplication_Modal(){
        return cy.get('mat-select[formcontrolname="taxes"]',{timeout:60000}).should('be.visible')
    }
    getSelect_CustomerNumbers_Modal(){
        return cy.get('mat-select[ng-reflect-placeholder="Customer Number"]',{timeout:60000}).should('be.visible')
    }
    getButton_Delete_Modal(){
        return cy.get('.button-delete',{timeout:60000})
    }
    getLink_CustomerShort(){
        return cy.get('div[class="mat-tab-label-content"]',{timeout:60000}).contains('CUSTOMER SHORT')
    }
    getEdit_Icon(){
        return cy.get('img[ng-reflect-message="Edit"]',{timeout:60000}).should('be.visible')
    }
    getLink_CustomerList(){
        return cy.get('div[class="mat-tab-label-content"]',{timeout:60000}).contains('CUSTOMER LIST')
    }
    getInputList_CustomerNumber(){
        return cy.get('input[ng-reflect-name="customerNumber"]',{timeout:60000}).should('be.visible')
    }
    getInputList_CustomerName(){
        return cy.get('input[ng-reflect-name="customerName"]',{timeout:60000}).should('be.visible')
    }
    getBtn_NotAssociated(){
        return cy.get('.button-filter > :nth-child(1)',{timeout:60000}).contains('Not associated')
    }
    getBtn_Associated(){
        return cy.get('button[class="ng-star-inserted"]',{timeout:60000}).contains('Associated')
    }
    getBtn_All(){
        return cy.get('button[class="ng-star-inserted"]',{timeout:60000}).contains('All')
    }
    getGeo_Table(n){
        return cy.get(`:nth-child(${n}) > .cdk-column-geo > .ng-star-inserted`,{timeout:60000}).should('be.visible')
    }
    getCountry_Table(n){
        return cy.get(`:nth-child(${n}) > .cdk-column-country > .ng-star-inserted`,{timeout:60000}).should('be.visible')
    }
    getCollapse_icon(){
        return cy.get('.mat-expansion-indicator',{timeout:60000})
    }
    getCustomerNumber_Table(n){
        return cy.get(`:nth-child(${n}) > .cdk-column-customer-number > .ng-star-inserted`,{timeout:60000}).should('be.visible')
    }
    getCustomerShort_Table(n){
        return cy.get(`.mat-expansion-panel-body > :nth-child(${n})`,{timeout:60000}).should('be.visible')
    }
    getCustomerName_Table(n){
        return cy.get(`:nth-child(${n}) > .cdk-column-customer-name > .ng-star-inserted`,{timeout:60000}).should('be.visible')
    }

}
