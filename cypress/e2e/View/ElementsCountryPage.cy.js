export default class ElementsSetupCountryPage {
    getTitle_Page(){
        return cy.get('span[class="title"]',{timeout:60000}).contains('COUNTRY')
    }
    getInputList_Geo(){
        return cy.get('input[ng-reflect-name="geo_id"]',{timeout:60000}).should('be.visible')
    }
    getInputList_Country(){
        return cy.get('input[ng-reflect-name="country"]',{timeout:60000}).should('be.visible')
    }
    getInputList_ISO_Code(){
        return cy.get('input[ng-reflect-name="iso_code"]',{timeout:60000}).should('be.visible')
    }
    getButton_New_Country(){
        return cy.get('span[class="mat-button-wrapper"]',{timeout:60000}).contains('NEW COUNTRY')
    }
    getTitle_Modal(){
        return cy.get('header[class="mat-dialog-title title-modal"]',{timeout:60000}).contains('NEW COUNTRY')
    }
    getInput_Geo_Modal(){
        return cy.get('input[formcontrolname="geo_id"]',{timeout:60000}).should('be.visible')
    }
    getInput_IsoCode_Modal(){
        return cy.get('input[formcontrolname="iso_code"]',{timeout:60000}).should('be.visible')
    }
    getInput_Country_Modal(){
        return cy.get('input[formcontrolname="country"]',{timeout:60000}).should('be.visible')
    }
    checkNoResultsFound(){
        return cy.get('span[class="result-length ng-star-inserted"]',{timeout:60000})
    }
}