export default class ElementsSetupChannelPage {
    getTitle_Page(){
        return cy.get('span[class="title"]',{timeout:60000}).contains('CHANNEL')
    }
    getInputList_Geo(){
        return cy.get('input[ng-reflect-name="geo_id"]',{timeout:60000}).should('be.visible')
    }
    getInputList_Country(){
        return cy.get('input[ng-reflect-name="country_id"]',{timeout:60000}).should('be.visible')
    }
    getInputList_Channel(){
        return cy.get('input[ng-reflect-name="channel"]',{timeout:60000}).should('be.visible')
    }
    getButton_New_Channel(){
        return cy.get('span[class="mat-button-wrapper"]',{timeout:60000}).contains('NEW CHANNEL')
    }
    getTitle_Modal(){
        return cy.get('header[class="mat-dialog-title title-modal"]',{timeout:60000}).contains('NEW CHANNEL')
    }
    getInput_Country_Modal(){
        return cy.get('input[formcontrolname="country_id"]',{timeout:60000}).should('be.visible')
    }
    getInput_Channel_Modal(){
        return cy.get('input[formcontrolname="channel"]',{timeout:60000}).should('be.visible')
    }
    getInput_Description_Modal(){
        return cy.get('input[formcontrolname="description"]',{timeout:60000}).should('be.visible')
    }
    checkNoResultsFound(){
        return cy.get('span[class="result-length ng-star-inserted"]',{timeout:60000})
    }
}