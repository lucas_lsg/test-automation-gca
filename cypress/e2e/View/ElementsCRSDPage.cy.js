export default class ElementsSetupCRSDPage {
    getTitle_Page(){
        return cy.get('div[class="title"]',{timeout:60000})
    }
    getInputList_Year(){
        return cy.get('mat-select[ng-reflect-name="year"]',{timeout:60000}).should('be.visible')
    }
    getBreadcrumb_HOME(){
        return cy.get('a[ng-reflect-router-link="/home" ]',{timeout:60000}).should('be.visible')
    }
    getBreadcrumb_SETUP(){
        return cy.get('a[ng-reflect-router-link="/home/setup"]',{timeout:60000}).should('be.visible')
    }
    getBreadcrumb_CRSD(){
        return cy.get('label[class="xng-breadcrumb-trail ng-star-inserted"]',{timeout:60000}).should('be.visible')
    }
    getCalendar_CRSD(){
        return cy.get('div[class="calendar-crsd"]',{timeout:60000}).should('be.visible')
    }
    getBack_Btn(){
        return cy.get('span[class="mat-button-wrapper"]',{timeout:60000}).should('be.visible')
    }
    getSetup_Card(){
        return cy.get('div[class="card-button-title"]',{timeout:60000}).should('be.visible')
    }
    getCrsd_Card(){
        return cy.get('div[class="card-button-title"]',{timeout:60000}).should('be.visible')
    }
}