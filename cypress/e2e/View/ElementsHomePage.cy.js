export default class ElementsHomePage {
    getTitle_Page(){
        return cy.get('div[class="title"]',{timeout:60000}).contains('Home').should('be.visible')
    }
    getCard_Admin(){
        return cy.get('div[class="card-button-title"]',{timeout:60000}).contains('admin')
    }
    getCard_AdminUser(){
        return cy.get('div[class="card-button-title"]',{timeout:60000}).contains('User')
    }
    getCard_AdminProfile(){
        return cy.get('div[class="card-button-title"]',{timeout:60000}).contains('Profile')
    }
    getCard_CustAllocRetail(){
        return cy.get('div[class="card-button-title"]',{timeout:60000}).contains('customer allocation retail')
    }
    getCard_CustAllocRetailAlRt(){
        return cy.get('div[class="card-button-title"]',{timeout:60000}).contains('Allocation Retail')
    }
    getCard_CustAllocRetailAlRtPPM(){
        return cy.get('div[class="card-button-title"]',{timeout:60000}).contains('PPM')
    }
    getCard_KA_Allocation(){
        return cy.get('div[class="card-button-title"]',{timeout:60000}).contains('ka allocation')
    }
    getCard_KA_AllocationManag(){
        return cy.get('div[class="card-button-title"]',{timeout:60000}).contains('KA Management')
    }
    getCard_KA_AllocationKaAlloc(){
        return cy.get('div[class="card-button-title"]',{timeout:60000}).contains('KA Allocation')
    }
    getCard_KA_AllocationBRDemands(){
        return cy.get('div[class="card-button-title"]',{timeout:60000}).contains('Brazil Demands')
    }
    getCard_KA_AllocationLOM(){
        return cy.get('div[class="card-button-title"]',{timeout:60000}).contains('Lom')
    }
    getCard_Setup(){
        return cy.get('div[class="card-button-title"]',{timeout:60000}).contains('setup')
    }
    getCard_SetupCountry(){
        return cy.get('div[class="card-button-title"]',{timeout:60000}).contains('Country')
    }
    getCard_SetupGeo(){
        return cy.get('div[class="card-button-title"]',{timeout:60000}).contains('Geo')
    }
    getCard_SetupSalesManag(){
        return cy.get('div[class="card-button-title"]',{timeout:60000}).contains('sales management')
    }
    getCard_SetupOrderADM(){
        return cy.get('div[class="card-button-title"]',{timeout:60000}).contains('Order Admin Management')
    }
    getCard_SetupSManag(){
        return cy.get('div[class="card-button-title"]',{timeout:60000}).contains('setup management')
    }
    getCard_SetupChannel(){
        return cy.get('div[class="card-button-title"]',{timeout:60000}).contains('Channel')
    }
    getCard_SetupCustomerSettings(){
        return cy.get('div[class="card-button-title"]',{timeout:60000}).contains('customer settings')
    }
    getCard_SetupFiscalCalendar(){
        return cy.get('div[class="card-button-title"]',{timeout:60000}).contains('Fiscal Calendar')
    }
    getCard_SetupMaterials(){
        return cy.get('div[class="card-button-title"]',{timeout:60000}).contains('materials management')
    }
}