export default class ElementsMaterialsPage {
    getTitle_Page(){
        return cy.get('span[class="title"]',{timeout:60000}).contains('MATERIALS')
    }
    getInput_Geo(){
        return cy.get('input[ng-reflect-name="geoId"]',{timeout:60000}).should('be.visible')
    }
    getInput_Country(){
        return cy.get('input[ng-reflect-name="countryId"]',{timeout:60000}).should('be.visible')
    }
    getInput_InternalName(){
        return cy.get('input[ng-reflect-name="internalName"]',{timeout:60000}).should('be.visible')
    }
    getInput_SalesModel(){
        return cy.get('input[ng-reflect-name="salesModel"]',{timeout:60000}).should('be.visible')
    }
    getInput_Origin(){
        return cy.get('mat-select[ng-reflect-name="origin"]',{timeout:60000}).should('be.visible')
    }
    getLink_AllFilters(){
        return cy.get('.link',{timeout:60000}).contains('All filters')
    }
    getInput_Status(){
        return cy.get('mat-select[ng-reflect-name="status"]',{timeout:60000}).should('be.visible')
    }
    getInput_Ean(){
        return cy.get('input[ng-reflect-name="upcCode"]',{timeout:60000}).should('be.visible')
    }
    getInput_ColorDesc(){
        return cy.get('input[ng-reflect-name="colorDesc"]',{timeout:60000}).should('be.visible')
    }
    getInput_MarketName(){
        return cy.get('input[ng-reflect-name="marketName"]',{timeout:60000}).should('be.visible')
    }
    getInput_AllocationBz(){
        return cy.get('input[ng-reflect-name="allocationBz"]',{timeout:60000}).should('be.visible')
    }
    getInput_ProductGroup(){
        return cy.get('input[ng-reflect-name="productGroup"]',{timeout:60000}).should('be.visible')
    }
    getLink_LessFilters(){
        return cy.get('.link',{timeout:60000}).contains('Less filters')
    }
    getTitle_EditModal(){
        return cy.get('header[class="mat-dialog-title title-modal"]',{timeout:60000}).contains('EDIT MATERIAL')
    }
    getInput_LocalMarketName(){
        return cy.get('input[ng-reflect-name="localMarketName"]',{timeout:60000}).should('be.visible')
    }
    getInput_LocalColorDesc(){
        return cy.get('input[ng-reflect-name="localColorDesc"]',{timeout:60000}).should('be.visible')
    }
    getInput_ExportIcon(){
        return cy.get('button[class="export-button"]',{timeout:60000}).should('be.visible')
    }
    getInternalName_results(results_number){
        return cy.get(`:nth-child(${results_number}) > .cdk-column-internal-name > .ng-star-inserted`)
    }
    getColorDesc_results(results_number){
        return cy.get(`:nth-child(${results_number}) > .cdk-column-color-desc > .ng-star-inserted`)
    }
    getMarketName_results(results_number){
        return cy.get(`:nth-child(${results_number}) > .cdk-column-market-name > .ng-star-inserted`)
    }
    getEAN_results(results_number){
        return cy.get(`:nth-child(${results_number}) > .cdk-column-ean > .ng-star-inserted`)
    }
    getProductGroup_results(results_number){
        return cy.get(`:nth-child(${results_number}) > .cdk-column-product-group > .ng-star-inserted`)
    }
    getOrigin_results(results_number){
        return cy.get(`:nth-child(${results_number}) > .cdk-column-origin > .ng-star-inserted`)
    }
    getStatus_results(results_number){
        return cy.get(`:nth-child(${results_number}) > .cdk-column-status > .ng-star-inserted`)
    }
}
