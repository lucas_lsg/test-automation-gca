export default class ElementsFiscalCalendarPage {
    getTitle_Page(){
        return cy.get('span[class="title"]',{timeout:60000}).contains('FISCAL CALENDAR')
    }
    getInputList_Year(){
        return cy.get('mat-select[ng-reflect-name="year"]',{timeout:60000}).should('be.visible')
    }
    getInputList_Quarter(){
        return cy.get('mat-select[ng-reflect-name="quarter"]',{timeout:60000}).should('be.visible')
    }
    getInputList_Week(){
        return cy.get('input[ng-reflect-name="week"]',{timeout:60000}).should('be.visible')
    }
    getInputList_Date(){
        return cy.get('input[ng-reflect-name="date"]',{timeout:60000}).should('be.visible')
    }
    getCalendarIcon(){
        return cy.get('.mat-datepicker-toggle > .mat-focus-indicator',{timeout:60000}).should('be.visible')
    }
    getOneDay(){
        return cy.get('div[class="mat-calendar-body-cell-content mat-focus-indicator"]',{timeout:60000}).should('be.visible')
    }
    getFiscalYear_Table(){
        return cy.get('.cdk-column-fiscal-year > .ng-star-inserted',{timeout:60000}).should('be.visible')
    }
    getQuarter_Table(){
        return cy.get('.cdk-column-quarter > .ng-star-inserted',{timeout:60000}).should('be.visible')
    }
    getWeek_Table(){
        return cy.get(':nth-child(1) > .cdk-column-week > .ng-star-inserted',{timeout:60000}).should('be.visible')
    }
    getDay_Table(){
        return cy.get('.cdk-column-days > .ng-star-inserted',{timeout:60000}).should('be.visible')
    }
    getClearDate_Icon(){
        return cy.get('.mat-form-field-infix > .mat-icon',{timeout:60000}).should('be.visible')
    }
    getClearWeek_Icon(){
        return cy.get(':nth-child(3) > .ng-tns-c185-2 > div.ng-valid > [ng-reflect-ng-switch="search"] > :nth-child(1) > .mat-form-field > .mat-form-field-wrapper > .mat-form-field-flex > .mat-form-field-suffix > .mat-focus-indicator > .mat-button-wrapper > .mat-icon',{timeout:60000}).should('be.visible')
    }
    getClearQuarter_Icon(){
        return cy.get('div.ng-untouched > [ng-reflect-ng-switch="search"] > :nth-child(1) > .mat-form-field > .mat-form-field-wrapper > .mat-form-field-flex > .mat-form-field-suffix > .mat-focus-indicator > .mat-button-wrapper > .mat-icon',{timeout:60000}).should('be.visible')
    }
}