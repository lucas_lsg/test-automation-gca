export default class ElementsSetupGeoPage {
    getTitle_Page(){
        return cy.get('span[class="title"]',{timeout:60000}).contains('GEO')
    }
    getInputList_Geo(){
        return cy.get('input[ng-reflect-name="geo"]',{timeout:60000}).should('be.visible')
    }
    getButton_New_Geo(){
        return cy.get('span[class="mat-button-wrapper"]',{timeout:60000}).contains('NEW GEO')
    }
    getTitle_Modal(){
        return cy.get('header[class="mat-dialog-title title-modal"]',{timeout:60000}).contains('NEW GEO')
    }
    getInput_Geo_Modal(){
        return cy.get('input[formcontrolname="geo"]',{timeout:60000}).should('be.visible')
    }
    checkNoResultsFound(){
        return cy.get('span[class="result-length ng-star-inserted"]',{timeout:60000})
    }
}
