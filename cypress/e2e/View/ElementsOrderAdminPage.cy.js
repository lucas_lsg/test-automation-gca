export default class ElementsSetupOrderAdminPage {
    getTitle_Page(){
        return cy.get('span[class="title"]',{timeout:60000}).contains('ORDER ADMIN MANAGEMENT')
    }
    getSelect_Geo(){
        return cy.get('input[ng-reflect-name="geoId"]',{timeout:60000}).should('be.visible')
    }
    getSelect_Country(){
        return cy.get('input[ng-reflect-name="countryId"]',{timeout:60000}).should('be.visible')
    }
    getSelect_OrderAdmin(){
        return cy.get('input[ng-reflect-name="orderAdminFullName"]',{timeout:60000}).should('be.visible')
    }
    getTitle_Modal(){
        return cy.get('header[class="mat-dialog-title title-modal"]',{timeout:60000}).contains('EDIT ASSOCIATION ORDER ADMIN')
    }
    getSelect_SalesManager_Modal(){
        return cy.get('mat-select[ng-reflect-name="sales_manager_user"]',{timeout:60000}).should('be.visible')
    }
}