export default class ElementsActions {
    getIcon_Edit(){
        return cy.get('.cdk-column-actions > [src="../../../assets/images/ico-edit.svg"]',{timeout:60000}).should('be.visible')
    }
    getIcon_Delete(){
        return cy.get('.cdk-column-actions > [src="../../../assets/images/icon-delete.svg"]',{timeout:60000}).should('be.visible')
    }
    getFiltered_Result(){
        return cy.get('span[class="mat-option-text"]',{timeout:60000})
    }
    getButton_Yes(){
        return cy.get('span[class="mat-button-wrapper"]',{timeout:60000}).contains('YES')
    }
    getButton_No(){
        return cy.get('span[class="mat-button-wrapper"]',{timeout:60000}).contains('NO')
    }
    getTitle_Confirmation(){
        return cy.get('header[class="mat-dialog-title title-modal ng-star-inserted"]',{timeout:60000}).contains('CONFIRMATION')
    }
    getNav_Bar(){
        return cy.get('span[class="navbar-title"]',{timeout:60000}).contains('GLOBAL CUSTOMER ALLOCATION')
    }
    getButton_Back(){
        return cy.get('span[class="mat-button-wrapper"]',{timeout:60000}).contains('BACK')
    }
    getNavLinkHome(){
        return cy.get('a[ng-reflect-router-link="/home"]',{timeout:60000})
    }
    getTitleSignGoogle(){
        return cy.get('.global-costumer-allocation',{timeout:60000})
    }
    getButtonSignGoogle(){
        return cy.get('button[class="mat-focus-indicator google-sign-in mat-button mat-button-base"]',{timeout:20000})
    }
    getButton_Save(){
        return cy.get('span[class="mat-button-wrapper"]',{timeout:60000}).contains('SAVE')
    }
    getButton_Cancel(){
        return cy.get('span[class="mat-button-wrapper"]',{timeout:60000}).contains('CANCEL')
    }
    getButtonClearFilter(){
        return cy.get('mat-icon[class="mat-icon notranslate material-icons mat-icon-no-color"]',{timeout:60000}).contains('close')
    }
    getToast_Title(){
        return cy.get('.toast-title',{timeout:60000}).should('be.visible')
    }
    getToast_Message(){
        return cy.get('*[role^="alert"]',{timeout:60000}).should('be.visible')
    }
    getToast(){
        return cy.get('#toast-container > .ng-trigger',{timeout:60000})
    }
    getMultiSelect_Search(){
        return cy.get('div[ng-reflect-ng-class="multiselect-search-customer-sh"]',{timeout:60000})
    }
    getGrid_Icon(){
        return cy.get('img[mattooltip="Grid"]',{timeout:60000})
    }
    checkNoResultsFound(){
        return cy.get('span[class="result-length ng-star-inserted"]',{timeout:60000})
    }
}
