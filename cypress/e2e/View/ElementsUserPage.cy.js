export default class ElementsUserPage {
    getTitle_Page(){
        return cy.get('span[class="title"]',{timeout:60000}).contains('USER')
    }
    getInputList_LastName(){
        return cy.get('input[ng-reflect-name="last_name"]',{timeout:60000}).should('be.visible')
    }
    getInputList_Name(){
        return cy.get('input[ng-reflect-name="name"]',{timeout:60000}).should('be.visible')
    }
    getInputList_Geo(){
        return cy.get('input[ng-reflect-name="geo_id"]',{timeout:60000}).should('be.visible')
    }
    getInputList_Country(){
        return cy.get('input[ng-reflect-name="country_id"]',{timeout:60000}).should('be.visible')
    }
    getSelectList_Status(){
        return cy.get('mat-select[ng-reflect-name="status"]',{timeout:60000}).should('be.visible')
    }
    getInputList_Profile(){
        return cy.get('input[ng-reflect-name="profile_id"]',{timeout:60000}).should('be.visible')
    }
    getButton_New_User(){
        return cy.get('span[class="mat-button-wrapper"]',{timeout:60000}).contains('NEW USER')
    }
    getTitle_Modal(){
        return cy.get('header[class="mat-dialog-title title-modal"]',{timeout:60000}).contains('NEW USER')
    }
    getInput_LastName_Modal(){
        return cy.get('input[formcontrolname="last_name"]',{timeout:60000}).should('be.visible')
    }
    getInput_Name_Modal(){
        return cy.get('input[formcontrolname="name"]',{timeout:60000}).should('be.visible')
    }
    getInput_Email_Modal(){
        return cy.get('input[ng-reflect-name="email"]',{timeout:60000}).should('be.visible')
    }
    getInput_Country_Modal(){
        return cy.get('input[formcontrolname="country_id"]',{timeout:60000}).should('be.visible')
    }
    getSelect_Status_Modal(){
        return cy.get('mat-select[formcontrolname="status"]',{timeout:60000}).should('be.visible')
    }
    getInput_Profile_Modal(){
        return cy.get('input[formcontrolname="profile_id"]',{timeout:60000}).should('be.visible')
    }
    getSelect_SalesManager_Modal(){
        return cy.get('mat-select[formcontrolname="sales_manager"]',{timeout:60000}).should('be.visible')
    }
    getSelect_OrderAdm_Modal(){
        return cy.get('mat-select[formcontrolname="order_admin"]',{timeout:60000}).should('be.visible')
    }
    checkNoResultsFound(){
        return cy.get('span[class="result-length ng-star-inserted"]',{timeout:60000})
    }
}
