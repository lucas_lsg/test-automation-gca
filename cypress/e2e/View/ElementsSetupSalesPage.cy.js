export default class ElementsSetupSalesPage {
    getTitle_Page(){
        return cy.get('span[class="title"]',{timeout:60000}).contains('SALES MANAGEMENT')
    }
    getInputList_Geo(){
        return cy.get('input[ng-reflect-name="geo_id"]',{timeout:60000}).should('be.visible')
    }
    getInputList_Country(){
        return cy.get('input[ng-reflect-name="country_id"]',{timeout:60000}).should('be.visible')
    }
    getInputList_SalesManager(){
        return cy.get('input[ng-reflect-name="sales_manager"]',{timeout:60000}).should('be.visible')
    }
}