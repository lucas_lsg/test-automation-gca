export default class ElementsProfilePage {
    getTitle_Page(){
        return cy.get('span[class="title"]',{timeout:60000}).contains('PROFILE')
    }
    getInputList_Profile(){
        return cy.get('input[ng-reflect-name="profile"]',{timeout:60000}).should('be.visible')
    }
    getSelectList_Status(){
        return cy.get('mat-select[ng-reflect-name="status"]',{timeout:60000}).should('be.visible')
    }
    getInputList_Geo(){
        return cy.get('input[ng-reflect-name="geo_id"]',{timeout:60000}).should('be.visible')
    }
    getInputList_Country(){
        return cy.get('input[ng-reflect-name="country_id"]',{timeout:60000}).should('be.visible')
    }
    getButton_New_Profile(){
        return cy.get('span[class="mat-button-wrapper"]',{timeout:60000}).contains(' NEW PROFILE ')
    }
    getTitle_Modal(){
        return cy.get('header[class="mat-dialog-title title-modal"]',{timeout:60000}).contains('NEW PROFILE')
    }
    getInput_Profile_Modal(){
        return cy.get('input[formcontrolname="profile"]',{timeout:60000}).should('be.visible')
    }
    getSelect_Status_Modal(){
        return cy.get('mat-select[ng-reflect-value="status"]',{timeout:60000}).should('be.visible')
    }
    getSelectCountry_Modal(){
        return cy.get('mat-select[formcontrolname="countries"]',{timeout:60000}).should('be.visible')
    }
    getTitle_PermModal_Modal(){
        return cy.get('header[class="mat-dialog-title title-modal"]',{timeout:60000}).contains('PERMISSIONS')
    }
    getTabAdministration_Modal(){
        return cy.get('div[class="mat-tab-label-content"]',{timeout:60000}).contains('ADMINISTRATION')
    }
    getTabCustomerAlloc_Modal(){
        return cy.get('div[class="mat-tab-label-content"]',{timeout:60000}).contains('CUSTOMER ALLOCATION')
    }
    getTabKaAlloc_Modal(){
        return cy.get('div[class="mat-tab-label-content"]',{timeout:60000}).contains('KA ALLOCATION')
    }
    getTabOnePlan_Modal(){
        return cy.get('div[class="mat-tab-label-content"]',{timeout:60000}).contains('ONE PLAN')
    }
    getTabSetup_Modal(){
        return cy.get('div[class="mat-tab-label-content"]',{timeout:60000}).contains('SETUP')
    }
    getItemProfile_Modal(){
        return cy.get('div[class="mat-list-text"]',{timeout:60000}).contains('Profile')
    }
    getItemUser_Modal(){
        return cy.get('div[class="mat-list-text"]',{timeout:60000}).contains('User')
    }
    getItemAllocationRetail_Modal(){
        return cy.get('div[class="mat-list-text"]',{timeout:60000}).contains('Allocation Retail')
    }
    getItemBooked_Modal(){
        return cy.get('div[class="mat-list-text"]',{timeout:60000}).contains('Booked')
    }
    getItemExecutiveAllocation_Modal(){
        return cy.get('div[class="mat-list-text"]',{timeout:60000}).contains('Executive Allocation')
    }
    getItemMissing_Modal(){
        return cy.get('div[class="mat-list-text"]',{timeout:60000}).contains('Missing')
    }
    getItemOverbooked_Modal(){
        return cy.get('div[class="mat-list-text"]',{timeout:60000}).contains('Overbooked')
    }
    getItemToBook_Modal(){
        return cy.get('div[class="mat-list-text"]',{timeout:60000}).contains('To Book')
    }
    getItemBrazilDemand_Modal(){
        return cy.get('div[class="mat-list-text"]',{timeout:60000}).contains('Brazil Demand')
    }
    getItemCarriesInfo_Modal(){
        return cy.get('div[class="mat-list-text"]',{timeout:60000}).contains('Carries Info')
    }
    getItemKaAllocation_Modal(){
        return cy.get('div[class="mat-list-text"]',{timeout:60000}).contains('KA Allocation')
    }
    getItemKaManagement_Modal(){
        return cy.get('div[class="mat-list-text"]',{timeout:60000}).contains('KA Management')
    }
    getItemLom_Modal(){
        return cy.get('div[class="mat-list-text"]',{timeout:60000}).contains('LOM')
    }
    getItemOnePlan_Modal(){
        return cy.get('div[class="mat-list-text"]',{timeout:60000}).contains('One Plan Upload')
    }
    getItemOnePlanUpload_Modal(){
        return cy.get('div[class="mat-list-text"]',{timeout:60000}).contains('One Plan Upload')
    }
    getItemFlexOnePlanVariation_Modal(){
        return cy.get('div[class="mat-list-text"]',{timeout:60000}).contains('Flex One Plan Variation')
    }
    getItemChannel_Modal(){
        return cy.get('div[class="mat-list-text"]',{timeout:60000}).contains('Channel')
    }
    getItemCountry_Modal(){
        return cy.get('div[class="mat-list-text"]',{timeout:60000}).contains('Country')
    }
    getItemCustomerManagement_Modal(){
        return cy.get('div[class="mat-list-text"]',{timeout:60000}).contains('Customer Management')
    }
    getItemCustomerSettings_Modal(){
        return cy.get('div[class="mat-list-text"]',{timeout:60000}).contains('Customer Settings')
    }
    getItemFiscalCalendar_Modal(){
        return cy.get('div[class="mat-list-text"]',{timeout:60000}).contains('Fiscal Calendar')
    }
    getItemGeo_Modal(){
        return cy.get('div[class="mat-list-text"]',{timeout:60000}).contains('Geo')
    }
    getItemOrderAdminManagement_Modal(){
        return cy.get('div[class="mat-list-text"]',{timeout:60000}).contains('Order Admin Management')
    }
    getItemSalesManagement_Modal(){
        return cy.get('div[class="mat-list-text"]',{timeout:60000}).contains('Sales Management')
    }
    getItemSetupManagement_Modal(){
        return cy.get('div[class="mat-list-text"]',{timeout:60000}).contains('Setup Management')
    }
    getItemProfileCreate_Modal(){
        return cy.get('label[for="profile-CREATE"]',{timeout:60000})
    }
    getItemUserCreate_Modal(){
        return cy.get('label[for="user-CREATE"]',{timeout:60000})
    }
    getItemAllocationRetailCreate_Modal(){
        return cy.get('label[for="allocation-retail-CREATE"]',{timeout:60000})
    }
    getItemBookedCreate_Modal(){
        return cy.get('label[for="booked-CREATE"]',{timeout:60000})
    }
    getItemExecutiveAllocationCreate_Modal(){
        return cy.get('label[for="executive-allocation-CREATE"]',{timeout:60000})
    }
    getItemMissingCreate_Modal(){
        return cy.get('label[for="missing-CREATE"]',{timeout:60000})
    }
    getItemOverbookedCreate_Modal(){
        return cy.get('label[for="overbooked-CREATE"]',{timeout:60000})
    }
    getItemToBookCreate_Modal(){
        return cy.get('label[for="to-book-CREATE"]',{timeout:60000})
    }
    getItemBrazilDemandCreate_Modal(){
        return cy.get('label[for="brazil-demand-CREATE"]',{timeout:60000})
    }
    getItemCarriesInfoCreate_Modal(){
        return cy.get('label[for="carries-info-CREATE"]',{timeout:60000})
    }
    getItemKaManagementCreate_Modal(){
        return cy.get('label[for="ka-management-CREATE"]',{timeout:60000})
    }
    getItemLomCreate_Modal(){
        return cy.get('label[for="lom-CREATE"]',{timeout:60000})
    }
    getItemOnePlanCreate_Modal(){
        return cy.get('label[for="one-plan-CREATE"]',{timeout:60000})
    }
    getItemOnePlanVariationAnalysisCreate_Modal(){
        return cy.get('label[for="one-plan-variation-CREATE"]',{timeout:60000})
    }
    getItemChannelCreate_Modal(){
        return cy.get('label[for="channel-CREATE"]',{timeout:60000})
    }
    getItemCountryCreate_Modal(){
        return cy.get('label[for="country-CREATE"]',{timeout:60000})
    }
    getItemCustomerSettingsCreate_Modal(){
        return cy.get('label[for="customer-short-CREATE"]',{timeout:60000})
    }
    getItemGeoCreate_Modal(){
        return cy.get('label[for="geo-CREATE"]',{timeout:60000})
    }
    getItemSalesManagementCreate_Modal(){
        return cy.get('label[for="sales-management-CREATE"]',{timeout:60000})
    }
    getItemSetupManagementCreate_Modal(){
        return cy.get('label[for="setup-management-CREATE"]',{timeout:60000})
    }
    getItemProfileDelete_Modal(){
        return cy.get('label[for="profile-DELETE"]',{timeout:60000})
    }
    getItemUserDelete_Modal(){
        return cy.get('label[for="user-DELETE"]',{timeout:60000})
    }
    getItemAllocationRetailDelete_Modal(){
        return cy.get('label[for="allocation-retail-DELETE"]',{timeout:60000})
    }
    getItemBookedDelete_Modal(){
        return cy.get('label[for="booked-DELETE"]',{timeout:60000})
    }
    getItemExecutiveAllocationDelete_Modal(){
        return cy.get('label[for="executive-allocation-DELETE"]',{timeout:60000})
    }
    getItemMissingDelete_Modal(){
        return cy.get('label[for="missing-DELETE"]',{timeout:60000})
    }
    getItemOverbookedDelete_Modal(){
        return cy.get('label[for="overbooked-DELETE"]',{timeout:60000})
    }
    getItemToBookDelete_Modal(){
        return cy.get('label[for="to-book-DELETE"]',{timeout:60000})
    }
    getItemBrazilDemandDelete_Modal(){
        return cy.get('label[for="brazil-demand-DELETE"]',{timeout:60000})
    }
    getItemCarriesInfoDelete_Modal(){
        return cy.get('label[for="carries-info-DELETE"]',{timeout:60000})
    }
    getItemLomDelete_Modal(){
        return cy.get('label[for="lom-DELETE"]',{timeout:60000})
    }
    getItemOnePlanDelete_Modal(){
        return cy.get('label[for="one-plan-DELETE"]',{timeout:60000})
    }
    getItemOnePlanVariationAnalysisDelete_Modal(){
        return cy.get('label[for="one-plan-variation-DELETE"]',{timeout:60000})
    }
    getItemChannelDelete_Modal(){
        return cy.get('label[for="channel-DELETE"]',{timeout:60000})
    }
    getItemCountryDelete_Modal(){
        return cy.get('label[for="country-DELETE"]',{timeout:60000})
    }
    getItemGeoDelete_Modal(){
        return cy.get('label[for="geo-DELETE"]',{timeout:60000})
    }
    getItemOrderAdminManagementDelete_Modal(){
        return cy.get('label[for="order-admin-management-DELETE"]',{timeout:60000})
    }
    getItemSalesManagementDelete_Modal(){
        return cy.get('label[for="sales-management-DELETE"]',{timeout:60000})
    }
    getItemProfileUpdate_Modal(){
        return cy.get('label[for="profile-UPDATE"]',{timeout:60000})
    }
    getItemUserUpdate_Modal(){
        return cy.get('label[for="user-UPDATE"]',{timeout:60000})
    }
    getItemAllocationRetailUpdate_Modal(){
        return cy.get('label[for="allocation-retail-UPDATE"]',{timeout:60000})
    }
    getItemBookedUpdate_Modal(){
        return cy.get('label[for="booked-UPDATE"]',{timeout:60000})
    }
    getItemExecutiveAllocationUpdate_Modal(){
        return cy.get('label[for="executive-allocation-UPDATE"]',{timeout:60000})
    }
    getItemMissingUpdate_Modal(){
        return cy.get('label[for="missing-UPDATE"]',{timeout:60000})
    }
    getItemOverbookedUpdate_Modal(){
        return cy.get('label[for="overbooked-UPDATE"]',{timeout:60000})
    }
    getItemToBookUpdate_Modal(){
        return cy.get('label[for="to-book-UPDATE"]',{timeout:60000})
    }
    getItemBrazilDemandUpdate_Modal(){
        return cy.get('label[for="brazil-demand-UPDATE"]',{timeout:60000})
    }
    getItemCarriesInfoUpdate_Modal(){
        return cy.get('label[for="carries-info-UPDATE"]',{timeout:60000})
    }
    getItemKaAllocationUpdate_Modal(){
        return cy.get('label[for="ka-allocation-UPDATE"]',{timeout:60000})
    }
    getItemKaManagementUpdate_Modal(){
        return cy.get('label[for="ka-management-UPDATE"]',{timeout:60000})
    }
    getItemLomUpdate_Modal(){
        return cy.get('label[for="lom-UPDATE"]',{timeout:60000})
    }
    getItemMaterialManagementUpdate_Modal(){
        return cy.get('label[for="material-management-UPDATE"]',{timeout:60000})
    }
    getItemOnePlanUpdate_Modal(){
        return cy.get('label[for="one-plan-UPDATE"]',{timeout:60000})
    }
    getItemOnePlanVariationAnalysisUpdate_Modal(){
        return cy.get('label[for="one-plan-variation-UPDATE"]',{timeout:60000})
    }
    getItemChannelUpdate_Modal(){
        return cy.get('label[for="channel-UPDATE"]',{timeout:60000})
    }
    getItemCountryUpdate_Modal(){
        return cy.get('label[for="country-UPDATE"]',{timeout:60000})
    }
    getItemCustomerManagementUpdate_Modal(){
        return cy.get('label[for="customer-management-UPDATE"]',{timeout:60000})
    }
    getItemCustomerSettingsUpdate_Modal(){
        return cy.get('label[for="customer-short-UPDATE"]',{timeout:60000})
    }
    getItemGeoUpdate_Modal(){
        return cy.get('label[for="geo-UPDATE"]',{timeout:60000})
    }
    getItemOrderAdminManagementUpdate_Modal(){
        return cy.get('label[for="order-admin-management-UPDATE"]',{timeout:60000})
    }
    getItemSalesManagementUpdate_Modal(){
        return cy.get('label[for="sales-management-UPDATE"]',{timeout:60000})
    }
    getItemSetupManagementUpdate_Modal(){
        return cy.get('label[for="setup-management-UPDATE"]',{timeout:60000})
    }
    getItemProfileView_Modal(){
        return cy.get('label[for="profile-VIEW"]',{timeout:60000})
    }
    getItemUserView_Modal(){
        return cy.get('label[for="user-VIEW"]',{timeout:60000})
    }
    getItemAllocationRetailView_Modal(){
        return cy.get('label[for="allocation-retail-VIEW"]',{timeout:60000})
    }
    getItemBookedView_Modal(){
        return cy.get('label[for="booked-VIEW"]',{timeout:60000})
    }
    getItemExecutiveAllocationView_Modal(){
        return cy.get('label[for="executive-allocation-VIEW"]',{timeout:60000})
    }
    getItemMissingView_Modal(){
        return cy.get('label[for="missing-VIEW"]',{timeout:60000})
    }
    getItemOverbookedView_Modal(){
        return cy.get('label[for="overbooked-VIEW"]',{timeout:60000})
    }
    getItemToBookView_Modal(){
        return cy.get('label[for="to-book-VIEW"]',{timeout:60000})
    }
    getItemBrazilDemandView_Modal(){
        return cy.get('label[for="brazil-demand-VIEW"]',{timeout:60000})
    }
    getItemCarriesInfoView_Modal(){
        return cy.get('label[for="carries-info-VIEW"]',{timeout:60000})
    }
    getItemKaAllocationView_Modal(){
        return cy.get('label[for="ka-allocation-VIEW"]',{timeout:60000})
    }
    getItemKaManagementView_Modal(){
        return cy.get('label[for="ka-management-VIEW"]',{timeout:60000})
    }
    getItemLomView_Modal(){
        return cy.get('label[for="lom-VIEW"]',{timeout:60000})
    }
    getItemMaterialManagementView_Modal(){
        return cy.get('label[for="material-management-VIEW"]',{timeout:60000})
    }
    getItemOnePlanView_Modal(){
        return cy.get('label[for="one-plan-VIEW"]',{timeout:60000})
    }
    getItemOnePlanVariationAnalysisView_Modal(){
        return cy.get('label[for="one-plan-variation-VIEW"]',{timeout:60000})
    }
    getItemChannelView_Modal(){
        return cy.get('label[for="channel-VIEW"]',{timeout:60000})
    }
    getItemCountryView_Modal(){
        return cy.get('label[for="country-VIEW"]',{timeout:60000})
    }
    getItemCustomerManagementView_Modal(){
        return cy.get('label[for="customer-management-VIEW"]',{timeout:60000})
    }
    getItemCustomerSettingsView_Modal(){
        return cy.get('label[for="customer-short-VIEW"]',{timeout:60000})
    }
    getItemFiscalCalendarView_Modal(){
        return cy.get('label[for="fiscal-calendar-VIEW"]',{timeout:60000})
    }
    getItemGeoView_Modal(){
        return cy.get('label[for="geo-VIEW"]',{timeout:60000})
    }
    getItemOrderAdminManagementView_Modal(){
        return cy.get('label[for="order-admin-management-VIEW"]',{timeout:60000})
    }
    getItemSalesManagementView_Modal(){
        return cy.get('label[for="sales-management-VIEW"]',{timeout:60000})
    }
    getItemSetupManagementView_Modal(){
        return cy.get('label[for="setup-management-VIEW"]',{timeout:60000})
    }
    SelectAllPermissions_Modal(){
        return cy.get('div[ng-reflect-message="Select All"]',{timeout:60000})
    }
    checkNoResultsFound(){
        return cy.get('span[class="result-length ng-star-inserted"]',{timeout:60000})
    }
}