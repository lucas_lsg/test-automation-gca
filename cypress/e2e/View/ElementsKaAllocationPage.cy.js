export default class ElementsKaAllocationPage {
    getTitle_Page(){
        return cy.get('div[class="title"]',{timeout:60000}).contains('KA ALLOCATION')
    }
    getInput_Geo(){
        return cy.get('input[ng-reflect-name="geoId"]',{timeout:60000}).should('be.visible')
    }
    getInput_Country(){
        return cy.get('input[ng-reflect-name="countryId"]',{timeout:60000}).should('be.visible')
    }
    getInput_SalesModel(){
        return cy.get('input[ng-reflect-name="salesModelId"]',{timeout:60000}).should('be.visible')
    }
    getInput_Product(){
        return cy.get('input[ng-reflect-name="productId"]',{timeout:60000}).should('be.visible')
    }
    getInput_Ka(){
        return cy.get('input[ng-reflect-name="KaId"]',{timeout:60000}).should('be.visible')
    }
}