export default class ElementsSetupManagementPage {
    getTitle_Page(){
        return cy.get('span[class="title"]',{timeout:60000}).contains('SETUP MANAGEMENT')
    }
    getInputList_Country(){
        return cy.get('input[ng-reflect-name="countryId"]',{timeout:60000}).should('be.visible')
    }
    getSelectList_FiscalYear(){
        return cy.get('mat-select[ng-reflect-name="fiscalYear"]',{timeout:60000}).should('be.visible')
    }
    getSelectList_Quarter(){
        return cy.get('mat-select[ng-reflect-name="quarter"]',{timeout:60000}).should('be.visible')
    }
    getButton_Edit_Current_Setup(){
        return cy.get('span[class="mat-button-wrapper"]',{timeout:60000}).contains('EDIT CURRENT SETUP')
    }
    getTitle_Modal(){
        return cy.get('header[class="mat-dialog-title title-modal"]',{timeout:60000}).contains('EDIT CURRENT SETUP')
    }
    getInput_Country(){
        return cy.get('mat-select[formcontrolname="countryId"]',{timeout:60000}).should('be.visible')
    }
    getInput_ToBook(){
        return cy.get('mat-select[formcontrolname="toBook"]',{timeout:60000}).should('be.visible')
    }
    getInput_ToAllocate(){
        return cy.get('mat-select[formcontrolname="toAllocate"]',{timeout:60000}).should('be.visible')
    }
    getInput_EnteredSetup(){
        return cy.get('mat-select[formcontrolname="enteredSetup"]',{timeout:60000}).should('be.visible')
    }
    getCheckIconSetup(row,wk){
        return cy.get(`:nth-child(${row}) > .cdk-column-${wk} > .icon-check`)
    }
    getCheckIconEntered(month){
        return cy.get(`.cdk-column-${month} > .icon-check`)
    }
}