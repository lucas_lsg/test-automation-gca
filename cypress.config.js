const { defineConfig } = require("cypress");
module.exports = defineConfig({
  projectId: "en9o31",
  redirectionLimit: 50,
  waitForAnimations: true,
  viewportWidth: 1500,
  viewportHeight: 900,
  videosFolder: "cypress/videos",
  failOnStatusCode: false,
  video: true,
  e2e: {
    // We've imported your old cypress plugins here.
    // You may want to clean this up later by importing these.
    setupNodeEvents(on, config) {
      return require("./cypress/plugins/index.js")(on, config);
      
    },
    
    baseUrl: "https://dev-dot-gca-front-dot-dataservices-non-prod.appspot.com",
    excludeSpecPattern: [
      "**/Controller/*.cy.js",
      "**/Model/*.cy.js",
      "**/View/*.cy.js",
    ],
    specPattern: [
      "**/TestCase/Home/Admin/*.cy.js",
      "**/TestCase/Home/*.cy.js",
      "**/TestCase/Setup/*.cy.js",
      "**/TestCase/KA_Allocation/*.cy.js",
      "**/TestCase/*.cy.js",
    ],
    retries: {
      // Configure retry attempts for `cypress run`
      // Default is 0
      runMode: 2,
      // Configure retry attempts for `cypress open`
      // Default is 0
      openMode: 2,
    },
    //experimentalSessionAndOrigin: true,
  },
});
